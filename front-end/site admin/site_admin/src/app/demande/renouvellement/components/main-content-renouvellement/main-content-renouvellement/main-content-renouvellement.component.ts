import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UserService } from 'src/app/user/components/services/user.service';
import { Renouvellement } from '../../models/Renouvellement';
import { RenouvellementService } from '../../service/renouvellement.services';

@Component({
  selector: 'app-main-content-renouvellement',
  templateUrl: './main-content-renouvellement.component.html',
  styleUrls: ['./main-content-renouvellement.component.scss']
})
export class MainContentRenouvellementComponent implements OnInit {

  courentSection = "enCour";
  buttonText = "en cour"
  renouvellements: any
  btn = "warning";
  role = 'guene';
  rvForm1!: FormGroup;
  renouvellementcurrent: any;
  compt = 0;
  closeResult = '';
  lock = 1;
  id: any;
  client: any
  currentClient: any
  submitForm = false;
  motif: any
  constructor(private renouvellementService: RenouvellementService,
    private router: Router,
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private userService: UserService,
  ) {  }

  ngOnInit(): void {
    this.getRenouvellements()

  }

  getClient(id: number) {
    this.userService.getUser(id).subscribe({
      next: (res) => {
        this.currentClient = res;
      }
    })
  }
  getRenouvellements() {
    this.renouvellementService.getRenouvellements().subscribe({
      next: (res) => {
        this.renouvellements = res;
        console.log(res)
      }
    })
  }
  open(content: any, id: number) {
    this.initForm()
    this.id = id
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      console.log(reason)
    });
  }

  initForm() {
    this.rvForm1 = this.formBuilder.group({
      motifRejet: ['', Validators.required],
    })
  }

  // pour rejet une demande il faut cree un motif puis son id dans la table renouvellement

  supprimerRenouvellement(): void {
    // creation de la motif
    const formValue = this.rvForm1.value
    this.renouvellementService.createMotif(formValue.motifRejet).subscribe((response) => {
      this.motif = response.id

    // insertion de la motif
    this.renouvellementService.getRenouvellement(this.id).subscribe((response: any) => {
      this.renouvellementcurrent = response;
      this.renouvellementService.SupprimerRenouvellement(
        this.renouvellementcurrent?.id,
        this.renouvellementcurrent?.numeroRegistre,
        this.renouvellementcurrent?.annee,
        this.renouvellementcurrent?.nombreExemplaire,
        this.renouvellementcurrent?.extrait,
        this.renouvellementcurrent?.agent,
        this.motif
      ).subscribe({
        next: (res) => {
          console.log(res);
          this.getRenouvellements()
        },
        error: (e) => console.error(e)
      });
    });
  })
  }
  etatRetrairRenouvellement(id: number): void {
    this.renouvellementService.getRenouvellement(id).subscribe((response: any) => {
      this.renouvellementcurrent = response;
      this.renouvellementService.etatRetraiRenouvellement(
        this.renouvellementcurrent?.id,
        this.renouvellementcurrent?.numeroRegistre,
        this.renouvellementcurrent?.annee,
        this.renouvellementcurrent?.nombreExemplaire,
        this.renouvellementcurrent?.extrait,
        this.renouvellementcurrent?.agent,
        this.renouvellementcurrent?.motif,
        this.renouvellementcurrent?.etat,
        !this.renouvellementcurrent?.etatRetrait,
      ).subscribe({
        next: (res) => {
          console.log(res);
          this.getRenouvellements()
        },
        error: (e) => console.error(e)
      });
    });
  }

  nonterminerRenouvellement(id: number): void {
    this.renouvellementService.getRenouvellement(id).subscribe((response: any) => {
      this.renouvellementcurrent = response;
      this.renouvellementService.terminerRenouvellement(
        this.renouvellementcurrent?.id,
        this.renouvellementcurrent?.numeroRegistre,
        this.renouvellementcurrent?.annee,
        this.renouvellementcurrent?.nombreExemplaire,
        this.renouvellementcurrent?.extrait,
        this.renouvellementcurrent?.agent,
        this.renouvellementcurrent?.motif,
        false
      ).subscribe({
        next: (res) => {
          console.log(res);
          this.getRenouvellements()
        },
        error: (e) => console.error(e)
      });
    });
  }
  changeSection(section: string) {
    this.courentSection = section;
  }
  // changeEtat(n : number): void{
  //   this.renouvellementcurrent= this.renouvellements.find(p => p.id == n)
  //     if(this.renouvellementcurrent['etat']=== 'en cour'){
  //       this.renouvellementcurrent['etat']= 'terminer'
  //       }else{
  //         this.renouvellementcurrent['etat']= 'en cour';
  //       }
  //   }


}
