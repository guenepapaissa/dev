import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AgentService } from 'src/app/agent/components/service/agent.service';
import { UserService } from 'src/app/user/components/services/user.service';
import { Renouvellement } from '../../models/Renouvellement';
import { RenouvellementService } from '../../service/renouvellement.services';

@Component({
  selector: 'app-detail-renouvellement',
  templateUrl: './detail-renouvellement.component.html',
  styleUrls: ['./detail-renouvellement.component.scss']
})
export class DetailRenouvellementComponent implements OnInit {

  id !: number;
  currentrenouvellement: any;
  client!:any;
  motif: any;
  currentMotif:any
  currentAgent:any
  profil=localStorage.getItem('profil')

  constructor(private activatedRoute: ActivatedRoute,
    private renouvellementService: RenouvellementService,
    private userService: UserService,
    private agentService : AgentService
  ) { }

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.params['id'];
    //this.currentrenouvellement = this.renouvellementService.editerrenouvellement(this.id);
    this.getRenouvellement(this.id)
  }

  getRenouvellement(id: number) {
    this.renouvellementService.getRenouvellement(id).subscribe((response: any) => {
      this.currentrenouvellement = response;
      this.getClient()
      this.getMotif(this.currentrenouvellement.motif)
      this.getAgent(this.currentrenouvellement.agent)
    });
  }
  getClient(){
   this.userService.getUser(this.currentrenouvellement?.client).subscribe((response)=>{
     this.client=response
   })
  }
  getMotif(id:number){
    this.currentMotif=this.renouvellementService.getMotif(id).subscribe((response)=>{
      this.currentMotif=response
    })
  }
  getAgent(id: number){
    this.agentService.getAgent(id).subscribe((response)=>{
      this.currentAgent=response
    })
  }

}

