import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Renouvellement } from '../models/Renouvellement';




@Injectable({
  providedIn: 'root'
})
export class RenouvellementService {
  renouvellement!: Renouvellement[];
  renouvellementcurrent : any;
  api= environment.api
  agent= <any> localStorage.getItem('idUser')
  constructor(private http : HttpClient) {

  }


  getRenouvellements(){
    return this.http.get<any>(this.api+'renouvellement/')
  }
  getRenouvellement(id: any){
    return this.http.get<any>(this.api+'renouvellement/'+id)
  }
  createMotif(motif:string){
    return this.http.post<any>(this.api+'motif/',{motif:motif})
  }
  getMotif(id: any){
    return this.http.get<any>(this.api+'motif/'+id)
  }
  getEnleverMotif(id: any){
    return this.http.get<any>(this.api+'eneleverMotifRenouvellement/'+id)
  }
  SupprimerRenouvellement(id: any,numeroRegistre: number,annee: number,nombreExemplaire:number, extrait:String,agent:any, motif: any){

    let update = {
      "numeroRegistre": numeroRegistre,
      "annee": annee,
      "nombreExemplaire": nombreExemplaire,
      "extrait": extrait,
      "agent":this.agent,
      "motif": motif,
    }
    return this.http.put<any>(`${this.api}renouvellement/${id}`,update);
  }
  updateRenouvellement(id: any,prenom: string, nom: string, telephone: string, email: string, login: string, password: string){
    let update = {
      "numeroRegistre": prenom,
      "anne": nom,
      "nombreExemplaire": telephone,
      "etatRetrait": email,
      "idDemande": login,
      "date": password,
      "motifRejet": password,
      "etatDemande": password,

    }
    return this.http.put(`${this.api}renouvellement/${id}`,update);
  }
  etatRetraiRenouvellement(id: any,numeroRegistre: number,annee: number,nombreExemplaire:number, extrait:String,agent:any, motif: any, etat:boolean,etatRetrait:boolean){
    let update = {
      "numeroRegistre": numeroRegistre,
      "annee": annee,
      "nombreExemplaire": nombreExemplaire,
      "extrait": extrait,
      "agent":this.agent,
      "motif": motif,
      "etat":etat,
      "etatRetrait":etatRetrait
    }
    return this.http.put<any>(`${this.api}renouvellement/${id}`,update);
  }
  terminerRenouvellement(id: any,numeroRegistre: number,annee: number,nombreExemplaire:number, extrait:String,agent:any, motif: any, etat:boolean){
    let update = {
      "numeroRegistre": numeroRegistre,
      "annee": annee,
      "nombreExemplaire": nombreExemplaire,
      "extrait": extrait,
      "agent":this.agent,
      "motif": motif,
      "etat":etat
    }
    return this.http.put<any>(`${this.api}renouvellement/${id}`,update);
  }
  // createNaissance(date: Date, etat: boolean, telephone: string, email: string, login: string, password: string){
  //   let create = {


  //   }
  //   return this.http.put(`${this.api}renouvellement/`,create);
  // }




  listrenouvellementent(){
    return this.renouvellement;
  }
  // listAgent(): Agent[] {
  //   return this.agent;
  // }
  editerrenouvellement(idrenouvellement: number): Renouvellement[] {
    this.renouvellementcurrent = this.renouvellement.find(p => p.id == idrenouvellement)
    return this.renouvellementcurrent;
  }

  // detailAgent(idUser: number): Agent[] {
  //   this.agentcurrent = this.agent.find(p => p.id == idUser)
  //   return this.agentcurrent;
  // }


}
