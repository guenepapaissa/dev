import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RejetRenouvellementComponent } from './rejet-renouvellement.component';

describe('RejetRenouvellementComponent', () => {
  let component: RejetRenouvellementComponent;
  let fixture: ComponentFixture<RejetRenouvellementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RejetRenouvellementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RejetRenouvellementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
