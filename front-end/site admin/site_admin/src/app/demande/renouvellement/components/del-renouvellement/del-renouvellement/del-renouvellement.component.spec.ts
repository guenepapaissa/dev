import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DelRenouvellementComponent } from './del-renouvellement.component';

describe('DelRenouvellementComponent', () => {
  let component: DelRenouvellementComponent;
  let fixture: ComponentFixture<DelRenouvellementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DelRenouvellementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DelRenouvellementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
