import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditRenouvellementComponent } from './edit-renouvellement.component';

describe('EditRenouvellementComponent', () => {
  let component: EditRenouvellementComponent;
  let fixture: ComponentFixture<EditRenouvellementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditRenouvellementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditRenouvellementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
