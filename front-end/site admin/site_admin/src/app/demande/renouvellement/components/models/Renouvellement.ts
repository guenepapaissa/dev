export class Renouvellement{
  constructor(
    public id : number,
    public lock: number,
    public numrenouvellement: string,
    public nom: string,
    public prenom: string,
    public adresse: string,
    public retrait: string,
    public num: string,
    public papier: string,
    public dateDebut: Date,
    public etat?: string,
    public motifRejet?:string,
    public dateRv?: Date,
  ){}
}
