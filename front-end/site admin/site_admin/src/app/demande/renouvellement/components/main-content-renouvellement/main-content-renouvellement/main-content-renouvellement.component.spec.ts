import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MainContentRenouvellementComponent } from './main-content-renouvellement.component';

describe('MainContentRenouvellementComponent', () => {
  let component: MainContentRenouvellementComponent;
  let fixture: ComponentFixture<MainContentRenouvellementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MainContentRenouvellementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainContentRenouvellementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
