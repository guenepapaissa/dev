import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailRenouvellementComponent } from './detail-renouvellement.component';

describe('DetailRenouvellementComponent', () => {
  let component: DetailRenouvellementComponent;
  let fixture: ComponentFixture<DetailRenouvellementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailRenouvellementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailRenouvellementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
