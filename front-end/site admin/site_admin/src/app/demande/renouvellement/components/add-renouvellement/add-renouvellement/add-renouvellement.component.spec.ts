import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddRenouvellementComponent } from './add-renouvellement.component';

describe('AddRenouvellementComponent', () => {
  let component: AddRenouvellementComponent;
  let fixture: ComponentFixture<AddRenouvellementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddRenouvellementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddRenouvellementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
