import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UserService } from 'src/app/user/components/services/user.service';
import { Renouvellement } from '../../models/Renouvellement';
import { RenouvellementService } from '../../service/renouvellement.services';

@Component({
  selector: 'app-edit-renouvellement',
  templateUrl: './edit-renouvellement.component.html',
  styleUrls: ['./edit-renouvellement.component.scss']
})
export class EditRenouvellementComponent implements OnInit {

  id !: number;
  currentrenouvellement: any;
  active = 1;
  closeResult = '';
  rvForm1!: FormGroup;
  client!:any;
  motif: any;
  currentMotif:any
  constructor(private activatedRoute: ActivatedRoute,
    private route: Router,
    private renouvellementService: RenouvellementService,
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private userService: UserService,
  ) { }

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.params['id'];
    //this.currentrenouvellement = this.renouvellementService.editerrenouvellement(this.id);
    this.getRenouvellement(this.id)
    this.initForm()
  }
  getRenouvellement(id: number) {
    this.renouvellementService.getRenouvellement(id).subscribe((response: any) => {
      this.currentrenouvellement = response;
      this.getClient()
      if(this.currentrenouvellement.motif){
        this.getMotif(this.currentrenouvellement.motif)
      }

    });
  }
  getClient(){
   this.userService.getUser(this.currentrenouvellement?.client).subscribe((response)=>{
     this.client=response
   })
  }
  getMotif(id:number){
    this.currentMotif=this.renouvellementService.getMotif(id).subscribe((response)=>{
      this.currentMotif=response
    })
  }
  recupererRenouvellement(): void {
    this.renouvellementService.getRenouvellement(this.id).subscribe((response: any) => {
      this.currentrenouvellement = response;
      this.renouvellementService.getEnleverMotif(
        this.currentrenouvellement?.id).subscribe({
        next: (res) => {
          console.log(res);
         this.route.navigate(['/renouvellement'])
        },
        error: (e) => console.error(e)
      });
    });
  }
  supprimer(motifRejet: string) {

    this.currentrenouvellement.motifRejet = motifRejet;
    this.route.navigate(['/renouvellement'])
  }
  terminer() {
    this.currentrenouvellement.etat = 'terminer'
    this.route.navigate(['/renouvellement']);
  }
  recuperer() {
    this.currentrenouvellement.motifRejet = '';
    this.route.navigate(['/renouvellement'])
  }

  // suppression renouvellemnt
  supprimerRenouvellement(): void {
    // creation de la motif
    const formValue = this.rvForm1.value
    this.renouvellementService.createMotif(formValue.motifRejet).subscribe((response) => {
      this.motif = response.id

    // insertion de la motif
    this.renouvellementService.getRenouvellement(this.id).subscribe((response: any) => {
      this.currentrenouvellement = response;
      this.renouvellementService.SupprimerRenouvellement(
        this.currentrenouvellement?.id,
        this.currentrenouvellement?.numeroRegistre,
        this.currentrenouvellement?.annee,
        this.currentrenouvellement?.nombreExemplaire,
        this.currentrenouvellement?.extrait,
        this.currentrenouvellement?.agent,
        this.motif
      ).subscribe({
        next: (res) => {
          console.log(res);
          this.route.navigate(['/renouvellement'])
        },
        error: (e) => console.error(e)
      });
    });
  })
  }

  terminerRenouvellement(): void {
    // cette methode permet de modier l'etat de la demande, encour <=>  terminer
    this.renouvellementService.getRenouvellement(this.id).subscribe((response: any) => {
      this.currentrenouvellement = response;
      console.log(this.currentrenouvellement)
      this.renouvellementService.terminerRenouvellement(
        this.currentrenouvellement?.id,
        this.currentrenouvellement?.numeroRegistre,
        this.currentrenouvellement?.annee,
        this.currentrenouvellement?.nombreExemplaire,
        this.currentrenouvellement?.extrait,
        this.currentrenouvellement?.agent,
        this.currentrenouvellement?.motif,
        true
      ).subscribe({
        next: (res) => {
          console.log(res);
          this.route.navigate(['/renouvellement'])
        },
        error: (e) => console.error(e)
      });
    });
  }

  open(content: any) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result: any) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason: any) => {
   //   this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  initForm() {
    this.rvForm1 = this.formBuilder.group({
      motifRejet: ['', Validators.required],
    })
  }

}


