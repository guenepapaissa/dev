import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Renouvellement } from '../../models/Renouvellement';
import { RenouvellementService } from '../../service/renouvellement.services';

@Component({
  selector: 'app-del-renouvellement',
  templateUrl: './del-renouvellement.component.html',
  styleUrls: ['./del-renouvellement.component.scss']
})
export class DelRenouvellementComponent implements OnInit {

  renouvellementsArchive : any
  renouvellementcurrent: any;
  closeResult = '';
  id!:number
  currentMotif:any

  constructor(  private renouvellementService: RenouvellementService,
                private router : Router,
                private modalService: NgbModal,
     ) {
   }
  ngOnInit(): void {
    this.getArchiveRenouvellemnt();
  }
  getArchiveRenouvellemnt(){
    this.renouvellementService.getRenouvellements().subscribe({
      next : (res)=>{
        this.renouvellementsArchive=res
        console.log(this.renouvellementsArchive)
      }
    })
  }
  openSm(content: any, id: number) {
    this.id=id
    this.getMotif(id)
    this.modalService.open(content,{ size: 'sm' }).result.then((result: any) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason: any) => {
   //   this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  getMotif(id:number){
    this.currentMotif=this.renouvellementService.getMotif(id).subscribe((response)=>{
      this.currentMotif=response
    })
  }
}
