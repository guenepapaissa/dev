import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AddRenouvellementComponent } from "./components/add-renouvellement/add-renouvellement/add-renouvellement.component";
import { DelRenouvellementComponent } from "./components/del-renouvellement/del-renouvellement/del-renouvellement.component";
import { DetailRenouvellementComponent } from "./components/detail-renouvellement/detail-renouvellement/detail-renouvellement.component";
import { EditRenouvellementComponent } from "./components/edit-renouvellement/edit-renouvellement/edit-renouvellement.component";
import { MainContentRenouvellementComponent } from "./components/main-content-renouvellement/main-content-renouvellement/main-content-renouvellement.component";

const routes: Routes = [
  {
    path: '',
    data: {
      title: `Renouvellement`
    },
    children: [
      {
        path: '',
        redirectTo: '',
        pathMatch: 'full',
        component: MainContentRenouvellementComponent,
        data: {
          title: $localize`Renouvellement`
        }
      },
      {
        path: 'addrnuvlmnt',
        component: AddRenouvellementComponent,
        data: {
          title: $localize`Ajout Renouvellement`
        }
      },
      {
        path: 'editrnuvlmnt/:id',
        component: EditRenouvellementComponent,
        data: {
          title: $localize`Modifier Renouvellement`
        }
      },
      {
        path: 'detailrnuvlmnt/:id',
        component: DetailRenouvellementComponent,
        data: {
          title: $localize`Detail Renouvellement`
        }
      },

    ]
  },

];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]

})
export class RenouvellementRoutingModule {

}
