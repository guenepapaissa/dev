import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AddToutgenreComponent } from "./components/add-toutgenre/add-toutgenre/add-toutgenre.component";
import { DelToutgenreComponent } from "./components/del-toutgenre/del-toutgenre/del-toutgenre.component";
import { DetailToutgenreComponent } from "./components/detail-toutgenre/detail-toutgenre/detail-toutgenre.component";
import { EditToutgenreComponent } from "./components/edit-toutgenre/edit-toutgenre/edit-toutgenre.component";
import { MainContentToutgenreComponent } from "./components/main-content-toutGenre/main-content-toutgenre/main-content-toutgenre.component";

const routes: Routes = [
  {
    path: '',
    data: {
      title: `Tout Genre`
    },
    children: [
      {
        path: '',
        redirectTo: '',
        pathMatch: 'full',
        component: MainContentToutgenreComponent,
        data: {
          title: $localize`Tout Genre`
        }
      },
      {
        path: 'addttgenre',
        component: AddToutgenreComponent,
        data: {
          title: $localize`Ajout Tout Genre`
        }
      },
      {
        path: 'editttgenre/:id',
        component: EditToutgenreComponent,
        data: {
          title: $localize`Modifier Tout Genre`
        }
      },
      {
        path: 'detailttgenre/:id',
        component:  DetailToutgenreComponent,
        data: {
          title: $localize`Detail Tout Genre`
        }
      },

    ]
  },

];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]

})
export class ToutGenreRoutingModule {

}
