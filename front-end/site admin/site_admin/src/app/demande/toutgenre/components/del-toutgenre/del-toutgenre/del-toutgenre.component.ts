import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToutGenre } from '../../models/ToutGenre';
import { ToutGenreService } from '../../service/toutGenre.service';

@Component({
  selector: 'app-del-toutgenre',
  templateUrl: './del-toutgenre.component.html',
  styleUrls: ['./del-toutgenre.component.scss']
})
export class DelToutgenreComponent implements OnInit {

  ttGenres :any;
  ttGenreArchive: any;
  closeResult = '';
  id!:number
  currentMotif:any

  constructor(  private ttgenreService: ToutGenreService,
                private router : Router,
                private modalService: NgbModal,
     ) {

    this.ttGenres = ttgenreService.listTtGenre();
   }
  ngOnInit(): void {
    this.getArchiveToutGenres()
  }
  getArchiveToutGenres(){
    this.ttgenreService.getToutGenres().subscribe({
      next : (res)=>{
        this.ttGenreArchive=res
      }
    })
  }
  getMotif(id:number){
    this.currentMotif=this.ttgenreService.getMotif(id).subscribe((response)=>{
      this.currentMotif=response
    })
  }
  openSm(content: any, id: number) {
    this.id=id
    this.getMotif(id)
    this.modalService.open(content,{ size: 'sm' }).result.then((result: any) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason: any) => {
   //   this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

}
