import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToutGenre } from '../../models/ToutGenre';
import { ToutGenreService } from '../../service/toutGenre.service';

@Component({
  selector: 'app-main-content-toutgenre',
  templateUrl: './main-content-toutgenre.component.html',
  styleUrls: ['./main-content-toutgenre.component.scss']
})
export class MainContentToutgenreComponent implements OnInit {
  courentSection = "enCour";
  buttonText = "en cour"
  ttgenres: any
  btn = "warning";
  role = 'guene';
  rvForm1!: FormGroup;
  ttcurrent: any;
  compt = 0;
  closeResult = '';
  lock = 1;
  id: any;
  motif: any
  submitForm = false;
  constructor(private ttgenreService: ToutGenreService,
    private router: Router,
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
  ) {
    this.ttgenres = ttgenreService.listTtGenre();
  }

  ngOnInit(): void {
    this.getToutGenres()
  }
  getToutGenres() {
    this.ttgenreService.getToutGenres().subscribe({
      next: (res) => {
        this.ttgenres = res
        console.log(this.ttgenres)
      }

    })
  }
  open(content: any, id: number) {
    this.initForm()
    this.id = id
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      console.log(reason)
    });
  }
  initForm() {
    this.rvForm1 = this.formBuilder.group({
      motifRejet: ['', Validators.required],
    })
  }
  // pour rejet une demande il faut cree un motif puis son id dans la table renouvellement
  supprimerToutGenre(): void {
    console.log(this.id)
    const formValue = this.rvForm1.value
    this.ttgenreService.createMotif(formValue.motifRejet).subscribe((response) => {
      this.motif = response.id
      console.log(response.id)

      this.ttgenreService.getToutGenre(this.id).subscribe((response: any) => {
        this.ttcurrent = response;
        this.ttgenreService.SupprimerToutGenre(
          this.ttcurrent?.id,
          this.ttcurrent?.numeroRegistre,
          this.ttcurrent?.annee,
          this.ttcurrent?.nombreExemplaire,
          this.ttcurrent?.adresse,
          this.ttcurrent?.cerificat,
          this.ttcurrent?.agent,
          this.motif
        ).subscribe({
          next: (res) => {
            console.log(res);
            this.getToutGenres()
          },
          error: (e) => console.error(e)
        });
      });
    })
  }


  nonterminerToutGenre(id: number): void {
    this.ttgenreService.getToutGenre(id).subscribe((response: any) => {
      this.ttcurrent = response;
      this.ttgenreService.terminerToutGenre(
        this.ttcurrent?.id,
        this.ttcurrent?.numeroRegistre,
        this.ttcurrent?.annee,
        this.ttcurrent?.nombreExemplaire,
        this.ttcurrent?.adresse,
        this.ttcurrent?.cerificat,
        this.ttcurrent?.agent,
        this.ttcurrent?.motif,
        false
      ).subscribe({
        next: (res) => {
          console.log(res);
          this.getToutGenres()
        },
        error: (e) => console.error(e)
      });
    });
  }





  // retourId(n: number): void {
  //   this.ttcurrent = this.ttgenres.find(p => p.id == n)
  //   if (this.ttcurrent['lock'] === 0) {
  //     this.ttcurrent['lock'] = 1;
  //   } else {
  //     this.ttcurrent['lock'] = 0;
  //   }
  // }

  changeSection(section: string) {
    this.courentSection = section;
  }
  // changeEtat(n : number): void{
  //   this.ttcurrent= this.ttgenres.find(p => p.id == n)
  //     if(this.ttcurrent['etat']=== 'en cour'){
  //       this.ttcurrent['etat']= 'terminer'
  //       }else{
  //         this.ttcurrent['etat']= 'en cour';
  //       }
  //   }




}
