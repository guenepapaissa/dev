import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { UserService } from 'src/app/user/components/services/user.service';
import { ToutGenre } from '../../models/ToutGenre';
import { ToutGenreService } from '../../service/toutGenre.service';


@Component({
  selector: 'app-edit-toutgenre',
  templateUrl: './edit-toutgenre.component.html',
  styleUrls: ['./edit-toutgenre.component.scss']
})
export class EditToutgenreComponent implements OnInit {


  id !: number;
  currentTtgenre: any;
  active = 1;
  closeResult = '';
  client!:any;
  rvForm1!: FormGroup;
  motif: any;
  currentMotif:any
  constructor(private activatedRoute: ActivatedRoute,
    private route: Router,
    private ttGenreService: ToutGenreService,
    private modalService: NgbModal,
    private userService: UserService,
    private router: Router,
    private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.params['id'];
   this.getToutGenre(this.id)
   this.initForm()
  }
  getToutGenre(id: number) {
    this.ttGenreService.getToutGenre(id).subscribe((response: any) => {
      this.currentTtgenre = response;
      this.getClient()
        this.getMotif(this.currentTtgenre.motif)
      this.initForm()
    });
    this.initForm();
  }
  getClient(){
    this.userService.getUser(this.currentTtgenre?.client).subscribe((response)=>{
      this.client=response
    })
   }
   getMotif(id:number){
    this.currentMotif=this.ttGenreService.getMotif(id).subscribe((response)=>{
      this.currentMotif=response
    })
  }
  supprimer(motifRejet: string) {

    this.currentTtgenre.motifRejet = motifRejet;
    this.route.navigate(['/ttgenre'])
  }
  recuperer() {
    this.currentTtgenre.motifRejet = '';
    this.route.navigate(['/ttgenre'])
  }
  terminerToutGenre(): void {
    this.ttGenreService.getToutGenre(this.id).subscribe((response: any) => {
      this.currentTtgenre = response;
      this.ttGenreService.terminerToutGenre(
        this.currentTtgenre?.id,
        this.currentTtgenre?.numeroRegistre,
        this.currentTtgenre?.annee,
        this.currentTtgenre?.nombreExemplaire,
        this.currentTtgenre?.adresse,
        this.currentTtgenre?.cerificat,
        this.currentTtgenre?.agent,
        this.currentTtgenre?.motif,
        true
      ).subscribe({
        next: (res) => {
          console.log(res);
          this.route.navigate(['/ttgenre'])
        },
        error: (e) => console.error(e)
      });
    });
  }
  supprimerToutGenre(): void {
    console.log(this.id)
    const formValue = this.rvForm1.value
    this.ttGenreService.createMotif(formValue.motifRejet).subscribe((response)=>{
      this.motif=response.id
      console.log(response.id)
    })
    this.ttGenreService.getToutGenre(this.id).subscribe((response: any) => {
      this.currentTtgenre = response;
      this.ttGenreService.SupprimerToutGenre(
        this.currentTtgenre?.id,
        this.currentTtgenre?.numeroRegistre,
        this.currentTtgenre?.annee,
        this.currentTtgenre?.nombreExemplaire,
        this.currentTtgenre?.adresse,
        this.currentTtgenre?.cerificat,
        this.currentTtgenre?.agent,
        this.motif
      ).subscribe({
        next: (res) => {
          console.log(res);
          this.route.navigate(['/ttgenre'])
        },
        error: (e) => console.error(e)
      });
    });
  }

  recupererToutGenre(): void {
    this.ttGenreService.getToutGenre(this.id).subscribe((response: any) => {
      this.currentTtgenre = response;
      this.ttGenreService.getEnleverMotif(
        this.currentTtgenre?.id).subscribe({
        next: (res) => {
          console.log(res);
         this.router.navigate(['/ttgenre'])
        },
        error: (e) => console.error(e)
      });
    });
  }
  recupererback() {
    // recuperation demande declaration Naissance
    this.ttGenreService.getToutGenre(this.currentTtgenre.id).subscribe((response: any) => {
      this.currentTtgenre = response;
      console.log(this.currentTtgenre)
      this.ttGenreService.changeEtatToutGenre(
        this.currentTtgenre?.id,
        this.currentTtgenre?.prenom,
        this.currentTtgenre?.nom,
        this.currentTtgenre?.telephone,
        this.currentTtgenre?.email,
        this.currentTtgenre?.login,
        this.currentTtgenre?.password,
        this.currentTtgenre?.typeAgent,
        false
      ).subscribe({
        next: (res) => {
          console.log(res);
          this.route.navigate(["/agent"]);
        },
        error: (e) => console.error(e)
      });
    });
  }
  terminer() {
    this.currentTtgenre.etat = 'terminer'
    this.route.navigate(['/ttgenre'])
  }
  initForm() {
    this.rvForm1 = this.formBuilder.group({
      motifRejet: ['', Validators.required],
    })
  }
  open(content: any) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  onSave() {
    // suppression demande
    const formValue = this.rvForm1.value;
    const newRecurrentTtgenre = new ToutGenre(
      this.currentTtgenre.id,
      this.currentTtgenre.lock,
      this.currentTtgenre.numrenouvellement,
      this.currentTtgenre.nom,
      this.currentTtgenre.prenom,
      this.currentTtgenre.adresse,
      this.currentTtgenre.retrait,
      this.currentTtgenre.num,
      this.currentTtgenre.papier,
      this.currentTtgenre.dateDebut,
      this.currentTtgenre.etat,
      formValue['motifRejet'],
      this.currentTtgenre.dateRv,
    )
    console.log(newRecurrentTtgenre);
    this.supprimer(formValue['motifRejet']);
  }
}


