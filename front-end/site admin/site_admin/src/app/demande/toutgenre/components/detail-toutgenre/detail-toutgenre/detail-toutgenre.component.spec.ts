import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailToutgenreComponent } from './detail-toutgenre.component';

describe('DetailToutgenreComponent', () => {
  let component: DetailToutgenreComponent;
  let fixture: ComponentFixture<DetailToutgenreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailToutgenreComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailToutgenreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
