import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { AgentService } from 'src/app/agent/components/service/agent.service';
import { UserService } from 'src/app/user/components/services/user.service';
import { ToutGenre } from '../../models/ToutGenre';
import { ToutGenreService } from '../../service/toutGenre.service';

@Component({
  selector: 'app-detail-toutgenre',
  templateUrl: './detail-toutgenre.component.html',
  styleUrls: ['./detail-toutgenre.component.scss']
})
export class DetailToutgenreComponent implements OnInit {
  id !:number;
  currentTtgenre :any;
  active = 1;
  motif: any;
  currentMotif:any
  client!:any;
  currentAgent: any
  profil=localStorage.getItem('profil')

  constructor(private activatedRoute: ActivatedRoute,
     private ttGenreService : ToutGenreService,
     private userService: UserService,
     private agentService: AgentService
     ) { }

     ngOnInit(): void {
      this.id = this.activatedRoute.snapshot.params['id'];
     this.getToutGenre(this.id)
    }
    getToutGenre(id: number) {
      this.ttGenreService.getToutGenre(id).subscribe((response: any) => {
        this.currentTtgenre = response;
        this.getClient()
        this.getMotif(this.currentTtgenre.motif)
        this.getAgent(this.currentTtgenre.agent)

      });

    }
    getClient(){
      this.userService.getUser(this.currentTtgenre?.client).subscribe((response)=>{
        this.client=response
      })
     }
     getMotif(id:number){
      this.currentMotif=this.ttGenreService.getMotif(id).subscribe((response)=>{
        this.currentMotif=response
      })
    }
      getAgent(id:number){
        this.agentService.getAgent(id).subscribe((response)=>{
          this.currentAgent=response
        })
      }

}

