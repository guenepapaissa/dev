import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DelToutgenreComponent } from './del-toutgenre.component';

describe('DelToutgenreComponent', () => {
  let component: DelToutgenreComponent;
  let fixture: ComponentFixture<DelToutgenreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DelToutgenreComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DelToutgenreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
