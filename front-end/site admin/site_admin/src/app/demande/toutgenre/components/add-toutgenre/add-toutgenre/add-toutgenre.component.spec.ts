import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddToutgenreComponent } from './add-toutgenre.component';

describe('AddToutgenreComponent', () => {
  let component: AddToutgenreComponent;
  let fixture: ComponentFixture<AddToutgenreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddToutgenreComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddToutgenreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
