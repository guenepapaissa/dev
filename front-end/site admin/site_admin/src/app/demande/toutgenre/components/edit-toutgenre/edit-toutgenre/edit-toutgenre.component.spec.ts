import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditToutgenreComponent } from './edit-toutgenre.component';

describe('EditToutgenreComponent', () => {
  let component: EditToutgenreComponent;
  let fixture: ComponentFixture<EditToutgenreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditToutgenreComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditToutgenreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
