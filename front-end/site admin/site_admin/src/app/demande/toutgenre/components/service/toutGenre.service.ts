import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ToutGenre } from '../models/ToutGenre';



@Injectable({
  providedIn: 'root'
})
export class ToutGenreService {
  ttGenre!: ToutGenre[];
  ttGenrecurrent : any;
  api= environment.api
  agent= <any> localStorage.getItem('idUser')

  constructor(private http : HttpClient) {
    // this.ttGenre = [
    //   {
    //     id:  1,
    //     lock: 0,
    //     numttgenre: "ttGenre001",
    //     nom: "Deme",
    //     prenom: "Dieynaba",
    //     adresse: "pout",
    //     num: "7777777777",
    //     papier: "Celibat",
    //     retrait:"sur place",
    //     dateDebut: new Date,
    //     dateRv: new Date("03-01-2022"),
    //     etat: "en cour",

    //   },
    //   {
    //     id: 2,
    //     lock: 1,
    //     numttgenre: "ttGenre002",
    //     nom: "Cana",
    //     prenom: "you",
    //     adresse: "thies",
    //     num: "7777777777",
    //     papier: "Residence",
    //     retrait:"sur place",
    //     dateDebut: new Date(),
    //     etat: "en cour",
    //     dateRv: new Date,


    //   },
    //   {
    //     id: 3,
    //     lock:  1,
    //     numttgenre: "ttGenre003",
    //     nom: "Dione",
    //     prenom: "Pape Laye",
    //     adresse: "thies",
    //     num: "7777777777",
    //     dateDebut: new Date(),
    //     retrait:"livraison",
    //     papier: "vie collectif",
    //     dateRv: new Date("03-01-2022"),
    //     etat: "terminer",

    //   },
    //   {
    //     id:  4,
    //     lock : 0,
    //     numttgenre: "ttGenre004",
    //     nom: "Faye",
    //     prenom: "Assane",
    //     adresse: "khombol",
    //     num: "7777777777",
    //     retrait:"sur place",
    //     papier: "Residence",
    //     etat: "en cour",
    //     dateDebut: new Date,
    //   }
    // ]

  }


  getToutGenres(){
    return this.http.get<any>(this.api+'toutGenre/')
  }
  getToutGenre(id: any){
    return this.http.get<any>(this.api+'toutGenre/'+id)
  }
  createMotif(motif:string){
    return this.http.post<any>(this.api+'motif/',{motif:motif})
  }
  getMotif(id: any){
    return this.http.get<any>(this.api+'motif/'+id)
  }
  getEnleverMotif(id: any){
    return this.http.get<any>(this.api+'eneleverMotifGenre/'+id)
  }
  terminerToutGenre(id: any,numeroRegistre: number,annee: number,nombreExemplaire:number,adresse:string, cerificat:String,agent:any, motif: any, etat:boolean){
    let update = {
      "numeroRegistre": numeroRegistre,
      "annee": annee,
      "nombreExemplaire": nombreExemplaire,
      "cerificat": cerificat,
      "adresse": adresse,
      "agent":this.agent,
      "motif": motif,
      "etat":etat
    }
    return this.http.put<any>(`${this.api}toutGenre/${id}`,update);
  }
   SupprimerToutGenre(id: any,numeroRegistre: number,annee: number,nombreExemplaire:number,adresse:string, cerificat:String,agent:any, motif: any){
    let update = {
      "numeroRegistre": numeroRegistre,
      "annee": annee,
      "nombreExemplaire": nombreExemplaire,
      "cerificat": cerificat,
      "adresse": adresse,
      "agent":this.agent,
      "motif": motif,
    }
    return this.http.put<any>(`${this.api}toutGenre/${id}`,update);
  }
  createToutGenre(prenom: string, nom: string, telephone: string, email: string, login: string, password: string){
    let create = {
      "numeroRegistre": prenom,
      "anne": nom,
      "nombreExemplaire": telephone,
      "etatRetrait": email,
      "idDemande": login,
      "date": password,
      "motifRejet": password,
      "etatDemande": password,

    }
    return this.http.put(`${this.api}toutGenre/`,create);
  }
  changeEtatToutGenre(id: any,prenom: string, nom: string, telephone: string, email: string, login: string, password: string, typeAgent: string,etat:boolean){
    let update = {
      "prenom": prenom,
      "nom": nom,
      "telephone": telephone,
      "email": email,
      "login": login,
      "password": password,
      "typeAgent": typeAgent,
      "etat":etat
    }
    return this.http.put(`${this.api}toutGenre/${id}`,update);
  }


  listTtGenre(){
    return this.ttGenre;
  }
  // listAgent(): Agent[] {
  //   return this.agent;
  // }
  editerttGenre(idttGenre: number): ToutGenre[] {
    this.ttGenrecurrent = this.ttGenre.find(p => p.id == idttGenre)
    return this.ttGenrecurrent;
  }

  // detailAgent(idUser: number): Agent[] {
  //   this.agentcurrent = this.agent.find(p => p.id == idUser)
  //   return this.agentcurrent;
  // }


}
