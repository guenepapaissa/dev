import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MainContentToutgenreComponent } from './main-content-toutgenre.component';

describe('MainContentToutgenreComponent', () => {
  let component: MainContentToutgenreComponent;
  let fixture: ComponentFixture<MainContentToutgenreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MainContentToutgenreComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainContentToutgenreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
