import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Mariage } from '../../../models/DeclarationMariage';
import { MariageService } from '../../../service/declarationMariage.service';
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/user/components/services/user.service';


@Component({
  selector: 'app-main-containt-dec-mariage',
  templateUrl: './main-containt-dec-mariage.component.html',
  styleUrls: ['./main-containt-dec-mariage.component.scss']
})
export class MainContaintDecMariageComponent implements OnInit {


  courentSection = "nvelle"
  buttonText = "en cour"
  mariages !:any;
  btn = "warning";
  role = 'guene'
  compt = 0;
  closeResult = '';
  active = 1;
  mariagecurrent: any;
  id: any;
  submitForm!: boolean;
  rvForm1!: FormGroup;
  client: any
  currentClient: any
  motif: any

  constructor(private mariageService: MariageService,
    private router: Router,
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private userService: UserService,
  ) {

  }

  ngOnInit(): void {
    this.initForm()
    this.getMariages()

  }
  getMariages(){
    this.mariageService.getMariages().subscribe({
      next: (res)=>{
        this.mariages=res;
        console.log(res)
      }
    })
  }

  getClient(id: number) {
    this.userService.getUser(id).subscribe({
      next: (res) => {
        this.currentClient = res;
      }
    })
  }
  open(content: any, id: number) {
    this.id = id
    this.initForm()
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      console.log(reason)
    });
  }
  supprimerMariage(): void {
    // creation de la motif
    const formValue = this.rvForm1.value
    this.mariageService.createMotif(formValue.motifRejet).subscribe((response) => {
      this.motif = response.id
      console.log(response.id)

    // insertion de la motif
    this.mariageService.getMariage(this.id).subscribe((response: any) => {
      this.mariagecurrent = response;
      this.mariageService.SupprimerMariage(
        this.mariagecurrent?.id,
        this.mariagecurrent?.dateEvenement,
        this.mariagecurrent?.extrait,
        this.mariagecurrent?.agent,
        this.mariagecurrent?.client,
        this.motif
      ).subscribe({
        next: (res) => {
          console.log(res);
          this.getMariages()
        },
        error: (e) => console.error(e)
      });
    });
  });
  }

  nonterminerMariage(id: number){
    this.mariageService.getMariage(id).subscribe((response: any) => {
      this.mariagecurrent = response;
      this.mariageService.changeEtatMariage(
        this.mariagecurrent?.id,
        this.mariagecurrent?.dateEvenement,
        this.mariagecurrent?.extrait,
        this.mariagecurrent?.agent,
        this.mariagecurrent?.client,
        false
      ).subscribe({
        next: (res) => {
          console.log(res);
          this.getMariages()
        },
        error: (e) => console.error(e)
      });
    });
  }
  terminerMariage(id: number){
    this.mariageService.getMariage(id).subscribe((response: any) => {
      this.mariagecurrent = response;
      this.mariageService.changeEtatMariage(
        this.mariagecurrent?.id,
        this.mariagecurrent?.dateEvenement,
        this.mariagecurrent?.extrait,
        this.mariagecurrent?.agent,
        this.mariagecurrent?.client,
        true
      ).subscribe({
        next: (res) => {
          console.log(res);
          this.getMariages()

        },
        error: (e) => console.error(e)
      });
    });
  }

  changeSection(section: string) {
    this.courentSection = section;
  }

  supprimer(n: number): void {
    //this.mariagecurrent = this.mariages.find(p => p.id == n)
    this.mariagecurrent.motifRejet = prompt(" Donnez Motif ");
  }
  initForm() {
    this.rvForm1 = this.formBuilder.group({
      motifRejet: ['', Validators.required],
    })
  }

  retourId(n: number): void {
   // this.mariagecurrent = this.mariages.find(p => p.id == n)
    if (this.mariagecurrent['etat'] === 'en cour') {
      this.mariagecurrent['etat'] = 'terminer'
    } else {
      this.mariagecurrent['etat'] = 'en cour';
    }
  }


  fin = new Date();
  duree = this.fin.getSeconds();

}

