import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddContaintDeclarationMariageComponent } from './add-containt-declaration-mariage.component';

describe('AddContaintDeclarationMariageComponent', () => {
  let component: AddContaintDeclarationMariageComponent;
  let fixture: ComponentFixture<AddContaintDeclarationMariageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddContaintDeclarationMariageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddContaintDeclarationMariageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
