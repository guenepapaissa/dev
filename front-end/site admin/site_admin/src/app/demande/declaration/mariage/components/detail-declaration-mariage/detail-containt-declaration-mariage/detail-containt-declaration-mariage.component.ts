import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/user/components/services/user.service';
import { MariageService } from '../../../service/declarationMariage.service';

@Component({
  selector: 'app-detail-containt-declaration-mariage',
  templateUrl: './detail-containt-declaration-mariage.component.html',
  styleUrls: ['./detail-containt-declaration-mariage.component.scss']
})
export class DetailContaintDeclarationMariageComponent implements OnInit {
  client!: any;
  motif: any;
  currentMotif: any
  currentmariage: any;
  id !: number;
  constructor(private userService: UserService,
    private activatedRoute: ActivatedRoute,
    private mariageService: MariageService,
    private route: Router,) {
    this.id = this.activatedRoute.snapshot.params['id'];
  }

  ngOnInit(): void {
    this.getMariage(this.id)
  }
  getMariage(id: number) {
    this.mariageService.getMariage(id).subscribe((response: any) => {
      this.currentmariage = response;
      this.getClient()
      if (this.currentmariage.motif)
        this.getMotif(this.currentmariage.motif)
    });
  }
  getMotif(id: number) {
    this.currentMotif = this.mariageService.getMotif(id).subscribe((response) => {
      this.currentMotif = response
    })
  }
  getClient() {
    this.userService.getUser(this.currentmariage?.client).subscribe((response) => {
      this.client = response
      console.log(this.client)
    })
  }

  recupererMariage(): void {
    this.mariageService.getMariage(this.id).subscribe((response: any) => {
      this.currentmariage = response;
      this.mariageService.getEnleverMotif(
        this.currentmariage?.id).subscribe({
          next: (res) => {
            console.log(res);
            this.route.navigate(['/mariage'])
          },
          error: (e) => console.error(e)
        });
    });
  }

}
