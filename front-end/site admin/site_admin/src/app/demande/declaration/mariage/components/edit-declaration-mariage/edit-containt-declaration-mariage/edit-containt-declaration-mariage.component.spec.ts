import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditContaintDeclarationMariageComponent } from './edit-containt-declaration-mariage.component';

describe('EditContaintDeclarationMariageComponent', () => {
  let component: EditContaintDeclarationMariageComponent;
  let fixture: ComponentFixture<EditContaintDeclarationMariageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditContaintDeclarationMariageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditContaintDeclarationMariageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
