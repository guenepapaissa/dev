import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailContaintDeclarationMariageComponent } from './detail-containt-declaration-mariage.component';

describe('DetailContaintDeclarationMariageComponent', () => {
  let component: DetailContaintDeclarationMariageComponent;
  let fixture: ComponentFixture<DetailContaintDeclarationMariageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailContaintDeclarationMariageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailContaintDeclarationMariageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
