import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Mariage } from '../../../models/DeclarationMariage';
import { MariageService } from '../../../service/declarationMariage.service';

@Component({
  selector: 'app-del-containt-declaration-mariage',
  templateUrl: './del-containt-declaration-mariage.component.html',
  styleUrls: ['./del-containt-declaration-mariage.component.scss']
})
export class DelContaintDeclarationMariageComponent implements OnInit {
  mariages !: any;
  mariagecurrent: any;
  closeResult = '';
  id!:number
  currentMotif:any

  constructor(  private mariageService: MariageService,
                private router : Router,
                private modalService: NgbModal,
     ) {   }
  ngOnInit(): void {
    this.getArchiveMariage();
  }
  getArchiveMariage(){
    this.mariageService.getMariages().subscribe({
      next : (res)=>{
        this.mariages=res;
        console.log(this.mariages)
      }
    })
  }
  getMotif(id:number){
    this.currentMotif=this.mariageService.getMotif(id).subscribe((response)=>{
      this.currentMotif=response
    })
  }
  openSm(content: any, id: number) {
    this.id=id
    this.getMotif(id)
    this.modalService.open(content,{ size: 'sm' }).result.then((result: any) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason: any) => {
   //   this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

}
