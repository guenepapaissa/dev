export class Mariage{
  constructor(
    public id : number,
    public numMariage: string,
    public nom: string,
    public prenom: string,
    public adresse: string,
    public num: string,
    public dateDebut: Date,
    public heure: Date,
    public motifRejet?: string,
    public etat?: string,
    public dateRv?: Date,
  ){}
}
