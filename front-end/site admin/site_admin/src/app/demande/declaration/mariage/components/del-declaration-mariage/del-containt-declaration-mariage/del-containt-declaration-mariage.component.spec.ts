import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DelContaintDeclarationMariageComponent } from './del-containt-declaration-mariage.component';

describe('DelContaintDeclarationMariageComponent', () => {
  let component: DelContaintDeclarationMariageComponent;
  let fixture: ComponentFixture<DelContaintDeclarationMariageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DelContaintDeclarationMariageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DelContaintDeclarationMariageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
