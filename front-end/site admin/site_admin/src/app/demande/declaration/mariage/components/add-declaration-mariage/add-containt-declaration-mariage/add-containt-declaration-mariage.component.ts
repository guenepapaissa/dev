import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/user/components/services/user.service';
import { Mariage } from '../../../models/DeclarationMariage';
import { MariageService } from '../../../service/declarationMariage.service';

@Component({
  selector: 'app-add-containt-declaration-mariage',
  templateUrl: './add-containt-declaration-mariage.component.html',
  styleUrls: ['./add-containt-declaration-mariage.component.scss']
})
export class AddContaintDeclarationMariageComponent implements OnInit {

  id !: number;
  currentmariage: any;
  closeResult = '';
  rvForm!: FormGroup;
  newMariage !: Mariage[];
  courentbouton = 'rv';
  rvForm1!: FormGroup;
  client!: any;
  motif: any;
  currentMotif: any

  constructor(private activatedRoute: ActivatedRoute,
    private route: Router,
    private mariageService: MariageService,
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private toastr: ToastrService,
  ) { }

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.params['id'];
    this.getMariage(this.id)
  }
  getMariage(id: number) {
    this.mariageService.getMariage(id).subscribe((response: any) => {
      this.currentmariage = response;
      this.getClient()
      if (this.currentmariage.motif)
        this.getMotif(this.currentmariage.motif)
      this.initForm();
    });
    this.initForm();
  }
  getMotif(id: number) {
    this.currentMotif = this.mariageService.getMotif(id).subscribe((response) => {
      this.currentMotif = response
    })
  }
  getClient() {
    this.userService.getUser(this.currentmariage?.client).subscribe((response) => {
      this.client = response
      console.log(this.client)
    })
  }
  SupprimerMariage(): void {
    console.log(this.id)
    const formValue = this.rvForm1.value
    this.mariageService.createMotif(formValue.motifRejet).subscribe((response)=>{
      this.motif=response.id
      console.log(response.id)
    })
    this.mariageService.getMariage(this.id).subscribe((response: any) => {
      this.currentmariage = response;
      this.mariageService.SupprimerMariage(
        this.currentmariage?.id,
        this.currentmariage?.dateEvenement,
        this.currentmariage?.extrait,
        this.currentmariage?.agent,
        this.currentmariage?.client,
        this.motif
      ).subscribe({
        next: (res) => {
          console.log(res);
          this.route.navigate(['/mariage'])
        },
        error: (e) => console.error(e)
      });
    });
  }
  rendezVousMariage(): void {
    // cette methode permet de modier l'etat de la demande, encour <=>  terminer
    const formValue = this.rvForm.value;
    const x = new Date(this.rvForm.value.dateRv);// date rendez vous fixer par l'agent
      const y = new Date();//today
      const z = x.toDateString().substring(0, 3)// recuperation du jour en string (mon, tue....)
      console.log(x)
      console.log(y)
      console.log(z)
      if (x > y && z != 'Sat' && z != 'Sun') {
    this.mariageService.getMariage(this.id).subscribe((response: any) => {
      this.currentmariage = response;
      console.log(this.currentmariage)
      this.mariageService.rendezVousMariage(
        this.currentmariage?.id,
        this.currentmariage?.dateEvenement,
        this.currentmariage?.extrait,
        this.currentmariage?.agent,
        this.currentmariage?.client,
        formValue.dateRv,
      ).subscribe({
        next: (res) => {
          console.log(res);
          this.route.navigate(['/mariage'])
        },
        error: (e) => console.error(e)
      });
    });
  }
  else{
    this.toastr.warning('Date incorrect !', 'Rendez-vous');
  }
  }
  supprimer(motifRejet: string) {

    this.currentmariage.motifRejet = motifRejet;
    this.route.navigate(['/mariage'])
  }
  recuperer() {

    this.currentmariage.motifRejet = '';
    this.route.navigate(['/mariage'])
  }

  open(content: any, modal: string) {
    this.courentbouton = modal;
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      // this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  initForm() {
    this.rvForm = this.formBuilder.group({
      dateRv: ['', Validators.required],
      etat: ['en cour', Validators.required],
    })
    this.rvForm1 = this.formBuilder.group({
      motifRejet: ['', Validators.required],
    })
  }
  changeModal(modal: string) {
    return this.courentbouton = modal;
  }
  changeMol(modal: string) {

    return false;
  }
  onSave() {
    if (this.courentbouton === 'fixer') {
      // fixer rv
      const x = new Date(this.rvForm.value['dateRv']);
      const y = new Date(this.currentmariage.dateDebut);
      const z = x.toDateString().substring(0, 3)
      console.log(z)
      if (x > y && z != 'Sat' && z != 'Sun') {
        alert("date correct");
        const formValue = this.rvForm.value;
        const newMariage = new Mariage(
          this.currentmariage.id,
          this.currentmariage.numMariage,
          this.currentmariage.nom,
          this.currentmariage.prenom,
          this.currentmariage.adresse,
          this.currentmariage.num,
          this.currentmariage.dateDebut,
          this.currentmariage.heure,
          this.currentmariage.motifRejet,
          formValue['etat'],
          formValue['dateRv'],
        )
        console.log(newMariage);
        this.mariageService.ajoutermariage(newMariage)
        this.route.navigate(['/mariage'])
      }
      else {
        alert("date incorrect");
        this.route.navigate(['/mariage/addmariage', this.currentmariage.id]);
      }
    }
    if (this.courentbouton === 'supprimer') {
      // suppression demande mariage
      console.log('newMariage');
      const formValue = this.rvForm1.value;
      const newMariage = new Mariage(
        this.currentmariage.id,
        this.currentmariage.numMariage,
        this.currentmariage.nom,
        this.currentmariage.prenom,
        this.currentmariage.adresse,
        this.currentmariage.num,
        this.currentmariage.dateDebut,
        this.currentmariage.heure,
        formValue['motifRejet'],
        this.currentmariage.etat,
        this.currentmariage.dateRv,
      )
      console.log(newMariage);
      this.supprimer(formValue['motifRejet']);
    }

  }

}


