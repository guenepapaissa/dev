import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MainContaintDecMariageComponent } from './main-containt-dec-mariage.component';

describe('MainContaintDecMariageComponent', () => {
  let component: MainContaintDecMariageComponent;
  let fixture: ComponentFixture<MainContaintDecMariageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MainContaintDecMariageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainContaintDecMariageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
