import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Mariage } from '../models/DeclarationMariage';


@Injectable({
  providedIn: 'root'
})
export class MariageService {
  mariage!: any;
  mariagecurrent : any;
  api= environment.api
  agent= <any> localStorage.getItem('idUser')

  constructor(private http : HttpClient) {
    // this.mariage = [
    //   {
    //     id:  1,
    //     numMariage: "mariage001",
    //     nom: "Deme",
    //     prenom: "Dieynaba",
    //     adresse: "pout",
    //     num: "7777777777",
    //     dateDebut: new Date,
    //     dateRv: new Date("03-01-2022"),
    //     etat: "en cour",

    //     heure: new Date,
    //   },
    //   {
    //     id: 2,
    //     numMariage: "mariage002",
    //     nom: "Cana",
    //     prenom: "you",
    //     adresse: "dakar",
    //     num: "7777777777",
    //     dateDebut: new Date(),
    //     dateRv: new Date,
    //     etat: "en cour",

    //     heure: new Date,
    //   },
    //   {
    //     id: 3,
    //     numMariage: "mariage003",
    //     nom: "Dione",
    //     prenom: "Pape Laye",
    //     adresse: "thies",
    //     num: "7777777777",
    //     dateDebut: new Date(),
    //     dateRv: new Date("03-01-2022"),
    //     etat: "terminer",
    //     heure: new Date,
    //   },
    //   {
    //     id:  4,
    //     numMariage: "mariage004",
    //     nom: "Faye",
    //     prenom: "Assane",
    //     adresse: "baol",
    //     num: "7777777777",
    //     dateDebut: new Date,
    //     etat: "en cour",

    //     heure: new Date,
    //   },
    //   {
    //     id:  5,
    //     numMariage: "mariage005",
    //     nom: "Mboup",
    //     prenom: "Magne",
    //     adresse: "lambaye",
    //     num: "7777777777",
    //     dateDebut: new Date,
    //     etat: "en cour",
    //     heure: new Date,
    //   },
    //   {
    //     id:  6,
    //     numMariage: "mariage006",
    //     nom: "Ndiaye",
    //     prenom: "Awa",
    //     adresse: "Ngoundaie",
    //     num: "7777777777",
    //     dateDebut: new Date,
    //     etat: "terminer",
    //     heure: new Date,
    //   }

    // ]

  }

  getMariages(){
    return this.http.get<any>(this.api+'declaration_mariage/')
  }
  getMariage(id: any){
    return this.http.get<any>(this.api+'declaration/'+id)
  }
  getEnleverMotif(id: any){
    return this.http.get<any>(this.api+'eneleverMotif/'+id)
  }
  createMotif(motif:string){
    return this.http.post<any>(this.api+'motif/',{motif:motif})
  }
  getMotif(id: any){
    return this.http.get<any>(this.api+'motif/'+id)
  }
  SupprimerMariage(id: any, dateEvenement: Date, extrait: string, agent:any,client:any,motif: any){

    let update = {
      "dateEvenement": dateEvenement,
      "extrait": extrait,
      "agent":this.agent,
      "client":client,
      "motif": motif,
    }
    return this.http.put<any>(`${this.api}declaration/${id}`,update);
  }
  changeEtatMariage(id: any, dateEvenement: Date, extrait: string, agent:any,client:any,etat: boolean){

    let update = {
      "dateEvenement": dateEvenement,
      "extrait": extrait,
      "agent":this.agent,
      "client":client,
      "etat": etat,
    }
    return this.http.put<any>(`${this.api}declaration/${id}`,update);
  }
  rendezVousMariage(id: any, dateEvenement: Date, extrait: string, agent:any,client:any,rendezVous: any){
    let update = {
      "dateEvenement": dateEvenement,
      "extrait": extrait,
      "agent":this.agent,
      "client":client,
      "rendezVous": rendezVous,
    }
    return this.http.put<any>(`${this.api}declaration/${id}`,update);
  }


  listmariage(){
    return this.mariage;
  }
  // listAgent(): Agent[] {
  //   return this.agent;
  // }


  // detailAgent(idUser: number): Agent[] {
  //   this.mariagecurrent = this.agent.find(p => p.id == idUser)
  //   return this.mariagecurrent;
  // }
  ajoutermariage(addmariage: Mariage){
    this.mariage.push(addmariage);
  }


}
