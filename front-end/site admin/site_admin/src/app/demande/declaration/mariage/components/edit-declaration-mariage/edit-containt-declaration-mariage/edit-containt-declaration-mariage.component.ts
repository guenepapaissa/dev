import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AgentService } from 'src/app/agent/components/service/agent.service';
import { UserService } from 'src/app/user/components/services/user.service';
import { MariageService } from '../../../service/declarationMariage.service';

@Component({
  selector: 'app-edit-containt-declaration-mariage',
  templateUrl: './edit-containt-declaration-mariage.component.html',
  styleUrls: ['./edit-containt-declaration-mariage.component.scss']
})
export class EditContaintDeclarationMariageComponent implements OnInit {

  id !: number;
  currentmariage: any;
  active = 1;
  client!:any;
  motif: any;
  currentMotif:any
  currentAgent:any
  profil=localStorage.getItem('profil')
  constructor(private activatedRoute: ActivatedRoute,
    private userService: UserService,
     private route : Router ,
     private mariageService : MariageService,
     private agentService: AgentService) { }

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.params['id'];
    this.getMariage(this.id)
  }
  getMariage(id: number) {
    this.mariageService.getMariage(id).subscribe((response: any) => {
      this.currentmariage = response;
      console.log(this.currentmariage)
      this.getClient()
      this.getAgent(this.currentmariage.agent)
      if(this.currentmariage.motif)
        this.getMotif(this.currentmariage.motif)


    });
  }
  getMotif(id:number){
    this.currentMotif=this.mariageService.getMotif(id).subscribe((response)=>{
      this.currentMotif=response
    })
  }
  getClient(){
    this.userService.getUser(this.currentmariage?.client).subscribe((response)=>{
      this.client=response
      console.log(this.client)
    })
   }
   getAgent(id:number){
     this.agentService.getAgent(id).subscribe((response)=>{
       this.currentAgent=response
     })
   }


}
