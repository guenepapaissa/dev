import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AddContaintDeclarationMariageComponent } from "./components/add-declaration-mariage/add-containt-declaration-mariage/add-containt-declaration-mariage.component";
import { DelContaintDeclarationMariageComponent } from "./components/del-declaration-mariage/del-containt-declaration-mariage/del-containt-declaration-mariage.component";
import { DetailContaintDeclarationMariageComponent } from "./components/detail-declaration-mariage/detail-containt-declaration-mariage/detail-containt-declaration-mariage.component";
import { EditContaintDeclarationMariageComponent } from "./components/edit-declaration-mariage/edit-containt-declaration-mariage/edit-containt-declaration-mariage.component";
import { MainContaintDecMariageComponent } from "./components/main-containt-dec-mariage/main-containt-dec-mariage/main-containt-dec-mariage.component";

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Declaration Mariage',
    },
    children: [
      {
        path: '',
        redirectTo :"",
        pathMatch:'full',
        component: MainContaintDecMariageComponent,
        data: {
          title:`Declaration Mariage`
        },
      },
      {
        path: 'addmariage/:id',
        component: AddContaintDeclarationMariageComponent,
        data: {
          title: $localize`Ajout Declaration Mariage`
        }
      },
      {
        path: 'editmariage/:id',
        component: EditContaintDeclarationMariageComponent,
        data: {
          title: $localize`Modifier Declaration Mariage`
        }
      },
      {
        path: 'detailmariage/:id',
        component:  DetailContaintDeclarationMariageComponent,
        data: {
          title: $localize`Detail Declaration Mariage`
        }
      },

    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]

})
export class MariageRoutingModule {

}
