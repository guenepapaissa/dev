import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/user/components/services/user.service';
import { Deces } from '../../models/DeclerationDeces';
import { DecesService } from '../../service/declerationDeces.service';

@Component({
  selector: 'app-add-declaration-deces',
  templateUrl: './add-declaration-deces.component.html',
  styleUrls: ['./add-declaration-deces.component.scss']
})
export class AddDeclarationDecesComponent implements OnInit {

  id !: number;
  currentDeces: any;
  closeResult = '';
  newDeces !: Deces[];
  courentbouton = 'rv';
  rvForm1!: FormGroup;
  rvForm!: FormGroup;
  client!: any;
  motif: any;
  currentMotif: any

  constructor(private activatedRoute: ActivatedRoute,
    private route: Router,
    private decesService: DecesService,
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private toastr: ToastrService,
  ) { }

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.params['id'];
    this.getDeces(this.id)
  }

  getDeces(id: number) {
    this.decesService.getDeces(id).subscribe((response: any) => {
      this.currentDeces = response;
      this.getClient()
      if (this.currentDeces.motif)
        this.getMotif(this.currentDeces.motif)

    });
    this.initForm();
  }
  getMotif(id: number) {
    this.currentMotif = this.decesService.getMotif(id).subscribe((response) => {
      this.currentMotif = response
    })
  }
  getClient() {
    this.userService.getUser(this.currentDeces?.client).subscribe((response) => {
      this.client = response
      console.log(this.client)
    })
  }
  SupprimerDeces(): void {
    console.log(this.id)
    const formValue = this.rvForm1.value
    this.decesService.createMotif(formValue.motifRejet).subscribe((response)=>{
      this.motif=response.id
      console.log(response.id)
    })
    this.decesService.getDeces(this.id).subscribe((response: any) => {
      this.currentDeces = response;
      this.decesService.SupprimerDeces(
        this.currentDeces?.id,
        this.currentDeces?.dateEvenement,
        this.currentDeces?.extrait,
        this.currentDeces?.agent,
        this.currentDeces?.client,
        this.motif
      ).subscribe({
        next: (res) => {
          console.log(res);
          this.route.navigate(['/deces'])
        },
        error: (e) => console.error(e)
      });
    });
  }
  rendezVousDeces(): void {
    // cette methode permet de modier l'etat de la demande, encour <=>  terminer
    const formValue = this.rvForm.value;
    const x = new Date(this.rvForm.value.dateRv);// date rendez vous fixer par l'agent
      const y = new Date();//today
      const z = x.toDateString().substring(0, 3)// recuperation du jour en string (mon, tue....)
      console.log(x)
      console.log(y)
      console.log(z)
      if (x > y && z != 'Sat' && z != 'Sun') {
    this.decesService.getDeces(this.id).subscribe((response: any) => {
      this.currentDeces = response;
      console.log(this.currentDeces)
      this.decesService.rendezVousDeces(
        this.currentDeces?.id,
        this.currentDeces?.dateEvenement,
        this.currentDeces?.extrait,
        this.currentDeces?.agent,
        this.currentDeces?.client,
        formValue.dateRv,
      ).subscribe({
        next: (res) => {
          console.log(res);
          this.route.navigate(['/deces'])
        },
        error: (e) => console.error(e)
      });
    });
  }
  else{
    this.toastr.warning('Date incorrect !', 'Rendez-vous');
  }
  }

  supprimer(motifRejet: string) {

    this.currentDeces.motifRejet = motifRejet
    this.route.navigate(['/deces'])
  }
  recuperer() {

    this.currentDeces.motifRejet = '';
    this.route.navigate(['/deces'])
  }

  open(content: any, modal: string) {
    this.courentbouton = modal;
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
    //  this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  // private getDismissReason(reason: any): string {
  //   if (reason === ModalDismissReasons.ESC) {
  //     return 'by pressing ESC';
  //   } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
  //     return 'by clicking on a backdrop';
  //   } else {
  //     return `with: ${reason}`;
  //   }
  // }
  initForm() {
    this.rvForm = this.formBuilder.group({
      dateRv: ['', Validators.required],
      etat: ['en cour', Validators.required],
    })
    this.rvForm1 = this.formBuilder.group({
      motifRejet: ['', Validators.required],
    })
  }
  changeModal(modal: string) {
    return this.courentbouton = modal;
  }
  changeMol(modal: string) {

    return false;
  }
  onSave() {
    if (this.courentbouton === 'fixer') {
      const x = new Date(this.rvForm.value['dateRv']);
      const y = new Date(this.currentDeces.dateDebut);
      const z = x.toDateString().substring(0, 3)
      console.log(z)
      if (x > y && z != 'Sat' && z != 'Sun') {
        alert("date correct");
        const formValue = this.rvForm.value;
        const newDeces = new Deces(
          this.currentDeces.id,
          this.currentDeces.numDeces,
          this.currentDeces.nom,
          this.currentDeces.prenom,
          this.currentDeces.adresse,
          this.currentDeces.num,
          this.currentDeces.dateDebut,
          this.currentDeces.heure,
          this.currentDeces.motifRejet,
          formValue['etat'],
          formValue['dateRv'],
        )
        console.log(this.newDeces);
        this.decesService.ajouterDeces(newDeces)
        this.route.navigate(['/deces'])
      }
      else {
        alert("date incorrect");
        this.route.navigate(['/deces/adddeces', this.currentDeces.id]);
      }
    }
    if (this.courentbouton === 'supprimer') {
      console.log('this.newDeces');
      const formValue = this.rvForm1.value;
      const newDeces = new Deces(
        this.currentDeces.id,
        this.currentDeces.numDeces,
        this.currentDeces.nom,
        this.currentDeces.prenom,
        this.currentDeces.adresse,
        this.currentDeces.num,
        this.currentDeces.dateDebut,
        this.currentDeces.heure,
        formValue['motifRejet'],
        this.currentDeces.etat,
        this.currentDeces.dateRv,
      )
      console.log(newDeces);
      this.supprimer(formValue['motifRejet'],);
    }

  }


}


