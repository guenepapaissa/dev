import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDeclarationDecesComponent } from './edit-declaration-deces.component';

describe('EditDeclarationDecesComponent', () => {
  let component: EditDeclarationDecesComponent;
  let fixture: ComponentFixture<EditDeclarationDecesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditDeclarationDecesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDeclarationDecesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
