import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DelDeclarationDecesComponent } from './del-declaration-deces.component';

describe('DelDeclarationDecesComponent', () => {
  let component: DelDeclarationDecesComponent;
  let fixture: ComponentFixture<DelDeclarationDecesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DelDeclarationDecesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DelDeclarationDecesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
