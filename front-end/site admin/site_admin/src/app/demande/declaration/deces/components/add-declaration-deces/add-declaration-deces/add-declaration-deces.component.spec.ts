import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDeclarationDecesComponent } from './add-declaration-deces.component';

describe('AddDeclarationDecesComponent', () => {
  let component: AddDeclarationDecesComponent;
  let fixture: ComponentFixture<AddDeclarationDecesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddDeclarationDecesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDeclarationDecesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
