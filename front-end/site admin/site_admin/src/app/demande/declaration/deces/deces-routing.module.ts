import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AddDeclarationDecesComponent } from "./components/add-declaration-deces/add-declaration-deces/add-declaration-deces.component";
import { DelDeclarationDecesComponent } from "./components/del-declaration-deces/del-declaration-deces/del-declaration-deces.component";
import { DetailDeclarationDecesComponent } from "./components/detail-declaration-deces/detail-declaration-deces/detail-declaration-deces.component";
import { EditDeclarationDecesComponent } from "./components/edit-declaration-deces/edit-declaration-deces/edit-declaration-deces.component";
import { MainContaintDecDecesComponent } from "./components/main-containt-dec-deces/main-containt-dec-deces/main-containt-dec-deces.component";

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Declaration Deces',
    },
    children: [
      {
        path: '',
        redirectTo :"",
        pathMatch:'full',
        component: MainContaintDecDecesComponent,
        data: {
          title:`Declaration Deces`
        },
      },
      {
        path: 'adddeces/:id',
        component: AddDeclarationDecesComponent,
        data: {
          title: $localize`Ajout Declaration Deces`
        }
      },
      {
        path: 'editdeces/:id',
        component:  EditDeclarationDecesComponent,
        data: {
          title: $localize`Modifier Declaration Deces`
        }
      },
      {
        path: 'detaildeces/:id',
        component: DetailDeclarationDecesComponent,
        data: {
          title: $localize`Detail Declaration Deces`
        }
      },

    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]

})
export class DecesRoutingModule {

}
