import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/user/components/services/user.service';
import { DecesService } from '../../service/declerationDeces.service';

@Component({
  selector: 'app-detail-declaration-deces',
  templateUrl: './detail-declaration-deces.component.html',
  styleUrls: ['./detail-declaration-deces.component.scss']
})
export class DetailDeclarationDecesComponent implements OnInit {
  client!:any;
  motif: any;
  currentMotif:any
  currentdeces: any;
  id !: number;
  constructor(private userService: UserService,
    private activatedRoute: ActivatedRoute,
    private decesService: DecesService,
     private route: Router,) {
      this.id = this.activatedRoute.snapshot.params['id']  }
      ngOnInit(): void {
        this.getDeces(this.id)
      }
      getDeces(id: number) {
        this.decesService.getDeces(id).subscribe((response: any) => {
          this.currentdeces = response;
          this.getClient()
          if(this.currentdeces.motif)
            this.getMotif(this.currentdeces.motif)
        });
      }
      getMotif(id:number){
        this.currentMotif=this.decesService.getMotif(id).subscribe((response)=>{
          this.currentMotif=response
        })
      }
      getClient(){
        this.userService.getUser(this.currentdeces?.client).subscribe((response)=>{
          this.client=response
          console.log(this.client)
        })
       }
       recupererDeces(): void {
        this.decesService.getDeces(this.id).subscribe((response: any) => {
          this.currentdeces = response;
          this.decesService.getEnleverMotif(
            this.currentdeces?.id).subscribe({
              next: (res) => {
                console.log(res);
                this.route.navigate(['/deces'])
              },
              error: (e) => console.error(e)
            });
        });
      }


}
