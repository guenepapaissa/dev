import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MainContaintDecDecesComponent } from './main-containt-dec-deces.component';

describe('MainContaintDecDecesComponent', () => {
  let component: MainContaintDecDecesComponent;
  let fixture: ComponentFixture<MainContaintDecDecesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MainContaintDecDecesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainContaintDecDecesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
