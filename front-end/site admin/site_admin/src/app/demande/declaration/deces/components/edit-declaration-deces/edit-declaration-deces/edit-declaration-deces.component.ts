import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AgentService } from 'src/app/agent/components/service/agent.service';
import { UserService } from 'src/app/user/components/services/user.service';
import { DecesService } from '../../service/declerationDeces.service';

@Component({
  selector: 'app-edit-declaration-deces',
  templateUrl: './edit-declaration-deces.component.html',
  styleUrls: ['./edit-declaration-deces.component.scss']
})
export class EditDeclarationDecesComponent implements OnInit {

  id !: number;
  currentDeces: any;
  active = 1;
  client!:any;
  motif: any;
  currentMotif:any
  currentAgent: any
  profil = localStorage.getItem('profil')
  constructor(private activatedRoute: ActivatedRoute,
    private userService: UserService,
     private route: Router,
     private agentService: AgentService,
      private decesService: DecesService) { }

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.params['id'];
    this.getDeces(this.id);
  }
  getDeces(id: number){
    this.decesService.getDeces(id).subscribe({
      next: (res)=>{
        this.currentDeces= res;
        this.getClient()
      if(this.currentDeces.motif)
        this.getMotif(this.currentDeces.motif)
        console.log(this.currentDeces)
      }
    })
  }
  getMotif(id:number){
    this.currentMotif=this.decesService.getMotif(id).subscribe((response)=>{
      this.currentMotif=response
    })
  }
  getClient(){
    this.userService.getUser(this.currentDeces?.client).subscribe((response)=>{
      this.client=response
      console.log(this.client)
    })
   }
   getAgent(id:number){
    this.agentService.getAgent(id).subscribe((response)=>{
      this.currentAgent=response
    })
  }

}


