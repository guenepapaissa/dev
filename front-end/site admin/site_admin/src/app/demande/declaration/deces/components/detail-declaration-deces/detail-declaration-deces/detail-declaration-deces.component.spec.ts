import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailDeclarationDecesComponent } from './detail-declaration-deces.component';

describe('DetailDeclarationDecesComponent', () => {
  let component: DetailDeclarationDecesComponent;
  let fixture: ComponentFixture<DetailDeclarationDecesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailDeclarationDecesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailDeclarationDecesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
