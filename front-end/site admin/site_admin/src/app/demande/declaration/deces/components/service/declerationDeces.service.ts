import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Deces } from '../models/DeclerationDeces';


@Injectable({
  providedIn: 'root'
})
export class DecesService {
  deces!: Deces[];
  decescurrent : any;
  api= environment.api
  agent= <any> localStorage.getItem('idUser')

  constructor(private http : HttpClient) {
    // this.deces = [
    //   {
    //     id:  1,
    //     numDeces: "deces001",
    //     nom: "Deme",
    //     prenom: "Dieynaba",
    //     adresse: "pout",
    //     num: "7777777777",
    //     dateDebut: new Date,
    //     dateRv: new Date("03-01-2022"),
    //     etat: "en cour",

    //     heure: new Date,
    //   },
    //   {
    //     id: 2,
    //     numDeces: "deces002",
    //     nom: "Cana",
    //     prenom: "you",
    //     adresse: "dakar",
    //     num: "7777777777",
    //     dateDebut: new Date(),
    //     dateRv: new Date,
    //     etat: "en cour",

    //     heure: new Date,
    //   },
    //   {
    //     id: 3,
    //     numDeces: "deces003",
    //     nom: "Dione",
    //     prenom: "Pape Laye",
    //     adresse: "thies",
    //     num: "7777777777",
    //     dateDebut: new Date(),
    //     dateRv: new Date("03-01-2022"),
    //     etat: "terminer",
    //     heure: new Date,
    //   },
    //   {
    //     id:  4,
    //     numDeces: "deces004",
    //     nom: "Faye",
    //     prenom: "Assane",
    //     adresse: "baol",
    //     num: "7777777777",
    //     dateDebut: new Date,
    //     etat: "en cour",

    //     heure: new Date,
    //   },

    //   {
    //     id:  5,
    //     numDeces: "Deces005",
    //     nom: "Mboup",
    //     prenom: "Magne",
    //     adresse: "lambaye",
    //     num: "7777777777",
    //     dateDebut: new Date,
    //     etat: "en cour",
    //     heure: new Date,
    //   },
    //   {
    //     id:  6,
    //     numDeces: "Deces006",
    //     nom: "Ndiaye",
    //     prenom: "Awa",
    //     adresse: "Ngoundaie",
    //     num: "7777777777",
    //     dateDebut: new Date,
    //     etat: "terminer",
    //     heure: new Date,
    //   }
    // ]

  }

  getDecess(){
    return this.http.get<any>(this.api+'declaration_deces/')
  }
  getDeces(id: any){
    return this.http.get<any>(this.api+'declaration/'+id)
  }
  getEnleverMotif(id: any){
    return this.http.get<any>(this.api+'eneleverMotif/'+id)
  }
  createMotif(motif:string){
    return this.http.post<any>(this.api+'motif/',{motif:motif})
  }
  getMotif(id: any){
    return this.http.get<any>(this.api+'motif/'+id)
  }
  SupprimerDeces(id: any, dateEvenement: Date, extrait: string, agent:any,client:any,motif: any){

    let update = {
      "dateEvenement": dateEvenement,
      "extrait": extrait,
      "agent":this.agent,
      "client":client,
      "motif": motif,
    }
    return this.http.put<any>(`${this.api}declaration/${id}`,update);
  }
  changeEtatDeces(id: any, dateEvenement: Date, extrait: string, agent:any,client:any,etat: boolean){

    let update = {
      "dateEvenement": dateEvenement,
      "extrait": extrait,
      "agent":this.agent,
      "client":client,
      "etat": etat,
    }
    return this.http.put<any>(`${this.api}declaration/${id}`,update);
  }
  rendezVousDeces(id: any, dateEvenement: Date, extrait: string, agent:any,client:any,rendezVous: any){
    let update = {
      "dateEvenement": dateEvenement,
      "extrait": extrait,
      "agent":this.agent,
      "client":client,
      "rendezVous": rendezVous,
    }
    return this.http.put<any>(`${this.api}declaration/${id}`,update);
  }



  listDeces(){
    return this.deces;
  }
  // listAgent(): Agent[] {
  //   return this.agent;
  // }
  editerDeces(iddeces: number): Deces[] {
    this.decescurrent = this.deces.find(p => p.id == iddeces)
    return this.decescurrent;
  }

  // detailAgent(idUser: number): Agent[] {
  //   this.decescurrent = this.agent.find(p => p.id == idUser)
  //   return this.decescurrent;
  // }
  ajouterDeces(adddeces: Deces){
    this.deces.push(adddeces);
  }


}
