import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/user/components/services/user.service';
import { Deces } from '../../models/DeclerationDeces';
import { DecesService } from '../../service/declerationDeces.service';

@Component({
  selector: 'app-main-containt-dec-deces',
  templateUrl: './main-containt-dec-deces.component.html',
  styleUrls: ['./main-containt-dec-deces.component.scss']
})
export class MainContaintDecDecesComponent implements OnInit {

  courentSection = "nvelle"
  buttonText = "en cour"
  decess !: any;
  btn = "warning";
  role = 'guene'
  compt = 0;
  closeResult = '';
  active = 1;
  decescurrent: any;
  id: any;
  submitForm!: boolean;
  rvForm1!: FormGroup;
  client: any
  currentClient: any
  motif: any

  constructor(private decesService: DecesService,
    private router: Router,
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private toastr: ToastrService,
  ) {

    this.decess = decesService.listDeces();
  }

  ngOnInit(): void {
    this.initForm()
    this.getDecess()
  }
  getDecess(){
    this.decesService.getDecess().subscribe({
      next: (res)=>{
        this.decess=res;
        console.log(res)
      }
    })
  }
  getClient(id: number) {
    this.userService.getUser(id).subscribe({
      next: (res) => {
        this.currentClient = res;
      }
    })
  }
  open(content: any, id: number) {
    this.id = id
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      console.log(reason)
    });
  }
  initForm() {
    this.rvForm1 = this.formBuilder.group({
      motifRejet: ['', Validators.required],
    })
  }
  supprimerDeces(): void {
    // creation de la motif
    const formValue = this.rvForm1.value
    this.decesService.createMotif(formValue.motifRejet).subscribe((response) => {
      this.motif = response.id
      console.log(response.id)

    // insertion de la motif
    this.decesService.getDeces(this.id).subscribe((response: any) => {
      this.decescurrent = response;
      this.decesService.SupprimerDeces(
        this.decescurrent?.id,
        this.decescurrent?.dateEvenement,
        this.decescurrent?.extrait,
        this.decescurrent?.agent,
        this.decescurrent?.client,
        this.motif
      ).subscribe({
        next: (res) => {
          console.log(res);
          this.getDecess()
        },
        error: (e) => console.error(e)
      });
    });
  })
  }
  nonterminerDeces(id: number){
    this.decesService.getDeces(id).subscribe((response: any) => {
      this.decescurrent = response;
      this.decesService.changeEtatDeces(
        this.decescurrent?.id,
        this.decescurrent?.dateEvenement,
        this.decescurrent?.extrait,
        this.decescurrent?.agent,
        this.decescurrent?.client,
        false
      ).subscribe({
        next: (res) => {
          console.log(res);
          this.getDecess()
        },
        error: (e) => console.error(e)
      });
    });
  }
  terminerDeces(id: number){
    this.decesService.getDeces(id).subscribe((response: any) => {
      this.decescurrent = response;
      this.decesService.changeEtatDeces(
        this.decescurrent?.id,
        this.decescurrent?.dateEvenement,
        this.decescurrent?.extrait,
        this.decescurrent?.agent,
        this.decescurrent?.client,
        true
      ).subscribe({
        next: (res) => {
          console.log(res);
          this.getDecess()

        },
        error: (e) => console.error(e)
      });
    });
  }
  changeSection(section: string) {
    this.courentSection = section;
  }
  supprimer(n: number): void {
   // this.decescurrent = this.decess.find(p => p.id == n)
    this.decescurrent.motifRejet = prompt(" Donnez Motif ");
  }

  retourId(n: number): void {
   // this.decescurrent = this.decess.find(p => p.id == n)
    if (this.decescurrent['etat'] === 'en cour') {
      this.decescurrent['etat'] = 'terminer'
    } else {
      this.decescurrent['etat'] = 'en cour';
    }
  }


  fin = new Date();
  duree = this.fin.getSeconds();

}

