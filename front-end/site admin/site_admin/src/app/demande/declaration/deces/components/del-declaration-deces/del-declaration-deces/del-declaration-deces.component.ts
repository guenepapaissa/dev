import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Deces } from '../../models/DeclerationDeces';
import { DecesService } from '../../service/declerationDeces.service';

@Component({
  selector: 'app-del-declaration-deces',
  templateUrl: './del-declaration-deces.component.html',
  styleUrls: ['./del-declaration-deces.component.scss']
})
export class DelDeclarationDecesComponent implements OnInit {
  decess !: any;
  decescurrent: any;
  closeResult = '';
  id!:number
  currentMotif:any

  constructor(  private decesService: DecesService,
                private router : Router,
                private modalService: NgbModal
     ) {   }
  ngOnInit(): void {
    this.getArchiveDeces()
  }
  getArchiveDeces(){
    this.decesService.getDecess().subscribe({
      next : (res)=>{
        this.decess= res;
        console.log(this.decess)
      }
    })
  }
  getMotif(id:number){
    this.currentMotif=this.decesService.getMotif(id).subscribe((response)=>{
      this.currentMotif=response
    })
  }
  openSm(content: any, id: number) {
    this.id=id
    this.getMotif(id)
    this.modalService.open(content,{ size: 'sm' }).result.then((result: any) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason: any) => {
   //   this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

}
