import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DelContaintDeclarationNaissanceComponent } from './del-containt-declaration-naissance.component';

describe('DelContaintDeclarationNaissanceComponent', () => {
  let component: DelContaintDeclarationNaissanceComponent;
  let fixture: ComponentFixture<DelContaintDeclarationNaissanceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DelContaintDeclarationNaissanceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DelContaintDeclarationNaissanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
