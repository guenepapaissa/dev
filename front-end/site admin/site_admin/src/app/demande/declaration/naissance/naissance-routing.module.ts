import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AddContaintDeclarationNaissanceComponent } from "./components/add-declaration-naissance/add-containt-declaration-naissance/add-containt-declaration-naissance.component";
import { DelContaintDeclarationNaissanceComponent } from "./components/del-declaration-naissance/del-containt-declaration-naissance/del-containt-declaration-naissance.component";
import { DetailContaintDeclarationNaissanceComponent } from "./components/detail-declaration-naissance/detail-containt-declaration-naissance/detail-containt-declaration-naissance.component";
import { EditDeclarationNaissanceComponent } from "./components/edit-declaration-naissance/edit-declaration-naissance/edit-declaration-naissance.component";
import { MainContaintDecNaissanceComponent } from "./components/main-containt-dec-naissance/main-containt-dec-naissance/main-containt-dec-naissance.component";

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Declaration  Naissance',
    },
    children: [
      {
        path: '',
        redirectTo :"",
        pathMatch:'full',
        component: MainContaintDecNaissanceComponent,
        data: {
          title:`Declaration Naissance`
        },
      },
      {
        path: 'addnaissance/:id',
        component: AddContaintDeclarationNaissanceComponent,
        data: {
          title: $localize`Ajout Declaration  Naissance`
        }
      },
      {
        path: 'editnaissance/:id',
        component:  EditDeclarationNaissanceComponent,
        data: {
          title: $localize`Modifier Declaration  Naissance`
        }
      },
      {
        path: 'detailnaissance/:id',
        component: DetailContaintDeclarationNaissanceComponent,
        data: {
          title: $localize`Detail Declaration  Naissance`
        }
      },

    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]

})
export class NaissanceRoutingModule {

}
