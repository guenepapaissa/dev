import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/user/components/services/user.service';

import { Naissance } from '../../../models/Naissance';
import { NaissanceService } from '../../../service/naissance.service';

@Component({
  selector: 'app-main-containt-dec-naissance',
  templateUrl: './main-containt-dec-naissance.component.html',
  styleUrls: ['./main-containt-dec-naissance.component.scss']
})
export class MainContaintDecNaissanceComponent implements OnInit {
  courentSection = "nvelle"
  buttonText = "en cour"
  naissances !: any;
  closeResult = '';
  active = 1;
  naissancecurrent: any;
  id!: number;
  submitForm=false
  rvForm1!: FormGroup;
  client: any
  currentClient= [{nom: ''}]
  motif: any

  constructor(private naissanceService: NaissanceService,
    private router: Router,
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private userService: UserService,
  ) {  }

  ngOnInit(): void {
    this.getNaissances()
    this.initForm()
  }
    getNaissances(){
      // recuperer tous les demandes dont idAgent est null
    this.naissanceService.getNaissances().subscribe({
      next: (res)=>{
        this.naissances=res;
        console.log(res)
        // for(let value of this.naissances){
        //   this.getClient(value?.client)
        // }
      }
    })
  }
  getClient(id: number) {
    this.userService.getUser(id).subscribe({
      next: (res) => {
        Array.prototype.push.apply( this.currentClient,[{nom: res.nom}])
        console.log(this.currentClient)
      }
    })
  }
  open(content: any, id: number) {
    this.id = id
    this.initForm()
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      console.log(reason)
    });
  }

  changeSection(section: string) {
    this.courentSection = section;
  }
  supprimer(n: number): void {
    //this.naissancecurrent = this.naissances.find(p => p.id == n)
    this.naissancecurrent.motifRejet = prompt(" Donnez Motif ");
  }

  supprimerNaissance(): void {
    // creation de la motif
    const formValue = this.rvForm1.value
    this.naissanceService.createMotif(formValue.motifRejet).subscribe((response) => {
      this.motif = response.id
      console.log(response.id)

    // insertion de la motif
    this.naissanceService.getNaissance(this.id).subscribe((response: any) => {
      this.naissancecurrent = response;
      this.naissanceService.SupprimerNaissance(
        this.naissancecurrent?.id,
        this.naissancecurrent?.dateEvenement,
        this.naissancecurrent?.extrait,
        this.naissancecurrent?.client,
        this.motif
      ).subscribe({
        next: (res) => {
          console.log(res);
          this.getNaissances()
        },
        error: (e) => console.error(e)
      });
    });
  })
  }
  nonterminerNaissance(id: number){
    this.naissanceService.getNaissance(id).subscribe((response: any) => {
      this.naissancecurrent = response;
      this.naissanceService.changeEtatNaissance(
        this.naissancecurrent?.id,
        this.naissancecurrent?.dateEvenement,
        this.naissancecurrent?.extrait,
        this.naissancecurrent?.client,
        false
      ).subscribe({
        next: (res) => {
          console.log(res);
          this.getNaissances()
        },
        error: (e) => console.error(e)
      });
    });
  }
  terminerNaissance(id: number){
    this.naissanceService.getNaissance(id).subscribe((response: any) => {
      this.naissancecurrent = response;
      this.naissanceService.changeEtatNaissance(
        this.naissancecurrent?.id,
        this.naissancecurrent?.dateEvenement,
        this.naissancecurrent?.extrait,
        this.naissancecurrent?.client,
        true
      ).subscribe({
        next: (res) => {
          console.log(res);
          this.getNaissances()
        },
        error: (e) => console.error(e)
      });
    });
  }

  retourId(n: number): void {
   // this.naissancecurrent = this.naissances.find(p => p.id == n)
    if (this.naissancecurrent['etat'] === 'en cour') {
      this.naissancecurrent['etat'] = 'terminer'
    } else {
      this.naissancecurrent['etat'] = 'en cour';
    }
  }
  initForm() {
    this.rvForm1 = this.formBuilder.group({
      motifRejet: ['', Validators.required],
    })
  }

  fin = new Date();
  duree = this.fin.getSeconds();

}
