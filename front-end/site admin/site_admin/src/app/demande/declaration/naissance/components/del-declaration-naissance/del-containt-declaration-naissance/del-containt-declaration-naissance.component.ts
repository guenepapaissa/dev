import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Naissance } from '../../../models/Naissance';
import { NaissanceService } from '../../../service/naissance.service';

@Component({
  selector: 'app-del-containt-declaration-naissance',
  templateUrl: './del-containt-declaration-naissance.component.html',
  styleUrls: ['./del-containt-declaration-naissance.component.scss']
})
export class DelContaintDeclarationNaissanceComponent implements OnInit {

  naissances !:any;
  naissancecurrent: any;
  closeResult = '';
  id!:number
  currentMotif:any

  constructor(  private naissanceService: NaissanceService,
                private router : Router,
                private modalService: NgbModal,
     ) {


   }
  ngOnInit(): void {
    this.getArchiveNaissance()
  }
  getArchiveNaissance(){
    this.naissanceService.getNaissances().subscribe({
      next: (res)=>{
        this.naissances=res;
        console.log(this.naissances)
      }
    })
  }
  getMotif(id:number){
    this.currentMotif=this.naissanceService.getMotif(id).subscribe((response)=>{
      this.currentMotif=response
    })
  }
  openSm(content: any, id: number) {
    this.id=id
    this.getMotif(id)
    this.modalService.open(content,{ size: 'sm' }).result.then((result: any) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason: any) => {
   //   this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
}
