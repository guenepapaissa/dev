import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/user/components/services/user.service';
import { Naissance } from '../../../models/Naissance';
import { NaissanceService } from '../../../service/naissance.service';

@Component({
  selector: 'app-add-containt-declaration-naissance',
  templateUrl: './add-containt-declaration-naissance.component.html',
  styleUrls: ['./add-containt-declaration-naissance.component.scss']
})
export class AddContaintDeclarationNaissanceComponent implements OnInit {

  id !: number;
  currentnaissance: any;
  closeResult = '';
  rvForm!: FormGroup;
  newNaissance !: Naissance[];
  courentbouton = 'rv';
  rvForm1!: FormGroup;
  client!:any;
  motif: any;
  currentMotif:any
  constructor(private activatedRoute: ActivatedRoute,
    private route: Router,
    private naissanceService: NaissanceService,
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private toastr: ToastrService,
  ) { }

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.params['id'];
    this.getNaissance(this.id)
  }

  getNaissance(id: number) {
    this.naissanceService.getNaissance(id).subscribe((response: any) => {
      this.currentnaissance = response;
      this.getClient()
      if(this.currentnaissance.motif)
        this.getMotif(this.currentnaissance.motif)
        this.initForm();
    });
  }
  getMotif(id:number){
    this.currentMotif=this.naissanceService.getMotif(id).subscribe((response)=>{
      this.currentMotif=response
    })
  }
  getClient(){
    this.userService.getUser(this.currentnaissance?.client).subscribe((response)=>{
      this.client=response
      console.log(this.client)
    })
   }

   SupprimerNaissance(): void {
    console.log(this.id)
    const formValue = this.rvForm1.value
    this.naissanceService.createMotif(formValue.motifRejet).subscribe((response)=>{
      this.motif=response.id
      console.log(response.id)
    })
    this.naissanceService.getNaissance(this.id).subscribe((response: any) => {
      this.currentnaissance = response;
      this.naissanceService.SupprimerNaissance(
        this.currentnaissance?.id,
        this.currentnaissance?.dateEvenement,
        this.currentnaissance?.extrait,
        this.currentnaissance?.client,
        this.motif
      ).subscribe({
        next: (res) => {
          console.log(res);
          this.route.navigate(['/naissance'])
        },
        error: (e) => console.error(e)
      });
    });
  }

  recupererNaissance(): void {
    this.naissanceService.getNaissance(this.id).subscribe((response: any) => {
      this.currentnaissance = response;
      this.naissanceService.SupprimerNaissance(
        this.currentnaissance?.id,
        this.currentnaissance?.dateEvenement,
        this.currentnaissance?.extrait,
        this.currentnaissance?.client,
        1
      ).subscribe({
        next: (res) => {
          console.log(res);
          this.route.navigate(['/naissance'])
        },
        error: (e) => console.error(e)
      });
    });
  }

  rendezVousNaissance(): void {
    // cette methode permet de modier l'etat de la demande, encour <=>  terminer
    const formValue = this.rvForm.value;
    const x = new Date(this.rvForm.value.dateRv);// date rendez vous fixer par l'agent
      const y = new Date();//today
      const z = x.toDateString().substring(0, 3)// recuperation du jour en string (mon, tue....)
      console.log(x)
      console.log(y)
      console.log(z)
      if (x > y && z != 'Sat' && z != 'Sun') {
    this.naissanceService.getNaissance(this.id).subscribe((response: any) => {
      this.currentnaissance = response;
      console.log(this.currentnaissance)
      this.naissanceService.rendezVousNaissance(
        this.currentnaissance?.id,
        this.currentnaissance?.dateEvenement,
        this.currentnaissance?.extrait,
        this.currentnaissance?.client,
        formValue.dateRv,
      ).subscribe({
        next: (res) => {
          console.log(res);
          this.route.navigate(['/naissance'])
        },
        error: (e) => console.error(e)
      });
    });
  }
  else{
    this.toastr.warning('Date incorrect !', 'Rendez-vous');
  }
  }
  supprimer(motifRejet: string) {
    this.currentnaissance.motifRejet = motifRejet;
    this.route.navigate(['/naissance'])
  }
  recuperer() {
    this.currentnaissance.motifRejet = '';
    this.route.navigate(['/naissance'])
  }


  open(content: any, modal: string) {
    this.courentbouton = modal;
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
     // this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }


  initForm() {

      this.rvForm = this.formBuilder.group({
        dateRv: ['', Validators.required],
        etat: ['en cour', Validators.required],
      })

    this.rvForm1 = this.formBuilder.group({
      motifRejet: ['', Validators.required],
    })



  }

  // onSave() {
  //   if (this.courentbouton === 'fixer') {
  //     // fixer rv naissance
  //     const x = new Date(this.rvForm.value['dateRv']);
  //     const y = new Date(this.currentnaissance.dateDebut);
  //     const z = x.toDateString().substring(0, 3)
  //     console.log(z)
  //     if (x > y && z != 'Sat' && z != 'Sun') {
  //       const formValue = this.rvForm.value;
  //       const newNaissance = new Naissance(
  //         this.currentnaissance.id,
  //         this.currentnaissance.numNaissance,
  //         this.currentnaissance.nom,
  //         this.currentnaissance.prenom,
  //         this.currentnaissance.adresse,
  //         this.currentnaissance.num,
  //         this.currentnaissance.dateDebut,
  //         this.currentnaissance.heure,
  //         this.currentnaissance.motifRejet,
  //         formValue['etat'],
  //         formValue['dateRv'],
  //       )
  //       console.log(newNaissance);
  //       this.naissanceService.ajouternaissance(newNaissance)
  //       this.route.navigate(['/naissance'])
  //     }
  //     else {
  //       alert("date incorrect");
  //       this.route.navigate(['/naissance/addnaissance', this.currentnaissance.id]);
  //     }
  //   }
  //   if (this.courentbouton === 'supprimer') {
  //     // supprimer declaration naissance
  //     console.log('newNaissance');
  //     const formValue = this.rvForm1.value;
  //     const newNaissance = new Naissance(
  //       this.currentnaissance.id,
  //       this.currentnaissance.numNaissance,
  //       this.currentnaissance.nom,
  //       this.currentnaissance.prenom,
  //       this.currentnaissance.adresse,
  //       this.currentnaissance.num,
  //       this.currentnaissance.dateDebut,
  //       this.currentnaissance.heure,
  //       formValue['motifRejet'],
  //       this.currentnaissance.etat,
  //       this.currentnaissance.dateRv,
  //     )
  //     console.log(newNaissance);
  //     this.supprimer(formValue['motifRejet']);
  //   }

  // }

}
