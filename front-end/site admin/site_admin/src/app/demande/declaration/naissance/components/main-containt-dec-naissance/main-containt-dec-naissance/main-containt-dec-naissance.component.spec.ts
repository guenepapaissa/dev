import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MainContaintDecNaissanceComponent } from './main-containt-dec-naissance.component';

describe('MainContaintDecNaissanceComponent', () => {
  let component: MainContaintDecNaissanceComponent;
  let fixture: ComponentFixture<MainContaintDecNaissanceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MainContaintDecNaissanceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainContaintDecNaissanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
