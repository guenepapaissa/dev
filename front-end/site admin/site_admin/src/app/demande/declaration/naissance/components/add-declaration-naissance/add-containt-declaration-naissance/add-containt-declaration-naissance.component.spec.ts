import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddContaintDeclarationNaissanceComponent } from './add-containt-declaration-naissance.component';

describe('AddContaintDeclarationNaissanceComponent', () => {
  let component: AddContaintDeclarationNaissanceComponent;
  let fixture: ComponentFixture<AddContaintDeclarationNaissanceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddContaintDeclarationNaissanceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddContaintDeclarationNaissanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
