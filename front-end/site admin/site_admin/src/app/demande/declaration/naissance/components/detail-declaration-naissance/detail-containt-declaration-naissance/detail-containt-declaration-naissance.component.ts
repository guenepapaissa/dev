import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/user/components/services/user.service';
import { NaissanceService } from '../../../service/naissance.service';

@Component({
  selector: 'app-detail-containt-declaration-naissance',
  templateUrl: './detail-containt-declaration-naissance.component.html',
  styleUrls: ['./detail-containt-declaration-naissance.component.scss']
})
export class DetailContaintDeclarationNaissanceComponent implements OnInit {
  client!:any;
  motif: any;
  currentMotif:any
  currentnaissance: any;

  id !: number;
  constructor(  private userService: UserService,
    private activatedRoute: ActivatedRoute,
    private naissanceService: NaissanceService,
     private route: Router,)
   {
    this.id = this.activatedRoute.snapshot.params['id'];
    }

  ngOnInit(): void {
    this.getNaissance(this.id)
  }
  getNaissance(id: number) {
    this.naissanceService.getNaissance(id).subscribe((response: any) => {
      this.currentnaissance = response;
      this.getClient()
      if(this.currentnaissance.motif)
        this.getMotif(this.currentnaissance.motif)
    });
  }
  getMotif(id:number){
    this.currentMotif=this.naissanceService.getMotif(id).subscribe((response)=>{
      this.currentMotif=response
    })
  }
  getClient(){
    this.userService.getUser(this.currentnaissance?.client).subscribe((response)=>{
      this.client=response
      console.log(this.client)
    })
   }

    recupererNaissance(): void {
      this.naissanceService.getNaissance(this.id).subscribe((response: any) => {
        this.currentnaissance = response;
        this.naissanceService.getEnleverMotif(
          this.currentnaissance?.id).subscribe({
          next: (res) => {
            console.log(res);
           this.route.navigate(['/naissance'])
          },
          error: (e) => console.error(e)
        });
      });
    }


}
