import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Naissance } from '../models/Naissance';


@Injectable({
  providedIn: 'root'
})
export class NaissanceService {
  naissance!: any;
  naissancecurrent : any;
  api= environment.api
  agent= <any> localStorage.getItem('idUser')

  constructor(private http : HttpClient) {
    // this.naissance = [
    //   {
    //     id:  1,
    //     numNaissance: "naissance001",
    //     nom: "Deme",
    //     prenom: "Dieynaba",
    //     adresse: "pout",
    //     num: "7777777777",
    //     dateDebut: new Date,
    //     dateRv: new Date("03-01-2022"),
    //     etat: "en cour",

    //     heure: new Date,
    //   },
    //   {
    //     id: 2,
    //     numNaissance: "naissance002",
    //     nom: "Cana",
    //     prenom: "you",
    //     adresse: "dakar",
    //     num: "7777777777",
    //     dateDebut: new Date(),
    //     dateRv: new Date("03-24-2022"),
    //     etat: "en cour",

    //     heure: new Date,
    //   },
    //   {
    //     id: 3,
    //     numNaissance: "naissance003",
    //     nom: "Dione",
    //     prenom: "Pape Laye",
    //     adresse: "thies",
    //     num: "7777777777",
    //     dateDebut: new Date(),
    //     dateRv: new Date("03-09-2022"),
    //     etat: "terminer",
    //     heure: new Date,
    //   },
    //   {
    //     id:  4,
    //     numNaissance: "naissance004",
    //     nom: "Faye",
    //     prenom: "Assane",
    //     adresse: "baol",
    //     num: "7777777777",
    //     dateDebut: new Date,
    //     etat: "en cour",

    //     heure: new Date,
    //   },
    //   {
    //     id:  5,
    //     numNaissance: "naissance005",
    //     nom: "Mboup",
    //     prenom: "Magne",
    //     adresse: "lambaye",
    //     num: "7777777777",
    //     dateDebut: new Date,
    //     etat: "en cour",
    //     heure: new Date,
    //   },
    //   {
    //     id:  6,
    //     numNaissance: "naissance006",
    //     nom: "Ndiaye",
    //     prenom: "Awa",
    //     adresse: "Ngoundaie",
    //     num: "7777777777",
    //     dateDebut: new Date,
    //     etat: "terminer",
    //     heure: new Date,
    //   }
    // ]

  }

  getNaissances(){
    return this.http.get<any>(this.api+'declaration_naissance/')
  }
  getNaissance(id: any){
    return this.http.get<any>(this.api+'declaration/'+id)
  }
  getMotif(id: any){
    return this.http.get<any>(this.api+'motif/'+id)
  }
  getEnleverMotif(id: any){
    return this.http.get<any>(this.api+'eneleverMotif/'+id)
  }
  createMotif(motif:string){
    return this.http.post<any>(this.api+'motif/',{motif:motif})
  }
  SupprimerNaissance(id: any, dateEvenement: Date, extrait: string,client:any,motif: any){

    let update = {
      "dateEvenement": dateEvenement,
      "extrait": extrait,
      "agent":this.agent,
      "client":client,
      "motif": motif,
    }
    return this.http.put<any>(`${this.api}declaration/${id}`,update);
  }
  changeEtatNaissance(id: any, dateEvenement: Date, extrait: string,client:any,etat: boolean){

    let update = {
      "dateEvenement": dateEvenement,
      "extrait": extrait,
      "agent":this.agent,
      "client":client,
      "etat": etat,
    }
    return this.http.put<any>(`${this.api}declaration/${id}`,update);
  }
  rendezVousNaissance(id: any, dateEvenement: Date, extrait: string,client:any,rendezVous: any){
    let update = {
      "dateEvenement": dateEvenement,
      "extrait": extrait,
      "agent":this.agent,
      "client":client,
      "rendezVous": rendezVous,
    }
    return this.http.put<any>(`${this.api}declaration/${id}`,update);
  }




  listNaissance(){
    return this.naissance;
  }
  // listAgent(): Agent[] {
  //   return this.agent;
  // }
  editernaissance(idnaissance: number): Naissance[] {
   // this.naissancecurrent = this.naissance.find(p => p.id == idnaissance)
    return this.naissancecurrent;
  }

  // detailAgent(idUser: number): Agent[] {
  //   this.naissancecurrent = this.agent.find(p => p.id == idUser)
  //   return this.naissancecurrent;
  // }
  ajouternaissance(addnaissance: Naissance){
    this.naissance.push(addnaissance);
  }


}
