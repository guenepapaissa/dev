import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import {
  AvatarModule,
  ButtonGroupModule,
  ButtonModule,
  CardModule,
  FormModule,
  GridModule,
  NavModule,
  ProgressModule,
  TableModule,
  TabsModule
} from '@coreui/angular';
import { IconModule } from '@coreui/icons-angular';
import { ChartjsModule } from '@coreui/angular-chartjs';
import { WidgetsModule } from 'src/app/views/widgets/widgets.module';
import { NaissanceRoutingModule } from './naissance-routing.module';



@NgModule({
  imports: [
    NaissanceRoutingModule,
    CardModule,
    NavModule,
    IconModule,
    TabsModule,
    CommonModule,
    GridModule,
    ProgressModule,
    ReactiveFormsModule,
    ButtonModule,
    FormModule,
    ButtonModule,
    ButtonGroupModule,
    ChartjsModule,
    AvatarModule,
    TableModule,
    WidgetsModule,
    ReactiveFormsModule,
  ],
  // declarations: [
  //   ListAuthentificationComponent
  // ],
  // declarations: [
  //   MainContentAuthentificationComponent
  // ],
//   declarations: [DashboardComponent]
})

export class NaissanceModule {
}
