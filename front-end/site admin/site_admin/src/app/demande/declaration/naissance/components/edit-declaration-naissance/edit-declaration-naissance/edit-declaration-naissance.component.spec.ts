import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDeclarationNaissanceComponent } from './edit-declaration-naissance.component';

describe('EditDeclarationNaissanceComponent', () => {
  let component: EditDeclarationNaissanceComponent;
  let fixture: ComponentFixture<EditDeclarationNaissanceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditDeclarationNaissanceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDeclarationNaissanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
