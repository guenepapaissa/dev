import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailContaintDeclarationNaissanceComponent } from './detail-containt-declaration-naissance.component';

describe('DetailContaintDeclarationNaissanceComponent', () => {
  let component: DetailContaintDeclarationNaissanceComponent;
  let fixture: ComponentFixture<DetailContaintDeclarationNaissanceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailContaintDeclarationNaissanceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailContaintDeclarationNaissanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
