import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AgentService } from 'src/app/agent/components/service/agent.service';
import { UserService } from 'src/app/user/components/services/user.service';
import { NaissanceService } from '../../../service/naissance.service';

@Component({
  selector: 'app-edit-declaration-naissance',
  templateUrl: './edit-declaration-naissance.component.html',
  styleUrls: ['./edit-declaration-naissance.component.scss']
})
export class EditDeclarationNaissanceComponent implements OnInit {
  id !: number;
  currentnaissance: any;
  active = 1;
  client!: any;
  motif: any;
  currentMotif: any
  currentAgent: any
  profil = localStorage.getItem('profil')
  constructor(private activatedRoute: ActivatedRoute,
    private userService: UserService,
    private route: Router,
    private agentService: AgentService,
    private naissanceService: NaissanceService) { }

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.params['id'];
    this.currentnaissance = this.naissanceService.editernaissance(this.id);
    this.getNaissance(this.id)
  }
  getNaissance(id: number) {
    this.naissanceService.getNaissance(id).subscribe((response: any) => {
      this.currentnaissance = response;
      console.log(this.currentnaissance)
      this.getClient()
      this.getAgent(this.currentnaissance.agent)
      if (this.currentnaissance.motif)
        this.getMotif(this.currentnaissance.motif)

    });
  }
  getMotif(id: number) {
    this.currentMotif = this.naissanceService.getMotif(id).subscribe((response) => {
      this.currentMotif = response
    })
  }
  getClient() {
    this.userService.getUser(this.currentnaissance?.client).subscribe((response) => {
      this.client = response
      console.log(this.client)
    })
  }
  getAgent(id: number) {
    this.agentService.getAgent(id).subscribe((response) => {
      this.currentAgent = response
    })
  }

}


