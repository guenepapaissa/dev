import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthentificationService } from '../../service/authentification.service';

@Component({
  selector: 'app-edit-authentification',
  templateUrl: './edit-authentification.component.html',
  styleUrls: ['./edit-authentification.component.scss']
})
export class EditAuthentificationComponent implements OnInit {
  id !:number;
  currentAuth :any;
  active = 1;
  constructor(private activatedRoute: ActivatedRoute, private route : Router ,private authentificationService : AuthentificationService) { }

  ngOnInit(): void {
    this.id =this.activatedRoute.snapshot.params['id'];
   this.currentAuth= this.authentificationService.editerAuth(this.id);
   //this.getAuthentification(this.id)
  }
  getAuthentification(id : number){
    this.authentificationService.getAuthentification(id).subscribe({
      next: (res)=>{
        this.currentAuth= res;
        console.log(this.currentAuth);
      }
    })
  }

}
