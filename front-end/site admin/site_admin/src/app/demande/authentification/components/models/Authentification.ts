export class Authentification{
  constructor(
    public id : number,
    public numAuthentfication: string,
    public nom: string,
    public prenom: string,
    public adresse: string,
    public num: string,
    public dateDebut: Date,
    public heure: Date,
    public motifRejet?: string,
    public etat?: string,
    public dateRv?: Date,
  ){}
}
