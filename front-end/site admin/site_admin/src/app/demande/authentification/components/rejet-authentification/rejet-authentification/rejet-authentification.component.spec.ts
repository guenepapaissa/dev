import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RejetAuthentificationComponent } from './rejet-authentification.component';

describe('RejetAuthentificationComponent', () => {
  let component: RejetAuthentificationComponent;
  let fixture: ComponentFixture<RejetAuthentificationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RejetAuthentificationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RejetAuthentificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
