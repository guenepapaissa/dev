import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DelAuthentificationComponent } from './del-authentification.component';

describe('DelAuthentificationComponent', () => {
  let component: DelAuthentificationComponent;
  let fixture: ComponentFixture<DelAuthentificationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DelAuthentificationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DelAuthentificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
