import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Authentification } from '../models/Authentification';


@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {
  auth!: Authentification[];
  authcurrent: any;
  api= environment.api

  constructor(private http : HttpClient) {
    this.auth = [
      {
        id: 1,
        numAuthentfication: "Auth001",
        nom: "Deme",
        prenom: "Dieynaba",
        adresse: "pout",
        num: "7777777777",
        dateDebut: new Date,
        dateRv: new Date("03-01-2022"),
        etat: "en cour",
        heure: new Date,
      },
      {
        id: 2,
        numAuthentfication: "Auth002",
        nom: "Cana",
        prenom: "you",
        adresse: "dakar",
        num: "7777777777",
        dateDebut: new Date(),
        dateRv: new Date,
        etat: "en cour",

        heure: new Date,
      },
      {
        id: 3,
        numAuthentfication: "Auth003",
        nom: "Dione",
        prenom: "Pape Laye",
        adresse: "thies",
        num: "7777777777",
        dateDebut: new Date(),
        dateRv: new Date("03-01-2022"),
        etat: "terminer",
        heure: new Date,
      },
      {
        id: 4,
        numAuthentfication: "Auth004",
        nom: "Faye",
        prenom: "Assane",
        adresse: "baol",
        num: "7777777777",
        dateDebut: new Date("03-01-2022"),
        dateRv: new Date("03-09-2022"),
        etat: "en cour",
        heure: new Date,
      },
      {
        id: 5,
        numAuthentfication: "Auth005",
        nom: "Mboup",
        prenom: "Magne",
        adresse: "lambaye",
        num: "777777777",
        dateDebut: new Date("03-20-2022"),
        dateRv: new Date("03-23-2022"),
        etat: "en cour",
        heure: new Date,
      },
      {
        id: 6,
        numAuthentfication: "Auth006",
        nom: "Ndiaye",
        prenom: "Awa",
        adresse: "Ngoundaie",
        num: "7777777777",
        dateDebut: new Date("03-21-2022"),
        etat: "terminer",
        heure: new Date,
      }
    ]
     //this.calendarOptions.events['title']= this.auths[indice].nom,
  //    this.calendarOptions = {
  //     locale: 'fr',
  //     locales: allLocales,
  //     initialView: 'dayGridMonth',
  //     dateClick: this.handleDateClick.bind(this), // bind is important!
  //     events: [
  //       {title: this.auths[indice].nom, date: this.auths[indice].dateRv},

  //     ],

  //     weekends: false,
  //   };
  // }
  }

  getAuthentifications(){
    return this.http.get<any>(this.api+'demande_toutGenre/')
  }
  getAuthentification(id: any){
    return this.http.get<any>(this.api+'demande_toutGenre/',id)
  }
  updateAuthentification(id: any,prenom: string, nom: string, telephone: string, email: string, login: string, password: string){
    let update = {
      "numeroRegistre": prenom,
      "anne": nom,
      "nombreExemplaire": telephone,
      "etatRetrait": email,
      "idDemande": login,
      "date": password,
      "motifRejet": password,
      "etatDemande": password,

    }
    return this.http.put(`${this.api}demande_toutGenre/${id}`,update);
  }
  createAuthentification(prenom: string, nom: string, telephone: string, email: string, login: string, password: string){
    let create = {
      "numeroRegistre": prenom,
      "anne": nom,
      "nombreExemplaire": telephone,
      "etatRetrait": email,
      "idDemande": login,
      "date": password,
      "motifRejet": password,
      "etatDemande": password,

    }
    return this.http.put(`${this.api}demande_toutGenre/`,create);
  }
  changeEtatAuthentification(id: any,prenom: string, nom: string, telephone: string, email: string, login: string, password: string, typeAgent: string,etat:boolean){
    let update = {
      "prenom": prenom,
      "nom": nom,
      "telephone": telephone,
      "email": email,
      "login": login,
      "password": password,
      "typeAgent": typeAgent,
      "etat":etat
    }
    return this.http.put(`${this.api}agent/${id}`,update);
  }
  listAuthentification() {
    return this.auth;
  }
  // listAgent(): Agent[] {
  //   return this.agent;
  // }
  editerAuth(idAuth: number): Authentification[] {
    this.authcurrent = this.auth.find(p => p.id == idAuth)
    return this.authcurrent;
  }

  // detailAgent(idUser: number): Agent[] {
  //   this.authcurrent = this.agent.find(p => p.id == idUser)
  //   return this.authcurrent;
  // }
  ajouterAuth(addauth: Authentification) {
    this.auth.push(addauth);
  }


}
