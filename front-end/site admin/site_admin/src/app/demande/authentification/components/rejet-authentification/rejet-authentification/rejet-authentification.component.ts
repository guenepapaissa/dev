import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Authentification } from '../../models/Authentification';
import { AuthentificationService } from '../../service/authentification.service';

@Component({
  selector: 'app-rejet-authentification',
  templateUrl: './rejet-authentification.component.html',
  styleUrls: ['./rejet-authentification.component.scss']
})
export class RejetAuthentificationComponent implements OnInit {

  auths !: Authentification[];
  authcurrent: any;

  constructor(  private authService: AuthentificationService,
                private router : Router,
     ) {

    this.auths = authService.listAuthentification();
   }
  ngOnInit(): void {
    //this.getArchiveAuthentification()
  }
  getArchiveAuthentification(){
    this.authService.getAuthentifications().subscribe({
      next: (res) =>{
        this.auths=res;
        console.log(this.auths)
      }
    })
  }
}
