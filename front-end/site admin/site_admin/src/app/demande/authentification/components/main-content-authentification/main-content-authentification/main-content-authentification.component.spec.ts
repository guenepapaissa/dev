import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MainContentAuthentificationComponent } from './main-content-authentification.component';

describe('MainContentAuthentificationComponent', () => {
  let component: MainContentAuthentificationComponent;
  let fixture: ComponentFixture<MainContentAuthentificationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MainContentAuthentificationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainContentAuthentificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
