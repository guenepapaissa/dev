import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Authentification } from '../../models/Authentification';
import { AuthentificationService } from '../../service/authentification.service';
import { NgbModal, ModalDismissReasons,NgbNav } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';

@Component({
  selector: 'app-main-content-authentification',
  templateUrl: './main-content-authentification.component.html',
  styleUrls: ['./main-content-authentification.component.scss']
})
export class MainContentAuthentificationComponent implements OnInit {
  courentSection = "nvelle"
  buttonText="en cour"
  auths !: Authentification[];
  btn="warning";
  role='guene'
  compt=0;
  closeResult = '';
  active = 1;
  authcurrent: any;
  id !: number;
  submitForm = false;

  constructor(  private authService: AuthentificationService,
                private router : Router,
                private modalService: NgbModal,
     ) {

    this.auths = authService.listAuthentification();
   }

  ngOnInit(): void {
    // this.getAuthentifications();
    // this.getAuthentification(this.id)
  }

  getAuthentifications(){
    this.authService.getAuthentifications().subscribe({
      next: (res)=>{
        this.auths=res;
        console.log(res)
      }
    })
  }

  open(content: any, id: number) {
    this.id = id
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      console.log(reason)
    });
  }


  supprimerAuthentification(): void {
    this.authService.getAuthentification(this.id).subscribe((response: any) => {
      this.authcurrent = response;
      console.log( this.authcurrent)
      this.authService.changeEtatAuthentification(
        this.authcurrent?.id,
        this.authcurrent?.prenom,
        this.authcurrent?.nom,
        this.authcurrent?.telephone,
        this.authcurrent?.email,
        this.authcurrent?.login,
        this.authcurrent?.password,
        this.authcurrent?.typeAgent,
        true
        ).subscribe({
        next: (res) => {
          console.log(res);
          window.location.reload()
         // this.route.navigate(["/agent"]);
        },
        error: (e) => console.error(e)
      });
    });
    // if (confirm(" voulez-vous bloquer  " + this.agentcurrent?.prenom + " " + this.agentcurrent?.nom)) {}
      this.submitForm = true


  }

  changeSection(section : string) {
    this.courentSection = section;
  }


  retourId(n : number): void{
  this.authcurrent= this.auths.find(p => p.id == n)
    if(this.authcurrent['etat']=== 'en cour'){
      this.authcurrent['etat']= 'terminer'
      }else{
        this.authcurrent['etat']= 'en cour';
      }
  }
  modifierEtat(): void {
    // cette methode permet de modier l'etat de la demande, encour <=>  terminer (voir methode retourId)
    this.authService.getAuthentification(this.id).subscribe((response: any) => {
      this.authcurrent = response;
      console.log( this.authcurrent)
      this.authService.changeEtatAuthentification(
        this.authcurrent?.id,
        this.authcurrent?.prenom,
        this.authcurrent?.nom,
        this.authcurrent?.telephone,
        this.authcurrent?.email,
        this.authcurrent?.login,
        this.authcurrent?.password,
        this.authcurrent?.typeAgent,
        true
        ).subscribe({
        next: (res) => {
          console.log(res);
          window.location.reload()
         // this.route.navigate(["/agent"]);
        },
        error: (e) => console.error(e)
      });
    });
  }
  fin = new Date();
  duree= this.fin.getSeconds();

}



  // etat(){
  //   if(this.buttonText==='en cour'){
  //     this.buttonText="terminer",
  //     this.btn="success";
  //   }
  //   else{
  //     this.buttonText="en cour",
  //     this.btn="warning"
  //   }
  // }
  // refresh(){
  //   if(this.compt ===0 ){
  //   this.router.navigate(['/authentification'])
  //   this.compt=1
  //   }
  // }
