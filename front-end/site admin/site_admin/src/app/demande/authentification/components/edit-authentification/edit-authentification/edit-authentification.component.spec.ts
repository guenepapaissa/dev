import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditAuthentificationComponent } from './edit-authentification.component';

describe('EditAuthentificationComponent', () => {
  let component: EditAuthentificationComponent;
  let fixture: ComponentFixture<EditAuthentificationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditAuthentificationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditAuthentificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
