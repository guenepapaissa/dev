import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import { AuthentificationService } from '../../service/authentification.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Authentification } from '../../models/Authentification';


@Component({
  selector: 'app-add-authentification',
  templateUrl: './add-authentification.component.html',
  styleUrls: ['./add-authentification.component.scss'],
})

export class AddAuthentificationComponent implements OnInit {

  id !: number;
  currentAuth: any;
  closeResult = '';
  rvForm!: FormGroup;
  rvForm1!: FormGroup;
  newAuth !: Authentification[];
  courentbouton = 'rv';
  submitForm!: boolean;


  constructor(private activatedRoute: ActivatedRoute,
    private route: Router,
    private authentificationService: AuthentificationService,
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.params['id'];
    this.currentAuth = this.authentificationService.editerAuth(this.id);
    this.getAuthentification(this.id)
  }

  getAuthentification(id: number) {
    this.authentificationService.getAuthentification(id).subscribe((response: any) => {
      this.currentAuth = response;
      console.log(this.currentAuth)
      this.initForm()
    });
    this.initForm();
  }

  supprimer(motifRejet:string) {
    this.currentAuth.motifRejet = motifRejet;
    this.route.navigate(['/authentification'])
  }

  recuperer() {
    this.currentAuth.motifRejet = '';
    this.route.navigate(['/authentification'])
  }


  recupererback() {
    this.authentificationService.getAuthentification(this.currentAuth.id).subscribe((response: any) => {
      this.currentAuth = response;
      console.log(this.currentAuth)
      this.authentificationService.changeEtatAuthentification(
        this.currentAuth?.id,
        this.currentAuth?.prenom,
        this.currentAuth?.nom,
        this.currentAuth?.telephone,
        this.currentAuth?.email,
        this.currentAuth?.login,
        this.currentAuth?.password,
        this.currentAuth?.typeAgent,
        false
      ).subscribe({
        next: (res) => {
          console.log(res);
          this.route.navigate(["/agent"]);
        },
        error: (e) => console.error(e)
      });
    });
  }

  open(content: any, modal: string) {
    this.courentbouton = modal;
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  // private getDismissReason(reason: any): string {
  //   if (reason === ModalDismissReasons.ESC) {
  //     return 'by pressing ESC';
  //   } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
  //     return 'by clicking on a backdrop';
  //   } else {
  //     return `with: ${reason}`;
  //   }
  // }
  initForm() {
      this.rvForm = this.formBuilder.group({
        dateRv: ['', Validators.required],
        etat: ['en cour', Validators.required],
      })
      this.rvForm1 = this.formBuilder.group({
        motifRejet: ['', Validators.required],
      })
  }

  onSave() {
    // fixer date Rendez-vous
    if (this.courentbouton === 'fixer') {
      const x = new Date(this.rvForm.value['dateRv']);
      const y = new Date(this.currentAuth.dateDebut);
      const z = x.toDateString().substring(0, 3)
      console.log(z)
      if (x > y && z != 'Sat' && z != 'Sun') {
        const formValue = this.rvForm.value;
        const newAuth = new Authentification(
          this.currentAuth.id,
          this.currentAuth.numAuthentfication,
          this.currentAuth.nom,
          this.currentAuth.prenom,
          this.currentAuth.adresse,
          this.currentAuth.num,
          this.currentAuth.dateDebut,
          this.currentAuth.heure,
          this.currentAuth.motifRejet,
          formValue['etat'],
          formValue['dateRv'],
        )
        console.log(newAuth);
        this.authentificationService.ajouterAuth(newAuth)
        this.authentificationService.updateAuthentification(
          formValue.prenom,
          formValue.nom,
          formValue.telephone,
          formValue.email,
          formValue.login,
          formValue.password,
          formValue.password,
          ).subscribe({
            next: (res) => {
              console.log(res);
              this.submitForm = true;
            },
            error: (e) => console.error(e)
          });
        this.route.navigate(['/authentification'])
      }
      else {
        alert("date incorrect");
        this.route.navigate(['/authentification/addauth', this.currentAuth.id]);
      }
    }

    // Rejet demande Authentification

    if (this.courentbouton === 'supprimer') {
      console.log('newAuth');
      const formValue = this.rvForm1.value;
      const newAuth = new Authentification(
        this.currentAuth.id,
        this.currentAuth.numAuthentfication,
        this.currentAuth.nom,
        this.currentAuth.prenom,
        this.currentAuth.adresse,
        this.currentAuth.num,
        this.currentAuth.dateDebut,
        this.currentAuth.heure,
        formValue['motifRejet'],
        this.currentAuth.etat,
        this.currentAuth.dateRv,
      )
      console.log(newAuth);
      this.supprimer(formValue['motifRejet']);
      this.authentificationService.updateAuthentification(
        formValue.prenom,
        formValue.nom,
        formValue.telephone,
        formValue.email,
        formValue.login,
        formValue.password,
        formValue.password,
        ).subscribe({
          next: (res) => {
            console.log(res);
            this.submitForm = true;
          },
          error: (e) => console.error(e)
        });
    }

  }

}

