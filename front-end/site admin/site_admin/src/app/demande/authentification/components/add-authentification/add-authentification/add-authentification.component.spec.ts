import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAuthentificationComponent } from './add-authentification.component';

describe('AddAuthentificationComponent', () => {
  let component: AddAuthentificationComponent;
  let fixture: ComponentFixture<AddAuthentificationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddAuthentificationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAuthentificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
