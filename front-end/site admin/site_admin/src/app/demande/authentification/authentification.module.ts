import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import {
  AvatarModule,
  ButtonGroupModule,
  ButtonModule,
  CardModule,
  FormModule,
  GridModule,
  NavModule,
  ProgressModule,
  TableModule,
  TabsModule
} from '@coreui/angular';
import { IconModule } from '@coreui/icons-angular';
import { ChartjsModule } from '@coreui/angular-chartjs';
import { AuthentificationRoutingModule } from './authentification-routing.module';
import { WidgetsModule } from 'src/app/views/widgets/widgets.module';



@NgModule({
  imports: [
    AuthentificationRoutingModule,
    CardModule,
    NavModule,
    IconModule,
    TabsModule,
    CommonModule,
    GridModule,
    ProgressModule,
    ReactiveFormsModule,
    ButtonModule,
    FormModule,
    ButtonModule,
    ButtonGroupModule,
    ChartjsModule,
    AvatarModule,
    TableModule,
    WidgetsModule,
    ReactiveFormsModule,
  ],
  // declarations: [
  //   ListAuthentificationComponent
  // ],
  // declarations: [
  //   MainContentAuthentificationComponent
  // ],
//   declarations: [DashboardComponent]
})

export class AuthentificationModule {
}
