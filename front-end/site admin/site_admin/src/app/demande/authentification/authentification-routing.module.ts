import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AddAuthentificationComponent } from "./components/add-authentification/add-authentification/add-authentification.component";
import { DelAuthentificationComponent } from "./components/del-authentification/del-authentification/del-authentification.component";
import { EditAuthentificationComponent } from "./components/edit-authentification/edit-authentification/edit-authentification.component";
import { MainContentAuthentificationComponent } from "./components/main-content-authentification/main-content-authentification/main-content-authentification.component";
import { RejetAuthentificationComponent } from "./components/rejet-authentification/rejet-authentification/rejet-authentification.component";

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Authentification',
    },
    children: [
      {
        path: '',
        redirectTo :"",
        pathMatch:'full',
        component: MainContentAuthentificationComponent,
        data: {
          title:`Authentifiication`
        },
      },
      {
        path: 'addauth/:id',
        component: AddAuthentificationComponent,
        data: {
          title: $localize`Ajout Authentification`
        }
      },
      {
        path: 'editauth/:id',
        component: EditAuthentificationComponent,
        data: {
          title: $localize`Modifier Authentification`
        }
      },

      {
        path: 'delauth/:id',
        component: DelAuthentificationComponent,
        data: {
          title: $localize`supprimer Authentification`
        }
      }
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]

})
export class AuthentificationRoutingModule {

}
