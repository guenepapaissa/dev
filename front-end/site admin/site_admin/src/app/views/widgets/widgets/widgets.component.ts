import { AfterContentInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { AgentService } from 'src/app/agent/components/service/agent.service';
import { DecesService } from 'src/app/demande/declaration/deces/components/service/declerationDeces.service';
import { MariageService } from 'src/app/demande/declaration/mariage/service/declarationMariage.service';
import { NaissanceService } from 'src/app/demande/declaration/naissance/service/naissance.service';
import { RenouvellementService } from 'src/app/demande/renouvellement/components/service/renouvellement.services';
import { ToutGenreService } from 'src/app/demande/toutgenre/components/service/toutGenre.service';
import { UserService } from 'src/app/user/components/services/user.service';

@Component({
  selector: 'app-widgets',
  templateUrl: './widgets.component.html',
  styleUrls: ['./widgets.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class WidgetsComponent implements OnInit,AfterContentInit {
  renouvellements: any
  toutGenre: any
  mariages: any
  deces: any
  naissance: any
  totalDemandes = 0
  enCour = 0
  terminer = 0
  rejeter = 0
  alltotalDemandes = 0
  allenCour = 0
  allterminer = 0
  allrejeter = 0
  totalAgent = 0
  agentnonBloquer = 0
  agentBloquer = 0

  client = 0
  agents: any
  typeAgent = localStorage.getItem('typeAgent') as string
  profil = localStorage.getItem('profil') as string
  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private renouvellementService: RenouvellementService,
    private toutGenreService: ToutGenreService,
    private naissanceService: NaissanceService,
    private mariageService: MariageService,
    private decesService: DecesService,
    private agentService: AgentService,
    private userService: UserService
  ) { }
    ngOnInit(): void {
      if (this.typeAgent === 'mariage') {
        this.getMariage()
      }
      if (this.typeAgent === 'naissance') {
        this.geNaissance();
      }

      if (this.typeAgent === 'deces') {
        this.getDeces()
      }
      if (this.typeAgent === 'renouvellement') {
        this.getRenouvellement()
      }
      if (this.typeAgent === 'genre') {
        this.getToutGenre()
      }
      if (this.profil === 'officier') {
        this.geNaissance();
        this.getDeces();
        this.getMariage();
        this.getRenouvellement();
        this.getToutGenre();

      }
      if(this.profil==='administrateur'){
        this.getAgents()
      }

    }
  ngAfterContentInit(): void {
    this.changeDetectorRef.detectChanges();

  }
  getAgents() {
    this.agentService.getAgents().subscribe((response) => {
      this.totalAgent = response.length
      console.log(this.totalAgent)
      this.agents = response
      for (let i = 0; i < this.totalAgent; i++) {
        if (this.agents[i].etat === true) {
          this.agentnonBloquer++
        }
        else {
          this.agentBloquer++
        }
      }
    })
    this.userService.getUsers().subscribe((response)=>{
      this.client=response.length
    })
  }
  getRenouvellement() {
    this.renouvellementService.getRenouvellements().subscribe((response) => {
      this.renouvellements = response
      this.totalDemandes = this.renouvellements?.length
      this.alltotalDemandes += this.totalDemandes
      console.log(this.renouvellements)
      this.enCour = 0
      this.terminer = 0
      this.rejeter = 0
      for (let i = 0; i < this.renouvellements?.length; i++) {
        if (this.renouvellements[i]?.etat === false && !this.renouvellements[i]?.motif) {
          this.enCour++
          this.allenCour++
        }
        if (this.renouvellements[i]?.etat === true) {
          this.terminer++
          this.allterminer++
        }
        if (this.renouvellements[i]?.motif) {
          this.rejeter++
          this.allrejeter++
        }
      }
      console.log(this.totalDemandes + ' o' + this.enCour + " " + this.terminer + " " + this.rejeter)

    })
  }

  getToutGenre() {
    this.toutGenreService.getToutGenres().subscribe((response: any) => {
      this.toutGenre = response
      console.log(this.toutGenre)
      this.totalDemandes = this.toutGenre?.length
      this.alltotalDemandes += this.totalDemandes

      this.enCour = 0
      this.terminer = 0
      this.rejeter = 0
      for (let i = 0; i < this.toutGenre?.length; i++) {
        if (this.toutGenre[i]?.etat === false && !this.toutGenre[i]?.motif) {
          this.enCour++
          this.allenCour++
        }
        if (this.toutGenre[i]?.etat === true) {
          this.terminer++
          this.allterminer++
        }
        if (this.toutGenre[i]?.motif) {
          this.rejeter++
          this.allrejeter++
        }
      }
      console.log(this.totalDemandes + ' ' + this.enCour + " " + this.terminer + " " + this.rejeter)

    })
  }

  getMariage() {
    this.mariageService.getMariages().subscribe((response) => {
      this.mariages = response
      console.log(this.mariages)
      this.enCour = 0
      this.terminer = 0
      this.rejeter = 0
      this.totalDemandes = this.mariages?.length
      this.alltotalDemandes += this.totalDemandes

      for (let i = 0; i < this.mariages?.length; i++) {
        if (this.mariages[i]?.etat === false && !this.mariages[i]?.motif) {
          this.enCour++
          this.allenCour++
        }
        if (this.mariages[i]?.etat === true) {
          this.terminer++
          this.allterminer++
        }
        if (this.mariages[i]?.motif) {
          this.rejeter++
          this.allrejeter++
        }
      }
      console.log(this.totalDemandes + ' ' + this.enCour + " " + this.terminer + " " + this.rejeter)

    })
  }

  getDeces() {
    this.decesService.getDecess().subscribe((response) => {
      this.deces = response
      console.log(this.deces)
      this.enCour = 0
      this.terminer = 0
      this.rejeter = 0
      this.totalDemandes = this.deces?.length
      this.alltotalDemandes += this.totalDemandes

      for (let i = 0; i < this.deces?.length; i++) {
        if (this.deces[i]?.etat === false && !this.deces[i]?.motif) {
          this.enCour++
          this.allenCour++
        }
        if (this.deces[i]?.etat === true) {
          this.terminer++
          this.allterminer++
        }
        if (this.deces[i]?.motif) {
          this.rejeter++
          this.allrejeter++
        }
      }
      console.log(this.totalDemandes + ' ' + this.enCour + " " + this.terminer + " " + this.rejeter)

    })
  }

  geNaissance() {
    this.naissanceService.getNaissances().subscribe((response) => {
      this.naissance = response
      this.enCour = 0
      this.terminer = 0
      this.rejeter = 0
      console.log(this.naissance)
      this.totalDemandes = this.naissance?.length
      this.alltotalDemandes += this.totalDemandes

      for (let i = 0; i < this.naissance?.length; i++) {
        if (this.naissance[i]?.etat === false && !this.naissance[i]?.motif) {
          this.enCour++
          this.allenCour++
        }
        if (this.naissance[i]?.etat === true) {
          this.terminer++
          this.allterminer++
        }
        if (this.naissance[i]?.motif) {
          this.rejeter++
          this.allrejeter++
        }
      }
      console.log(this.totalDemandes + ' ' + this.enCour + " " + this.terminer + " " + this.rejeter)

    })
  }

}
