import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Service } from 'src/app/service/service.module';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit{
  formClient!: FormGroup
  client!:any
  logError= false;
  submitForm!: boolean;
  constructor(private formBuilder: FormBuilder,private toastr: ToastrService, private servive: Service,private router: Router) {

   }
   ngOnInit(): void {
     this.initForm()
   }
   initForm(){
     this.formClient= this.formBuilder.group({
       prenom:['',Validators.required],
       nom:['',Validators.required],
       telephone:['',[Validators.required,Validators.minLength(9),Validators.maxLength(12),]],
       email:['',[Validators.required,Validators.email]],
       login:['',Validators.required],
       password:['',Validators.required],
       confPassword:['',Validators.required]
     })
   }
   onSubmit(){
     const formValue = this.formClient.value
    //  this.client=[
    //  formValue.prenom,
    //  formValue.nom,
    //  formValue.telephone,
    //  formValue.email,
    //  formValue.login,
    //  formValue.password,
    //  formValue.confPassword
    //  ]

     if(formValue.password!=formValue.confPassword){
      this.logError=true
     }else{
      this.servive.inscrireClient(formValue.prenom,
        formValue.nom,
        formValue.telephone,
        formValue.email,
        formValue.login,
        formValue.password,)
        .subscribe({
          next: (res)=>{
            console.log(res);
            this.submitForm = true;
            this.router.navigate(['/sk'])
          },
          error: (e) =>  this.toastr.warning('veuilliez renseigner tous les champs !', 'Connexion')
        })
        //this.router.navigate(['/sk'])
     }
   }
}
