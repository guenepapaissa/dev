import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Service } from 'src/app/service/service.module';
import { Utilisateur } from 'src/app/user/models/Utilisateur';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  userConnect: any;
  loginForm!: FormGroup;
  submitForm!: boolean;

  constructor(
    private service: Service,
    private formBuilder: FormBuilder,
    private router: Router,
    private toastr: ToastrService,
  ) { }

  ngOnInit(): void {
    this.initForm()
  }

  initForm() {
    this.loginForm = this.formBuilder.group({
      login: ['', Validators.required],
      password: ['', Validators.required]
    })
  }
  onSubmit() {
    const formValue = this.loginForm.value;
    this.service.logIn(formValue.login, formValue.password).subscribe((response: string) => {
      if (response === "erreur") {
        this.toastr.warning('login ou mot de passe incorrect !', 'Connexion');
      }
      else {
        this.userConnect = response
        //alert(JSON.stringify(this.userConnect))
        localStorage.setItem('idUser', this.userConnect?.id);
        localStorage.setItem('prenom', this.userConnect?.prenom);
        localStorage.setItem('nom', this.userConnect?.nom);
        localStorage.setItem('email', this.userConnect?.email);
        localStorage.setItem('login', this.userConnect?.login);
        localStorage.setItem('password', this.userConnect?.password);
        localStorage.setItem('typeAgent', this.userConnect?.typeAgent);
        localStorage.setItem('profil', this.userConnect?.profil);
        if(localStorage.getItem('profil')==='client'){
          this.router.navigate(['/sk'])
        }else{
          this.router.navigate(['/#/dasboard'])
        }

      }
    })

  }

}
