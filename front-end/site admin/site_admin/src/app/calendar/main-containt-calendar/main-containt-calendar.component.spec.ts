import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MainContaintCalendarComponent } from './main-containt-calendar.component';

describe('MainContaintCalendarComponent', () => {
  let component: MainContaintCalendarComponent;
  let fixture: ComponentFixture<MainContaintCalendarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MainContaintCalendarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainContaintCalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
