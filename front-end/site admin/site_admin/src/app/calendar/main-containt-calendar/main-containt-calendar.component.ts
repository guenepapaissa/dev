import { Component, OnInit, ViewChild } from '@angular/core';
import { SelectControlValueAccessor } from '@angular/forms';
import { CalendarOptions, FullCalendarComponent } from '@fullcalendar/angular';
import allLocales from '@fullcalendar/core/locales-all';
import { Callbacks } from 'jquery';
import { Authentification } from 'src/app/demande/authentification/components/models/Authentification';
import { AuthentificationService } from 'src/app/demande/authentification/components/service/authentification.service';
import { DecesService } from 'src/app/demande/declaration/deces/components/service/declerationDeces.service';
import { MariageService } from 'src/app/demande/declaration/mariage/service/declarationMariage.service';
import { Naissance } from 'src/app/demande/declaration/naissance/models/Naissance';
import { NaissanceService } from 'src/app/demande/declaration/naissance/service/naissance.service';
import { UserService } from 'src/app/user/components/services/user.service';


@Component({
  selector: 'app-main-containt-calendar',
  templateUrl: './main-containt-calendar.component.html',
  styleUrls: ['./main-containt-calendar.component.scss']
})
export class MainContaintCalendarComponent implements OnInit {
  naissances!: any;
  mariages!: any;
  decess!: any;
  even = [{}]
  client: any
  titre: any
  rv: any
  stop = true
  indi!: number
  typeAgent = localStorage.getItem('typeAgent') as string
  idAgent = localStorage.getItem('idUser')

  calendarOptions!: CalendarOptions
  @ViewChild('calendar') calendarComponent: FullCalendarComponent | undefined;
  constructor(private naissanceService: NaissanceService,
    private userService: UserService,
    private mariageService: MariageService,
    private decesService: DecesService
  ) {

  }

  getClient(id: any) {

    this.userService.getUser(id).subscribe((response) => {
      this.client = response.nom;
      return { title: this.client, start: this.naissances[id]?.rendezVous };
    })

  }

  getNaissances() {

    this.naissanceService.getNaissances().subscribe((response) => {
      this.naissances = response;


      for (let indice = 0; indice < this.naissances?.length; indice++) {

          setTimeout(() => {
            return this.userService.getUser(this.naissances[indice]?.client).subscribe((response) => {

              this.client = response.nom;

              this.even.push({ title: this.client, start: this.naissances[indice]?.rendezVous })

              console.log(JSON.stringify(this.even));

            })
          }, 2200);

          setTimeout(() => {
            this.calendarOptions = {
              headerToolbar: {
                left: 'title,prev,next',
                center: 'today',
                right: 'dayGridMonth,timeGridWeek,timeGridDay'
              },

              locale: 'fr',
              locales: allLocales,
              initialView: 'dayGridMonth',
              dateClick: this.handleDateClick.bind(this), // bind is important!
              weekends: false,
              events: this.even
            };
          }, 2500);


      }
      //  console.log('data '+ JSON.stringify(data))

      //  console.log(JSON.stringify(this.even))

    })

  }

  getMariages() {

    this.mariageService.getMariages().subscribe((response) => {
      this.mariages = response;


      for (let indice = 0; indice < this.mariages?.length; indice++) {

          this.indi = indice

          setTimeout(() => {
            return this.userService.getUser(this.mariages[indice]?.client).subscribe((response) => {

              this.client = response.nom;

              this.even.push({ title: this.client, start: this.mariages[indice]?.rendezVous })

              console.log(JSON.stringify(this.even));

            })
          }, 2200);

          setTimeout(() => {
            this.calendarOptions = {
              headerToolbar: {
                left: 'title,prev,next',
                center: 'today',
                right: 'dayGridMonth,timeGridWeek,timeGridDay'
              },

              locale: 'fr',
              locales: allLocales,
              initialView: 'dayGridMonth',
              dateClick: this.handleDateClick.bind(this), // bind is important!
              weekends: false,
              events: this.even
            };
          }, 2500);


      }
      //  console.log('data '+ JSON.stringify(data))

      //  console.log(JSON.stringify(this.even))

    })

  }

  getDecess() {

    this.decesService.getDecess().subscribe((response) => {
      this.decess = response;


      for (let indice = 0; indice < this.decess?.length; indice++) {

          this.indi = indice

          setTimeout(() => {
            return this.userService.getUser(this.decess[indice]?.client).subscribe((response) => {

              this.client = response.nom;

              this.even.push({ title: this.client, start: this.decess[indice]?.rendezVous })

              console.log(JSON.stringify(this.even));

            })
          }, 2200);

          setTimeout(() => {
            this.calendarOptions = {
              headerToolbar: {
                left: 'title,prev,next',
                center: 'today',
                right: 'dayGridMonth,timeGridWeek,timeGridDay'
              },

              locale: 'fr',
              locales: allLocales,
              initialView: 'dayGridMonth',
              dateClick: this.handleDateClick.bind(this), // bind is important!
              weekends: false,
              events: this.even
            };
          }, 2500);

      }
      //  console.log('data '+ JSON.stringify(data))

      //  console.log(JSON.stringify(this.even))

    })

  }

  toggleWeekends() {
    this.calendarOptions.weekends = !this.calendarOptions.weekends // toggle the boolean!
  }

  ngOnInit(): void {
    console.log(this.typeAgent)

    if (this.typeAgent === 'mariage') {
      this.getMariages()
    }
    if (this.typeAgent === 'naissance') {
      this.getNaissances();
    }

    if (this.typeAgent === 'deces') {
      this.getDecess()
    }
  }

  handleDateClick(arg: { dateStr: string; }) {
    alert(arg.dateStr)
  }

  // calendarOption(){
  //   console.log('execution')
  //   this.calendarOptions   = {
  //     headerToolbar: {
  //       left: 'title,prev,next',
  //       center: 'today' ,
  //      right: 'dayGridMonth,timeGridWeek,timeGridDay'
  //     },

  //     locale: 'fr',
  //     locales: allLocales,
  //     initialView: 'dayGridMonth',
  //     dateClick: this.handleDateClick.bind(this), // bind is important!
  //     weekends: false,
  //     events: this.even
  //   };

  // }


}

 //   console.log("indice : " + indice + " date est" + day + " " + month + " " + year)
   //event.push(){}
        //  this.calendarOptions.events[1].title= this.naissances[indice].nom
        // this.even +=  {title: this.naissances[indice].nom+" "+ this.naissances[indice].prenom, dateEv: this.naissances[indice].dateRv}
  // events :[
      //         {title: "issa", date: "17-03-2022"},]

         //  buttonText: {
      //     today:    'today',
      //     month:    'month',
      //     week:     'week',
      //     day:      'day',
      //     list:     'list'
      //   },


         // this.titre=this.getClient(this.naissances[0]?.client)
    // this.rv=this.naissances[0]?.rendezVous
    //  console.log(this.titre)
    //  console.log(this.rv)


            // this.titre=this.getClient(this.naissances[0]?.client)
        //  this.rv=this.naissances[0]?.rendezVous


         // this.getNaissances()
    //console.log( 'ng '+this.naissances[0]?.id)

    //this.even = [{ title: this.naissances[0].nom + " " + this.naissances[0].prenom, date: this.naissances[0].dateRv,  }]

    // const day = this.naissances[indice].rendezVous?.getDay();
    // const month = this.naissances[indice].rendezVous?.getMonth();
    // const year = this.naissances[indice].rendezVous?.getFullYear();
    // const a = ""
    // this.rv=this.naissances[indice]?.rendezVous
    // this.titre = this.getClient(this.naissances[indice]?.client)
    // Array.prototype.push.apply(this.even, [{ title: this.titre, start: this.naissances[indice]?.rendezVous,}]);



      // pause(): any{
  //   return new Promise((resolve)=>{
  //     setTimeout(()=>
  //     {

  //       resolve(true);
  //     }, 2000)
  //   })
  // }


  // Array.prototype.push.apply( this.even,[{ title: this.client,
            //   start: this.naissances[0]?.rendezVous,
            //  }])
             //this.even.push({title:this.client, start: this.naissances[id]?.rendezVous})

           // console.log(JSON.stringify(this.even))


                     // setTimeout(() => {
          //   return this.httpClient
          //     .get('http://localhost:8888/event.php')
          //     .subscribe((res: any) => {
          //       this.Events.push(res);
          //       console.log(this.Events);
          //     });
          // }, 2200);
