import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MainContentOfficierComponent } from './main-content-officier.component';

describe('MainContentOfficierComponent', () => {
  let component: MainContentOfficierComponent;
  let fixture: ComponentFixture<MainContentOfficierComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MainContentOfficierComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainContentOfficierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
