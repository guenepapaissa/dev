import { Component, OnInit } from '@angular/core';
import { OfficierService } from '../../service/officier.service';

@Component({
  selector: 'app-main-content-officier',
  templateUrl: './main-content-officier.component.html',
  styleUrls: ['./main-content-officier.component.scss']
})
export class MainContentOfficierComponent implements OnInit {
agents:any
declarations:any
renouvellements:any
toutgenres:any
naissances:any
decess:any
mariages:any
courentSection = "renouvellement";
  constructor(private officierService: OfficierService) { }

  ngOnInit(): void {
    this.getAgents()
    this.getRenouvellements()
    this.getToutGenres()
    this.getNaissances()
    this.getMariages()
    this.getDeces
  }
  getAgents(){
    this.officierService.getAgents().subscribe((response)=>{
      this.agents=response
    })
  }
  getRenouvellements(){
    this.officierService.getRenouvellements().subscribe((response)=>{
      this.renouvellements=response;

    })
  }
  getToutGenres(){
    this.officierService.getToutGenres().subscribe((response)=>{
      this.toutgenres=response
      console.log(this.toutgenres)
    })
  }
  getNaissances(){
    this.officierService.getNaissances().subscribe((response)=>{
      this.naissances=response;
    })
  }
  getDeces(){
    this.officierService.getDecess().subscribe((response)=>{
      this.decess=response
    })
  }
  getMariages(){
    this.officierService.getMariages().subscribe((response)=>{
      this.mariages=response
      console.log(this.mariages)
    })
  }

  changeSection(section: string) {
    this.courentSection = section;
  }
}
