import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { OfficierService } from '../../service/officier.service';

@Component({
  selector: 'app-rejet-officier',
  templateUrl: './rejet-officier.component.html',
  styleUrls: ['./rejet-officier.component.scss']
})
export class RejetOfficierComponent implements OnInit {
  agents:any
  declarations:any
  renouvellements:any
  toutgenres:any
  naissances:any
  decess:any
  mariages:any
  closeResult = '';
  id!:number
  currentMotif:any
  courentSection = "renouvellement";
    constructor(private officierService: OfficierService,private modalService: NgbModal,) { }

    ngOnInit(): void {
      this.getAgents()
      this.getRenouvellements()
      this.getToutGenres()
      this.getNaissances()
      this.getMariages()
      this.getDeces
    }
    getAgents(){
      this.officierService.getAgents().subscribe((response)=>{
        this.agents=response
      })
    }
    getRenouvellements(){
      this.officierService.getRenouvellements().subscribe((response)=>{
        this.renouvellements=response;

      })
    }
    getToutGenres(){
      this.officierService.getToutGenres().subscribe((response)=>{
        this.toutgenres=response
        console.log(this.toutgenres)
      })
    }
    getNaissances(){
      this.officierService.getNaissances().subscribe((response)=>{
        this.naissances=response;
      })
    }
    getDeces(){
      this.officierService.getDecess().subscribe((response)=>{
        this.decess=response
      })
    }
    getMariages(){
      this.officierService.getMariages().subscribe((response)=>{
        this.mariages=response
        console.log(this.mariages)
      })
    }

    changeSection(section: string) {
      this.courentSection = section;
    }
    openSm(content: any, id: number) {
      this.id=id
      this.getMotif(id)
      this.modalService.open(content,{ size: 'sm' }).result.then((result: any) => {
        this.closeResult = `Closed with: ${result}`;
      }, (reason: any) => {
     //   this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
    }
    getMotif(id:number){
      this.currentMotif=this.officierService.getMotif(id).subscribe((response)=>{
        this.currentMotif=response
      })
    }
}
