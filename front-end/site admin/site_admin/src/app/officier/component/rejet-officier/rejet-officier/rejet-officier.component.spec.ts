import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RejetOfficierComponent } from './rejet-officier.component';

describe('RejetOfficierComponent', () => {
  let component: RejetOfficierComponent;
  let fixture: ComponentFixture<RejetOfficierComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RejetOfficierComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RejetOfficierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
