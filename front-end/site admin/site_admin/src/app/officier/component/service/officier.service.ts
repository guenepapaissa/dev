import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class OfficierService {
  api = environment.api;
  id !: any;
  constructor(private http: HttpClient){}
  getAgents() {
    return this.http.get<any>(this.api + 'agent/');
  }
  getDeclarations(id:any) {
    return this.http.get<any>(this.api + 'declaration/'+id);
  }
  getRenouvellements() {
    return this.http.get<any>(this.api + 'renouvellement/');
  }
  getRenouvellement(id:any) {
    return this.http.get<any>(this.api + 'renouvellement/'+id);
  }
  getToutGenres() {
    return this.http.get<any>(this.api + 'toutGenre/');
  }
  getToutGenre(id: any){
    return this.http.get<any>(this.api+'toutGenre/'+id)
  }
  getMariages(){
    return this.http.get<any>(this.api+'declaration_mariage/')
  }
  getNaissances(){
    return this.http.get<any>(this.api+'declaration_naissance/')
  }
  getDecess(){
    return this.http.get<any>(this.api+'declaration_deces/')
  }
  getMotif(id: any){
    return this.http.get<any>(this.api+'motif/'+id)
  }
}
