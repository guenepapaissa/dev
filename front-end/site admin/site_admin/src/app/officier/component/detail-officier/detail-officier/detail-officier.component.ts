import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { AgentService } from 'src/app/agent/components/service/agent.service';

@Component({
  selector: 'app-detail-officier',
  templateUrl: './detail-officier.component.html',
  styleUrls: ['./detail-officier.component.scss']
})
export class DetailOfficierComponent implements OnInit {





  // agents : Agent[];
  agents: any;
  officiers: any;
  administrateurs: any;
  active = 1;
  agentcurrent: any;
  officiercurrent: any;
  name!: string;
  agentForm!: FormGroup;
  submitForm= false
  courentSection='agent';
  closeResult = '';
  id!:number;


  constructor(private agentService: AgentService, private toastr: ToastrService, private route: Router, private modalService: NgbModal,) {

  }

  ngOnInit(): void {

    this.getAgents()
    this.getOfficiers()
    //this.getAdministrateurs()

  }
  open(content: any, id: number) {
    this.id = id
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {

      //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      console.log(reason)
    });
  }

  // private getDismissReason(reason: any): string {
  //   if (reason === ModalDismissReasons.ESC) {
  //     return 'by pressing ESC';
  //   } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
  //     return 'by clicking on a backdrop';
  //   } else {
  //     return `with: ${reason}`;
  //   }
  // }
  changeSection(section : string) {
    this.courentSection = section;
  }
  supprimerAgent(): void {
    this.agentService.getAgent(this.id).subscribe((response: any) => {
      this.agentcurrent = response;
      console.log( this.agentcurrent)
      this.agentService.changeEtatAgent(
        this.agentcurrent?.id,
        this.agentcurrent?.prenom,
        this.agentcurrent?.nom,
        this.agentcurrent?.telephone,
        this.agentcurrent?.email,
        this.agentcurrent?.login,
        this.agentcurrent?.password,
        this.agentcurrent?.typeAgent,
        true
        ).subscribe({
        next: (res) => {
          console.log(res);
          this.toastr.success(' avec succes','Suppression')
          this.getAgents()
         // this.route.navigate(["/agent"]);
        },
        error: (e) => console.error(e)
      });
    });
    // if (confirm(" voulez-vous bloquer  " + this.agentcurrent?.prenom + " " + this.agentcurrent?.nom)) {}
      this.submitForm = true


  }

  supprimerOfficier(): void {
    this.agentService.getOfficier(this.id).subscribe((response: any) => {
      this.officiercurrent = response;
      console.log( this.officiercurrent)
      this.agentService.changeEtatOfficier(
        this.officiercurrent?.id,
        this.officiercurrent?.prenom,
        this.officiercurrent?.nom,
        this.officiercurrent?.telephone,
        this.officiercurrent?.email,
        this.officiercurrent?.login,
        this.officiercurrent?.password,
        true
        ).subscribe({
        next: (res) => {
          console.log(res);
         this.getOfficiers()
        },
        error: (e) => console.error(e)
      });
    });
    // if (confirm(" voulez-vous bloquer  " + this.agentcurrent?.prenom + " " + this.agentcurrent?.nom)) {}
      this.submitForm = true


  }


  getAgents(){
    this.agentService.getAgents().subscribe(response => {
      this.agents = response;
      console.log(this.agents)
      //alert(JSON.stringify(this.agents))
    });
  }
  getOfficiers(){
    this.agentService.getOfficiers().subscribe(response => {
      this.officiers = response;
      console.log(this.officiers)
      //alert(JSON.stringify(this.agents))
    });
  }
  // getAdministrateurs(){
  //   this.agentService.getAdministrateurs().subscribe(response => {
  //     this.administrateurs = response;
  //     console.log(this.administrateurs)
  //     //alert(JSON.stringify(this.agents))
  //   });
  // }
  getAgent(id: number) {
    this.agentService.getAgent(id).subscribe((response: any) => {
      this.agentcurrent = response;
    });
  }

}
