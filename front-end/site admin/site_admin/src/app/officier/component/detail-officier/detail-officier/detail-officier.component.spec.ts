import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailOfficierComponent } from './detail-officier.component';

describe('DetailOfficierComponent', () => {
  let component: DetailOfficierComponent;
  let fixture: ComponentFixture<DetailOfficierComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailOfficierComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailOfficierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
