import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { DetailOfficierComponent } from "./component/detail-officier/detail-officier/detail-officier.component";
import { MainContentOfficierComponent } from "./component/main-content-officier/main-content-officier/main-content-officier.component";
import { RejetOfficierComponent } from "./component/rejet-officier/rejet-officier/rejet-officier.component";

const routes: Routes = [
{
  path: '',
  data: {
    title: `Sama Këyyit`
  },
  children: [
    {
      path: '',
      redirectTo: '',
      pathMatch: 'full',
      component: MainContentOfficierComponent,
      data: {
        title: $localize`Sama Këyyit`
      }
    },


  ]
}

];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]

})
export class OfficierRoutingModule {

}
