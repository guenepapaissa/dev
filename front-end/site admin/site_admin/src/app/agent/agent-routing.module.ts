import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AddAgentComponent } from "./components/add-agent/add-agent.component";
import { DelAgentComponent } from "./components/del-agent/del-agent.component";
import { DetailAgentComponent } from "./components/detail-agent/detail-agent.component";
import { EditAgentComponent } from "./components/edit-agent/edit-agent.component";
import { MainContentAgentComponent } from "./components/main-content-agent/main-content-agent.component";

const routes: Routes = [
  {
    path: '',
    data: {
      title: `Agent`
    },
    children: [
      {
        path: '',
        redirectTo: '',
        pathMatch: 'full',
        component: MainContentAgentComponent,
        data: {
          title: $localize`Agent`
        }
      },
      {
        path: 'addagent',
        component: AddAgentComponent,
        data: {
          title: $localize`Ajout Agent`
        }
      },
      {
        path: 'editagent/:id',
        component: EditAgentComponent,
        data: {
          title: $localize`Modifier Agent`
        }
      },
      {
        path: 'detailagent/:id',
        component: DetailAgentComponent,
        data: {
          title: $localize`Detail Agent`
        }
      },
      {
        path: 'delagent',
        component: DelAgentComponent,
        data: {
          title: $localize`Detail Agent`
        }
      }
    ]
  },

];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]

})
export class AgentRoutingModule {

}
