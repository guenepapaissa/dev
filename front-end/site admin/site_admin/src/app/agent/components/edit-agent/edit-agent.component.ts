import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { min } from 'rxjs';
import { Agent } from '../models/Agent';
import { AgentService } from '../service/agent.service';


@Component({
  selector: 'app-edit-agent',
  templateUrl: './edit-agent.component.html',
  styleUrls: ['./edit-agent.component.scss']
})
export class EditAgentComponent implements OnInit {

  id !: number;
  currentAgent!: any;
  currentOfficier!: any;
  agentForm!: FormGroup;
  guichetCurrent!: string;
  mdpDefault = "samayKeyyit";
  newAgent: any;

  constructor(private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private route: Router,
    private agentService: AgentService) {

  }

  ngOnInit(): void {

    this.id = this.activatedRoute.snapshot.params['id'];
    this.getAgent(this.id)
    this.getOfficier(this.id)
    // this.currentAgent= this.agentService.editerAgent(this.id);

  }

  getAgent(id: number) {
    this.agentService.getAgent(id).subscribe((response: any) => {
      this.currentAgent = response;
      this.guichetCurrent = this.currentAgent?.typeAgent;
      console.log(this.guichetCurrent)
      this.initForm()
    });
    this.initForm();
  }

  getOfficier(id: number) {
    this.agentService.getOfficier(id).subscribe((response: any) => {
      this.currentOfficier = response;
      console.log(this.currentOfficier)
      this.initForm()
    });
    this.initForm();
  }
  updateAgent() {
    //this.agentService.updateAgent(this.currentAgent);
    this.route.navigate(["editagent"]);
  };
  initForm() {
    if (this.currentAgent?.typeAgent) {
      this.agentForm = this.formBuilder.group({
        prenom: [this.currentAgent?.prenom, Validators.required],
        nom: [this.currentAgent?.nom, Validators.required],
        telephone: [this.currentAgent?.telephone, [Validators.required,Validators.minLength(9),Validators.maxLength(12)]],
        typeAgent: [this.currentAgent?.typeAgent, Validators.required,],
        email: [this.currentAgent?.email, [Validators.required, Validators.email]]
      })
    } else {
      this.agentForm = this.formBuilder.group({
        prenom: [this.currentOfficier?.prenom, Validators.required],
        nom: [this.currentOfficier?.nom, Validators.required],
        telephone: [this.currentOfficier?.telephone, Validators.required],
        email: [this.currentOfficier?.email, [Validators.required, Validators.email]]
      })
    }
  }

  onSubmitFormAgent() {
    const formValue = this.agentForm.value;
      // Agent
      this.agentService.updateAgent(
        this.id,
        formValue.prenom,
        formValue.nom,
        formValue.telephone,
        formValue.email,
        this.currentAgent.login,
        this.currentAgent.password,
        formValue.typeAgent,
      ).subscribe({
        next: (res) => {
          console.log(res);
        },
        error: (e) => console.error(e)
      });

    this.route.navigate(['/agent'])
  }
  onSubmitFormOfficier() {
    const formValue = this.agentForm.value;
      // Officier
      this.agentService.updateOfficier(
        this.id,
        formValue.prenom,
        formValue.nom,
        formValue.telephone,
        formValue.email,
        this.currentOfficier.login,
        this.currentOfficier.password,
      ).subscribe({
        next: (res) => {
          this.route.navigate(['/agent'])
          console.log(res);
        },
        error: (e) => console.error(e)
      });

}

}
