import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Agent } from '../models/Agent';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AgentService {

  // filter(arg0: (agent: { name: string; }) => boolean): Agent[] {
  //   throw new Error('Method not implemented.');
  // }
  // getALL() {
  //   throw new Error('Method not implemented.');
  // }
  // getAll(): Agent[] {
  //   throw new Error('Method not implemented.');
  // }

  api = environment.api;
  agent!: Agent[];
  id !: any;
  agentcurrent: any;

  constructor(private http: HttpClient) {
  }
  getAgents() {
    return this.http.get<any>(this.api + 'agent/');
  }

  getOfficiers() {
    return this.http.get<any>(this.api + 'officier/');
  }

  getAdministrateurs() {
    return this.http.get<any>(this.api + 'administrateur/');
  }

  getAgent(id: any) {
    return this.http.get<any>(this.api + 'agent/' + id);
  }
  getOfficier(id: any) {
    return this.http.get<any>(this.api + 'officier/' + id);
  }
  getAdministrateur(id: any) {
    return this.http.get<any>(this.api + 'administrateur/' + id);
  }


  createAgent(prenom: string, nom: string, telephone: string, email: string, login: string, password: string, typeAgent: string) {
    let addAgent = {
      "prenom": prenom,
      "nom": nom,
      "telephone": telephone,
      "email": email,
      "login": login,
      "password": password,
      "typeAgent": typeAgent,
    }

   return this.http
      .post<any>(`${this.api}agent/`,addAgent);
  }

  createOfficier(prenom: string, nom: string, telephone: string, email: string, login: string, password: string) {
    let addOfficier = {
      "prenom": prenom,
      "nom": nom,
      "telephone": telephone,
      "email": email,
      "login": login,
      "password": password,
    }
   // return this.http.post(this.api + 'agent/', addAgent);
   return this.http
      .post<any>(`${this.api}officier/`,addOfficier);
  }
  createAdministrateur(prenom: string, nom: string, telephone: string, email: string, login: string, password: string) {
    let addAdministrateur = {
      "prenom": prenom,
      "nom": nom,
      "telephone": telephone,
      "email": email,
      "login": login,
      "password": password,
    }

   return this.http
      .post<any>(`${this.api}administrateur/`,addAdministrateur);
  }
  updateAgent(id: any,prenom: string, nom: string, telephone: string, email: string, login: string, password: string, typeAgent: string){
    let update = {
      "prenom": prenom,
      "nom": nom,
      "telephone": telephone,
      "email": email,
      "login": login,
      "password": password,
      "typeAgent": typeAgent,
    }
    return this.http.put(`${this.api}agent/${id}`,update);
  }

  updateOfficier(id: any,prenom: string, nom: string, telephone: string, email: string, login: string, password: string){
    let update = {
      "prenom": prenom,
      "nom": nom,
      "telephone": telephone,
      "email": email,
      "login": login,
      "password": password,
    }
    return this.http.put(`${this.api}officier/${id}`,update);
  }

  changeEtatAgent(id: any,prenom: string, nom: string, telephone: string, email: string, login: string, password: string, typeAgent: string,etat:boolean){
    let update = {
      "prenom": prenom,
      "nom": nom,
      "telephone": telephone,
      "email": email,
      "login": login,
      "password": password,
      "typeAgent": typeAgent,
      "etat":etat
    }
    return this.http.put(`${this.api}agent/${id}`,update);
  }
  changeEtatOfficier(id: any,prenom: string, nom: string, telephone: string, email: string, login: string, password: string,etat:boolean){
    let update = {
      "prenom": prenom,
      "nom": nom,
      "telephone": telephone,
      "email": email,
      "login": login,
      "password": password,
      "etat":etat
    }
    return this.http.put(`${this.api}officier/${id}`,update);
  }
  deleteAgent(id: any){
    return this.http.delete(`${this.api}agent/${id}`);
  }

  // createAgent(agent:any):Observable<any>{
  //   return this.http.post<any>(this.api + 'agent/',agent)
  // }

  // putAgent(id: any, data:any){
  //   return this.http.put(`${this.api}` +'agent/'+`/${id}`, data);
  // }
  listAgent(): Agent[] {
    return this.agent;
  }
  editerAgent(idUser: number): Agent[] {
    this.agentcurrent = this.agent.find(p => p.id == idUser)
    return this.agentcurrent;
  }
  detailAgent(idUser: number): Agent[] {
    this.agentcurrent = this.agent.find(p => p.id == idUser)
    return this.agentcurrent;
  }

  ajouterAgent(addagent: Agent) {
    this.agent.push(addagent);
  }

}
