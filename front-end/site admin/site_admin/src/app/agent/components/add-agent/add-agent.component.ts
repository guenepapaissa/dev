import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, PatternValidator, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Agent } from '../models/Agent';
import { AgentService } from '../service/agent.service';

@Component({
  selector: 'app-add-agent',
  templateUrl: './add-agent.component.html',
  styleUrls: ['./add-agent.component.scss']
})
export class AddAgentComponent implements OnInit {
  name!: string;
  agentForm!: FormGroup;
  newAgent !: Agent[];
  submitForm= false

  constructor(private agentService: AgentService, private formBuilder: FormBuilder, private router: Router) {
  }

  ngOnInit(): void {
    this.initForm();
  }
  initForm() {
    this.agentForm = this.formBuilder.group({
      prenom: ['', Validators.required],
      nom: ['', Validators.required],
      telephone: ['', [Validators.required,Validators.minLength(9),Validators.maxLength(12),]],
      email: ['', [Validators.required, Validators.email]],
      login: ['', Validators.required],
      password: ['', Validators.required],
      typeAgent: ['', Validators.required],
    })
  }

  onSubmitForm(): void {
    this.submitForm = true
    const formValue = this.agentForm.value;

    this.newAgent= [
      formValue['prenom'],
      formValue['nom'],
      formValue['telephone'],
      formValue['email'],
      formValue['login'],
      formValue['password'],
      formValue['typeAgent'],
      false
    ];
    // console.log(this.newAgent)
    // if( formValue.typeAgent==='administrateur'){
    //   this.agentService.createAdministrateur(
    //     formValue.prenom,
    //     formValue.nom,
    //     formValue.telephone,
    //     formValue.email,
    //     formValue.login,
    //     formValue.password,).subscribe({
    //       next: (res) => {
    //         console.log(res);
    //         this.submitForm = true;
    //       },
    //       error: (e) => console.error(e)
    //     });
    // }
    // if( formValue.typeAgent==='officier'){
    //   this.agentService.createOfficier(
    //     formValue.prenom,
    //     formValue.nom,
    //     formValue.telephone,
    //     formValue.email,
    //     formValue.login,
    //     formValue.password,).subscribe({
    //       next: (res) => {
    //         console.log(res);
    //         this.submitForm = true;
    //       },
    //       error: (e) => console.error(e)
    //     });
    // }
    //if(formValue.typeAgent!='officier' && formValue.typeAgent!='administrateur'){
    this.agentService.createAgent(
      formValue.prenom,
      formValue.nom,
      formValue.telephone,
      formValue.email,
      formValue.login,
      formValue.password,
      formValue.typeAgent).subscribe({
        next: (res) => {
          console.log(res);
          this.router.navigate(['/agent'])
          this.submitForm = true;
        },
        error: (e) => console.error(e)
      });
    //}



        // this.agentService.createAgent(this.newAgent).subscribe({
    //   next: (res) => {
    //     console.log(res);
    //     this.submitForm = true;
    //   },
    //   error: (e) => console.error(e)
    // });

    // this.agentService.createAgent(
    //   formValue.prenom,
    //   formValue.nom,
    //   formValue.telephone,
    //   formValue.email,
    //   formValue.login,
    //   formValue.password,
    //   formValue.guichet,
    //   false).subscribe(
    //     (result) => {
    //       console.log({ result: result });
    //       this.submitForm = false;
    //       this.router.navigate(['/agent'])
    //     }
    //   )

  }
  // addAgent(){
  //   this.agentService.ajouterAgent();
  //  // console.log(this.newAgent)
  // }
  // onSubmit(form:NgForm){
  //   console.log(form.value);
  // }
}


