import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AgentService } from '../service/agent.service';

@Component({
  selector: 'app-detail-agent',
  templateUrl: './detail-agent.component.html',
  styleUrls: ['./detail-agent.component.scss']
})
export class DetailAgentComponent implements OnInit {
  id !: number;
  currentAgent: any;
  currentOfficier: any;
  officier = false;
  administrateur = false
  currentAdministrateur: any;
  closeResult = '';
  profil!:string
  userConnect=localStorage.getItem('profil')

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal, private agentService: AgentService) { }

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.params['id'];
    this.getAgent(this.id)
    this.getOfficier(this.id)

  }
  open(content: any, profil: string) {
    this.profil=profil
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {

      //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      console.log(reason)
    });
  }
  // agent
  getAgent(id: number) {

    this.agentService.getAgent(id).subscribe({
      next: (res) => {
        console.log(res);
        this.currentAgent = res
      },
      error: (e) => console.error(e)
    });
  }
  // Officier
  getOfficier(id: number) {
    this.officier = true
    this.agentService.getOfficier(id).subscribe({
      next: (res) => {
        console.log(res);
        this.currentOfficier = res
      },
      error: (e) => console.error(e)
    });
  }
  // administrateur
  // getAdministrateur(id: number) {

  //   this.agentService.getAdministrateur(id).subscribe({
  //     next: (res) => {
  //        this.administrateur=true
  //       console.log(res);
  //       this.currentAdministrateur = res
  //     },
  //     error: (e) => console.error(e)
  //   });
  // }

  supprimerAgent() {
    if(this.profil==='agent'){
    this.agentService.changeEtatAgent(
      this.currentAgent?.id,
      this.currentAgent?.prenom,
      this.currentAgent?.nom,
      this.currentAgent?.telephone,
      this.currentAgent?.email,
      this.currentAgent?.login,
      this.currentAgent?.password,
      this.currentAgent?.typeAgent,
      true
    ).subscribe({
      next: (res) => {
        console.log(res);
        this.router.navigate(['/agent']);
      },
      error: (e) => console.error(e)
    });
  }
  }
  supprimerOfficier() {
    if(this.profil==='officier'){
    this.agentService.changeEtatOfficier(
      this.currentOfficier?.id,
      this.currentOfficier?.prenom,
      this.currentOfficier?.nom,
      this.currentOfficier?.telephone,
      this.currentOfficier?.email,
      this.currentOfficier?.login,
      this.currentOfficier?.password,
      true
    ).subscribe({
      next: (res) => {
        console.log(res);
        this.router.navigate(['/agent']);
      },
      error: (e) => console.error(e)
    });
  }
  }
  recupererAgent() {
    this.agentService.getAgent(this.currentAgent.id).subscribe((response: any) => {
      this.currentAgent = response;
      console.log(this.currentAgent)
      this.agentService.changeEtatAgent(
        this.currentAgent?.id,
        this.currentAgent?.prenom,
        this.currentAgent?.nom,
        this.currentAgent?.telephone,
        this.currentAgent?.email,
        this.currentAgent?.login,
        this.currentAgent?.password,
        this.currentAgent?.typeAgent,
        false
      ).subscribe({
        next: (res) => {
          console.log(res);
          this.router.navigate(["/agent"]);
        },
        error: (e) => console.error(e)
      });
    });
  }
  recupererOfficier() {
    this.agentService.getOfficier(this.currentOfficier.id).subscribe((response: any) => {
      this.currentOfficier = response;
      console.log(this.currentAgent)
      this.agentService.changeEtatOfficier(
        this.currentOfficier?.id,
        this.currentOfficier?.prenom,
        this.currentOfficier?.nom,
        this.currentOfficier?.telephone,
        this.currentOfficier?.email,
        this.currentOfficier?.login,
        this.currentOfficier?.password,
        false
      ).subscribe({
        next: (res) => {
          console.log(res);
          this.router.navigate(["/agent"]);
        },
        error: (e) => console.error(e)
      });
    });
  }


}
