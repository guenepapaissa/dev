export class Agent {
  constructor(
    public id : number,
    public prenom : string,
    public nom : string,
    public telephone : string,
    public email : string,
    public login : string,
    public password: string,
    public typeAgent : string,
    public etat : boolean,
    ) { };
}
