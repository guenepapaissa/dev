import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MainContentAgentComponent } from './main-content-agent.component';


describe('MainContentAgentComponent', () => {
  let component: MainContentAgentComponent;
  let fixture: ComponentFixture<MainContentAgentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MainContentAgentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainContentAgentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
