import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Agent } from '../models/Agent';
import { AgentService } from '../service/agent.service';

@Component({
  selector: 'app-del-agent',
  templateUrl: './del-agent.component.html',
  styleUrls: ['./del-agent.component.scss']
})
export class DelAgentComponent implements OnInit {

  agents: any;
  officiers: any;
  administrateurs: any;
  courentSection='agent';
  active = 1;


  constructor(private agentService: AgentService, private route: ActivatedRoute) {
    // this.agents = agentService.getAgents();
  }
  ngOnInit(): void {
    this.getAgents()
    this.getOfficiers()
    this.getAdministrateurs()
  }
  changeSection(section : string) {
    this.courentSection = section;
  }


  getAgents() {
    this.agentService.getAgents().subscribe(response => {
      this.agents = response;
      // alert(JSON.stringify(this.agents))
    });
  }
  getOfficiers(){
    this.agentService.getOfficiers().subscribe(response => {
      this.officiers = response;
      console.log(this.officiers)
      //alert(JSON.stringify(this.agents))
    });
  }
  getAdministrateurs(){
    this.agentService.getAdministrateurs().subscribe(response => {
      this.administrateurs = response;
      console.log(this.administrateurs)
      //alert(JSON.stringify(this.agents))
    });
  }
}
