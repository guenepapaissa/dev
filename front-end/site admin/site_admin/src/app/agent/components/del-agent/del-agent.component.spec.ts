import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DelAgentComponent } from './del-agent.component';

describe('DelAgentComponent', () => {
  let component: DelAgentComponent;
  let fixture: ComponentFixture<DelAgentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DelAgentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DelAgentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
