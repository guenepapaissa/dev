import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Service } from 'src/app/service/service.module';

import { navItems } from './_nav';
import { navItemsAdmin } from './_nav';
import { navItemsMariage } from './_nav';
import { navItemsNaissance } from './_nav';
import { navItemsDeces } from './_nav';
import { navItemsToutGenre } from './_nav';
import { navItemsRenouvellement } from './_nav';
import { navItemsOfficier } from './_nav';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html',
})
export class DefaultLayoutComponent implements OnInit {

 // public navItem = navItems;
 navItem:any
  profil: any
  typeAgent: any
  test='hector';

  public perfectScrollbarConfig = {
    suppressScrollX: true,
  };

  constructor(private service: Service, private router: Router) {

   // alert(JSON.stringify(this.userConnect))
    // if(this.test==='hector'){

    //   this.navItem=navItems
    // }


  }
  ngOnInit(): void {
    this.profil= localStorage.getItem('profil')
    this.typeAgent=localStorage.getItem('typeAgent')
    if(this.profil==='administrateur'){
      this.navItem=navItemsAdmin
    }
    if(this.typeAgent==='mariage'){
      this.navItem=navItemsMariage
    }
    if(this.typeAgent==='naissance'){
      this.navItem=navItemsNaissance
    }
    if(this.typeAgent==='deces'){
      this.navItem=navItemsDeces
    }
    if(this.typeAgent==='genre'){
      this.navItem=navItemsToutGenre
    }
    if(this.typeAgent==='renouvellement'){
      this.navItem=navItemsRenouvellement
    }
    if(this.profil==='officier'){
      this.navItem=navItemsOfficier
    }
    if(this.profil==='client'){
      this.router.navigate(['/sk'])
    }

    console.log(this.profil)
  }


}

