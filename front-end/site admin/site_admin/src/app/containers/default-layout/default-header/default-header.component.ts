import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { ClassToggleService, HeaderComponent } from '@coreui/angular';
import { Service } from 'src/app/service/service.module';

@Component({
  selector: 'app-default-header',
  templateUrl: './default-header.component.html',
})
export class DefaultHeaderComponent extends HeaderComponent implements OnInit {

  @Input() sidebarId: string = "sidebar";

  public newMessages = new Array(4)
  public newTasks = new Array(5)
  public newNotifications = new Array(5)
  nom!:string;
  prenom!:string;

  constructor(private classToggler: ClassToggleService, private service : Service) {
    super();
  }

  ngOnInit(): void {
    this.nom=localStorage.getItem('nom') as string;
    this.prenom=localStorage.getItem('prenom') as string;

  }
  goback(){
    console.log(window.history.back());
  }
  logOut(){
    this.service.logOut()
  }
}
