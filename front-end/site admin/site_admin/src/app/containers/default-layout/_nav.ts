import { INavData } from '@coreui/angular';

export const navItems: INavData[] = [
  {
    name: 'Tableau de bord',
    url: '/dashboard',
    iconComponent: { name: 'cil-speedometer' }

  },
  {
    name: 'Agent',
    url: '/agent',
    iconComponent: { name: 'cil-user' },

  },
  {
    name: 'Archive',
    url: '/delagent',
    iconComponent: { name: 'cil-align-left' },
  },
  {
    name: 'Utilisateur',
    url: '/user',
    iconComponent: { name: 'cil-people' },
  },
  {
    name: 'Utilisateur',
    url: '/adduser',
    iconComponent: { name: 'cil-user-follow' },
  },
  {
    name: 'Demande',
    title: true,
  },
  {
    name: 'Liste Demande',
    url: '/officier',
    iconComponent: { name: 'cil-align-center' },

  },
  {
    name: 'Archive',
    url: '/rejetDemande',
    iconComponent: { name: 'cil-align-left' },

  },
  {
    name: 'Naissance',
    url: '/naissance',
    iconComponent: { name: 'cil-pen' },
  },
  {
    name: 'Archive',
    url: '/delnaissance',
    iconComponent: { name: 'cil-align-left' },
  },
  {
    name: 'Mariage',
    url: '/mariage',
    iconComponent: { name: 'cil-pen' },
  },
  {
    name: 'Archive',
    url: '/delmariage',
    iconComponent: { name: 'cil-align-left' },
  },
  {
    name: 'Deces',
    url: '/deces',
    iconComponent: { name: 'cil-pen' },
  },
  {
    name: 'Archive',
    url: '/deldeces',
    iconComponent: { name: 'cil-align-left' },
  },
  {
    name: 'Renouvellement',
    url: '/renouvellement',
    iconComponent: { name: 'cil-spreadsheet' },
  },
  {
    name: 'Archive',
    url: '/delrnuvlmnt',
    iconComponent: { name: 'cil-align-left' },
  },
  {
    name: 'Tout Genre',
    url: '/ttgenre',
    iconComponent: { name: 'cil-notes' },
  },
  {
    name: 'Archive',
    url: '/delttgenre',
    iconComponent: { name: 'cil-align-left' },
  },
  {
    name: 'Authentification',
    url: '/authentification',
    iconComponent: { name: 'cil-task' },
  },
  {
    name: 'Archive',
    url: '/rejetauth',
    iconComponent: { name: 'cil-align-left' },
  },
  {
    name: 'Agenda',
    url: '/agenda',
    iconComponent: { name: 'cil-calendar' },
  },
  {
    name: 'Sama Keyyit',
    url: '/sk',
    //iconComponent: { name: 'cil-calendar' },
  },














  // {
  //   title: true,
  //   name: 'Theme'
  // },
  // {
  //   name: 'Colors',
  //   url: '/theme/colors',
  //   iconComponent: { name: 'cil-drop' }
  // },
  // {
  //   name: 'Typography',
  //   url: '/theme/typography',
  //   linkProps: { fragment: 'someAnchor' },
  //   iconComponent: { name: 'cil-pencil' }
  // },
  // {
  //   name: 'Components',
  //   title: true
  // },
  // {
  //   name: 'Base',
  //   url: '/base',
  //   iconComponent: { name: 'cil-puzzle' },
  //   children: [
  //     {
  //       name: 'Accordion',
  //       url: '/base/accordion'
  //     },
  //     {
  //       name: 'Breadcrumbs',
  //       url: '/base/breadcrumbs'
  //     },
  //     {
  //       name: 'Cards',
  //       url: '/base/cards'
  //     },
  //     {
  //       name: 'Carousel',
  //       url: '/base/carousel'
  //     },
  //     {
  //       name: 'Collapse',
  //       url: '/base/collapse'
  //     },
  //     {
  //       name: 'List Group',
  //       url: '/base/list-group'
  //     },
  //     {
  //       name: 'Navs & Tabs',
  //       url: '/base/navs'
  //     },
  //     {
  //       name: 'Pagination',
  //       url: '/base/pagination'
  //     },
  //     {
  //       name: 'Popovers',
  //       url: '/base/popovers'
  //     },
  //     {
  //       name: 'Progress',
  //       url: '/base/progress'
  //     },
  //     {
  //       name: 'Spinners',
  //       url: '/base/spinners'
  //     },
  //     {
  //       name: 'Tables',
  //       url: '/base/tables'
  //     },
  //     {
  //       name: 'Tabs',
  //       url: '/base/tabs'
  //     },
  //     {
  //       name: 'Tooltips',
  //       url: '/base/tooltips'
  //     }
  //   ]
  // },
  // {
  //   name: 'Buttons',
  //   url: '/buttons',
  //   iconComponent: { name: 'cil-cursor' },
  //   children: [
  //     {
  //       name: 'Buttons',
  //       url: '/buttons/buttons'
  //     },
  //     {
  //       name: 'Button groups',
  //       url: '/buttons/button-groups'
  //     },
  //     {
  //       name: 'Dropdowns',
  //       url: '/buttons/dropdowns'
  //     },
  //   ]
  // },
  // {
  //   name: 'Forms',
  //   url: '/forms',
  //   iconComponent: { name: 'cil-notes' },
  //   children: [
  //     {
  //       name: 'Form Control',
  //       url: '/forms/form-control'
  //     },
  //     {
  //       name: 'Select',
  //       url: '/forms/select'
  //     },
  //     {
  //       name: 'Checks & Radios',
  //       url: '/forms/checks-radios'
  //     },
  //     {
  //       name: 'Range',
  //       url: '/forms/range'
  //     },
  //     {
  //       name: 'Input Group',
  //       url: '/forms/input-group'
  //     },
  //     {
  //       name: 'Floating Labels',
  //       url: '/forms/floating-labels'
  //     },
  //     {
  //       name: 'Layout',
  //       url: '/forms/layout'
  //     },
  //     {
  //       name: 'Validation',
  //       url: '/forms/validation'
  //     }
  //   ]
  // },
  // {
  //   name: 'Charts',
  //   url: '/charts',
  //   iconComponent: { name: 'cil-chart-pie' }
  // },
  // {
  //   name: 'Icons',
  //   iconComponent: { name: 'cil-star' },
  //   url: '/icons',
  //   children: [
  //     {
  //       name: 'CoreUI Free',
  //       url: '/icons/coreui-icons',
  //       badge: {
  //         color: 'success',
  //         text: 'FREE'
  //       }
  //     },
  //     {
  //       name: 'CoreUI Flags',
  //       url: '/icons/flags'
  //     },
  //     {
  //       name: 'CoreUI Brands',
  //       url: '/icons/brands'
  //     }
  //   ]
  // },
  // {
  //   name: 'Notifications',
  //   url: '/notifications',
  //   iconComponent: { name: 'cil-bell' },
  //   children: [
  //     {
  //       name: 'Alerts',
  //       url: '/notifications/alerts'
  //     },
  //     {
  //       name: 'Badges',
  //       url: '/notifications/badges'
  //     },
  //     {
  //       name: 'Modal',
  //       url: '/notifications/modal'
  //     },
  //     {
  //       name: 'Toast',
  //       url: '/notifications/toasts'
  //     }
  //   ]
  // },
  // {
  //   name: 'Widgets',
  //   url: '/widgets',
  //   iconComponent: { name: 'cil-calculator' },
  //   badge: {
  //     color: 'info',
  //     text: 'NEW'
  //   }
  // },
  // {
  //   title: true,
  //   name: 'Extras'
  // },
  // {
  //   name: 'Pages',
  //   url: '/login',
  //   iconComponent: { name: 'cil-star' },
  //   children: [
      // {
      //   name: 'Login',
      //   url: '/login'
      // },


  //     {
  //       name: 'Register',
  //       url: '/register'
  //     },
  //     {
  //       name: 'Error 404',
  //       url: '/404'
  //     },
  //     {
  //       name: 'Error 500',
  //       url: '/500'
  //     }
  //   ]
  // },
];


export const navItemsAdmin: INavData[] = [
  {
    name: 'Tableau de bord',
    url: '/dashboard',
    iconComponent: { name: 'cil-speedometer' }

  },
  {
    name: 'Agent',
    url: '/agent',
    iconComponent: { name: 'cil-user' },

  },

  {
    name: 'Archive',
    url: '/delagent',
    iconComponent: { name: 'cil-align-left' },
  },
  {
    name: 'Utilisateur',
    url: '/user',
    iconComponent: { name: 'cil-people' },
  },
  {
    name: 'Utilisateur',
    url: '/adduser',
    iconComponent: { name: 'cil-user-follow' },
  },
]

export const navItemsRenouvellement: INavData[] = [
  {
    name: 'Tableau de bord',
    url: '/dashboard',
    iconComponent: { name: 'cil-speedometer' }

  },
  {
    name: 'Demande',
    title: true,
  },
  {
    name: 'Renouvellement',
    url: '/renouvellement',
    iconComponent: { name: 'cil-spreadsheet' },
  },
  {
    name: 'Archive',
    url: '/delrnuvlmnt',
    iconComponent: { name: 'cil-align-left' },
  },
]


export const navItemsToutGenre: INavData[] = [
  {
    name: 'Tableau de bord',
    url: '/dashboard',
    iconComponent: { name: 'cil-speedometer' }

  },
  {
    name: 'Demande',
    title: true,
  },
  {
    name: 'Tout Genre',
    url: '/ttgenre',
    iconComponent: { name: 'cil-notes' },
  },
  {
    name: 'Archive',
    url: '/delttgenre',
    iconComponent: { name: 'cil-align-left' },
  },
]

export const navItemsNaissance: INavData[] = [
  {
    name: 'Tableau de bord',
    url: '/dashboard',
    iconComponent: { name: 'cil-speedometer' }

  },
  {
    name: 'Demande',
    title: true,
  },
  {
    name: 'Naissance',
    url: '/naissance',
    iconComponent: { name: 'cil-pen' },
  },
  {
    name: 'Archive',
    url: '/delnaissance',
    iconComponent: { name: 'cil-align-left' },
  },
  {
    name: 'Agenda',
    url: '/agenda',
    iconComponent: { name: 'cil-calendar' },
  },
]

export const navItemsMariage: INavData[] = [
  {
    name: 'Tableau de bord',
    url: '/dashboard',
    iconComponent: { name: 'cil-speedometer' }

  },
  {
    name: 'Demande',
    title: true,
  },
  {
    name: 'Mariage',
    url: '/mariage',
    iconComponent: { name: 'cil-pen' },
  },
  {
    name: 'Archive',
    url: '/delmariage',
    iconComponent: { name: 'cil-align-left' },

  },
  {
    name: 'Agenda',
    url: '/agenda',
    iconComponent: { name: 'cil-calendar' },
  },
]
export const navItemsDeces: INavData[] = [
  {
    name: 'Tableau de bord',
    url: '/dashboard',
    iconComponent: { name: 'cil-speedometer' }

  },
  {
    name: 'Demande',
    title: true,
  },
  {
    name: 'Deces',
    url: '/deces',
    iconComponent: { name: 'cil-pen' },
  },
  {
    name: 'Archive',
    url: '/deldeces',
    iconComponent: { name: 'cil-align-left' },
  },
  {
    name: 'Agenda',
    url: '/agenda',
    iconComponent: { name: 'cil-calendar' },
  },
]

export const navItemsOfficier: INavData[] = [
  {
    name: 'Tableau de bord',
    url: '/dashboard',
    iconComponent: { name: 'cil-speedometer' }

  },
  {
    name: 'Agent',
    url: '/agents',
    iconComponent: { name: 'cil-user' },

  },
  {
    name: 'Demande',
    title: true,
  },
  {
    name: 'Liste Demande',
    url: '/officier',
    iconComponent: { name: 'cil-user' },

  },
  {
    name: 'Archive',
    url: '/rejetDemande',
    iconComponent: { name: 'cil-align-left' },

  },
]


