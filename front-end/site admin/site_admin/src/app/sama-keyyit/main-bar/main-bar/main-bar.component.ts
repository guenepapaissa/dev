import { Component, OnInit } from '@angular/core';
import { Service } from 'src/app/service/service.module';

@Component({
  selector: 'app-main-bar',
  templateUrl: './main-bar.component.html',
  styleUrls: ['./main-bar.component.scss']
})
export class MainBarComponent implements OnInit {
  prenom!:string
  nom!:string
  constructor(private service: Service) { }

  ngOnInit(): void {
    this.nom=localStorage.getItem('nom') as string
    this.prenom=localStorage.getItem('prenom') as string


  }
  logOut(){
    this.service.logOut();
  }

}
