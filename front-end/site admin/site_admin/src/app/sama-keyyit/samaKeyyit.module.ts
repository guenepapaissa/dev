import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import {
  AvatarModule,
  ButtonGroupModule,
  ButtonModule,
  CardModule,
  FormModule,
  GridModule,
  NavModule,
  ProgressModule,
  TableModule,
  TabsModule
} from '@coreui/angular';
import { IconModule } from '@coreui/icons-angular';
import { ChartjsModule } from '@coreui/angular-chartjs';
import { WidgetsModule } from 'src/app/views/widgets/widgets.module';
import { SamaKeyyitRoutingModule } from './samaKeyyit-routing.module';
import { MainBarComponent } from './main-bar/main-bar/main-bar.component';
import { RenouvellementComponent } from './guide/guide/renouvellement/renouvellement.component';
import { ToutGenreComponent } from './guide/guide/tout-genre/tout-genre.component';



@NgModule({
  imports: [
    SamaKeyyitRoutingModule,
    CardModule,
    NavModule,
    IconModule,
    TabsModule,
    CommonModule,
    GridModule,
    ProgressModule,
    ReactiveFormsModule,
    ButtonModule,
    FormModule,
    ButtonModule,
    ButtonGroupModule,
    ChartjsModule,
    AvatarModule,
    TableModule,
    WidgetsModule,
    ReactiveFormsModule,
  ],
  declarations: [
    RenouvellementComponent,
    ToutGenreComponent
  ],
  // declarations: [
  //   MainBarComponent
  // ],
})

export class SamaKeyyitModule {
}
