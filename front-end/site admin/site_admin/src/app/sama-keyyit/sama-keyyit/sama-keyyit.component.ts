import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Service } from 'src/app/service/service.module';
import { ClassToggleService, HeaderComponent } from '@coreui/angular';


@Component({
  selector: 'app-sama-keyyit',
  templateUrl: './sama-keyyit.component.html',
  styleUrls: ['./sama-keyyit.component.scss']
})
export class SamaKeyyitComponent extends HeaderComponent implements OnInit {
  prenom!:string
  nom!:string
  @Input() sidebarId: string = "sidebar";
  public newMessages = new Array(4)
  public newTasks = new Array(5)
  public newNotifications = new Array(5)
  constructor(private classToggler: ClassToggleService, private service: Service,) {
    super();
  }

  ngOnInit(): void {
    this.nom=localStorage.getItem('nom') as string
    this.prenom=localStorage.getItem('prenom') as string
  }

  logOut(){
    this.service.logOut();
  }
}
