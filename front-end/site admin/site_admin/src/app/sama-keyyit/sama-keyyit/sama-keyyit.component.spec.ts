import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SamaKeyyitComponent } from './sama-keyyit.component';

describe('SamaKeyyitComponent', () => {
  let component: SamaKeyyitComponent;
  let fixture: ComponentFixture<SamaKeyyitComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SamaKeyyitComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SamaKeyyitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
