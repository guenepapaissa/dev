import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-guide',
  templateUrl: './guide.component.html',
  styleUrls: ['./guide.component.scss']
})
export class GuideComponent implements OnInit {
  courentSection = "renouvellement";
  id!: number
  constructor(private activatedRoute:ActivatedRoute) { 
    this.id=this.activatedRoute.snapshot.params['id']
  }

  ngOnInit(): void {
  }
  changeSection(section: string) {
    
    this.courentSection = section;
  }

}
