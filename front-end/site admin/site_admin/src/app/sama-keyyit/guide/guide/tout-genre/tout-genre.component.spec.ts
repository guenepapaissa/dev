import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ToutGenreComponent } from './tout-genre.component';

describe('ToutGenreComponent', () => {
  let component: ToutGenreComponent;
  let fixture: ComponentFixture<ToutGenreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ToutGenreComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ToutGenreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
