import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthGard } from "../service/auth-gard.service";
import { FaireDemandeComponent } from "./faire-demande/faire-demande/faire-demande.component";
import { GuideComponent } from "./guide/guide/guide.component";
import { MesDemandesComponent } from "./mes-demandes/mes-demandes/mes-demandes.component";
import { NotificationCliComponent } from "./notification/notification-cli/notification-cli.component";
import { SamaKeyyitComponent } from "./sama-keyyit/sama-keyyit.component";

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Authentification',
    },
    children: [
      {
        path: '',
        redirectTo: "",
        pathMatch: 'full',
        component: SamaKeyyitComponent,
        data: {
          title: `Sama Keyyit`
        },
      },
      {
        path: 'faireDemande',
        canActivate : [AuthGard],
        component: FaireDemandeComponent,
        data: {
          title: $localize`Faire Demande`
        }
      },
      {
        path: 'guide',
        component: GuideComponent,
        data: {
          title: $localize`Guide Usager`
        }
      },

      {
        path: 'mesDemandes',
        canActivate : [AuthGard],
        component: MesDemandesComponent,
        data: {
          title: $localize`Mes Demandes`
        }
      },
      {
        path: 'notificationcli',
        canActivate : [AuthGard],
        component: NotificationCliComponent,
        data: {
          title: $localize`Notifications`
        }
      }
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]

})
export class SamaKeyyitRoutingModule {

}
