import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SamaKeyyit } from '../../models/SamaKeyyit';
import { SamaKayyitService } from '../../service/samaKeyyit.service';

@Component({
  selector: 'app-mes-demandes',
  templateUrl: './mes-demandes.component.html',
  styleUrls: ['./mes-demandes.component.scss']
})
export class MesDemandesComponent implements OnInit {
  mesDeclarations: any;
  mesRenouvellements: any;
  mesToutGenres: any;
  currentdemande: any;
  closeResult = '';
  id!:number;
  submitForm=false;
  currentMotif:any

  constructor(private SamaKeyyitServive: SamaKayyitService,private router: Router, private modalService: NgbModal,) {

   }

  ngOnInit(): void {
    this.getMesDeclarations()
    this.getMesToutGenres()
    this.getMesRenouvellements()
  }
  getMesDeclarations(){
    this.SamaKeyyitServive.getMesDeclarations().subscribe({
      next : (res)=>{
        this.mesDeclarations=res;
        console.log(this.mesDeclarations)
      }
    })
  }
  getMesRenouvellements(){
    this.SamaKeyyitServive.getMesRenouvellements().subscribe({
      next : (res)=>{
        this.mesRenouvellements=res;
        console.log(this.mesRenouvellements)
      }
    })
  }
  getMesToutGenres(){
    this.SamaKeyyitServive.getMesToutGenres().subscribe({
      next : (res)=>{
        this.mesToutGenres=res;
        console.log(this.mesToutGenres)
      }
    })
  }
  openSm(content: any, id: number) {
    this.id=id
    this.getMotif(id)
    this.modalService.open(content,{ size: 'sm' }).result.then((result: any) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason: any) => {
   //   this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  getMotif(id:number){
    this.currentMotif=this.SamaKeyyitServive.getMotif(id).subscribe((response)=>{
      this.currentMotif=response
    })
  }
  // ModifierDemande(): void {
  //   this.SamaKeyyitServive.getDemande(this.id).subscribe((response: any) => {
  //     this.currentdemande = response;
  //     console.log( this.currentdemande)
  //     this.SamaKeyyitServive.updateDemande(
  //       this.currentdemande?.id,
  //       this.currentdemande?.prenom,
  //       this.currentdemande?.nom,
  //       this.currentdemande?.telephone,
  //       this.currentdemande?.email,
  //       this.currentdemande?.login,
  //       this.currentdemande?.password,
  //       this.currentdemande?.typeAgent,
  //       ).subscribe({
  //       next: (res) => {
  //         console.log(res);
  //         window.location.reload()
  //        // this.route.navigate(["/agent"]);
  //       },
  //       error: (e) => console.error(e)
  //     });
  //   });
  //   // if (confirm(" voulez-vous bloquer  " + this.currentdemande?.prenom + " " + this.currentdemande?.nom)) {}
  //     this.submitForm = true
  // }
  annulerDemande(id:any){
    this.SamaKeyyitServive.deleteDemande(id,this.mesDeclarations).subscribe({
      next : (res)=>{
        console.log(res)
        this.router.navigate(['/sk']);
      }
    })
  }

}
