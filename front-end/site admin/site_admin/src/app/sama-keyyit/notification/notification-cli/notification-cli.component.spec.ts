import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationCliComponent } from './notification-cli.component';

describe('NotificationCliComponent', () => {
  let component: NotificationCliComponent;
  let fixture: ComponentFixture<NotificationCliComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NotificationCliComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationCliComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
