import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Service } from 'src/app/service/service.module';
import { LoginComponent } from 'src/app/views/pages/login/login.component';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class SamaKayyitService {
  api = environment.api;
  userConnect: any
  id: number = <any>localStorage.getItem('idUser')
  constructor(private http: HttpClient,private service : Service) {
    console.log('sk' +this.userConnect)
  }
  // userConnect


  getMesDeclarations() {
    return this.http.get<any>(this.api + 'mesdeclaration/' + this.id);
  }
  getMesRenouvellements() {
    return this.http.get<any>(this.api + 'mesrenoullement/' + this.id);
  }
  getMesToutGenres() {
    return this.http.get<any>(this.api + 'mestoutGenre/' + this.id);
  }
  getDemandes() {
    return this.http.get<any>(this.api + 'agent/');
  }
  getMotif(id: any){
    return this.http.get<any>(this.api+'motif/'+id)
  }
  createRenouvellement( numeroRegistre: number, annee: number, nombreExemplaire: number,  extrait: string) {
    let addRenouvellement = {
      'client': this.id,
      "numeroRegistre": numeroRegistre,
      "annee": annee,
      "nombreExemplaire": nombreExemplaire,
      "extrait": extrait,

    }
   return this.http
      .post<any>(`${this.api}renouvellement/`,addRenouvellement);
  }

  createNaissance(dateEvenement: Date, ) {
    let addDemande = {
      "extrait": 'Naissance',
      "dateEvenement": dateEvenement,
      "client": this.id,

    }
   return this.http
      .post<any>(`${this.api}declaration_naissance/`,addDemande);
  }
  createMariage(dateEvenement: Date, ) {
    let addDemande = {
      "extrait": 'Mariage',
      "dateEvenement": dateEvenement,
      "client": this.id,

    }
   return this.http
      .post<any>(`${this.api}declaration_mariage/`,addDemande);
  }
  createDeces(dateEvenement: Date, ) {
    let addDemande = {
      "extrait": 'Deces',
      "dateEvenement": dateEvenement,
      "client": this.id,

    }
   return this.http
      .post<any>(`${this.api}declaration_deces/`,addDemande);
  }
  createToutGenre( numeroRegistre: number, annee: number, nombreExemplaire: number,adresse:string,  cerificat: string) {
    let addToutGenre = {
      'client': this.id,
      "numeroRegistre": numeroRegistre,
      "annee": annee,
      "nombreExemplaire": nombreExemplaire,
      "adresse": adresse,
      "cerificat": cerificat,

    }
   return this.http
      .post<any>(`${this.api}toutGenre/`,addToutGenre);
  }
  updateDemande(id: any,prenom: string, nom: string, telephone: string, email: string, login: string, password: string, typeAgent: string){
    let update = {
      "prenom": prenom,
      "nom": nom,
      "telephone": telephone,
      "email": email,
      "login": login,
      "password": password,
      "typeAgent": typeAgent,
    }
    return this.http.put(`${this.api}agent/${id}`,update);
  }
  deleteDemande(id: any,data :any){
    return this.http.delete(`${this.api}agent/${id}`,data);
  }
}
