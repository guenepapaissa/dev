export class SamaKeyyit{
  constructor(
    public numDemande: number,
    public dateDemande: Date,
    public motifRejet: string,
    public etatDemande: string,
    public typeDemande: string,
    public idPersonnel: number,
    public idAyantDroit: number,
    public idRetrait: number,

  ){}
}
