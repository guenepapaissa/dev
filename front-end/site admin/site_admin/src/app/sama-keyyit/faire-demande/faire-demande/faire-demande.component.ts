import { invalid } from '@angular/compiler/src/render3/view/util';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { SamaKayyitService } from '../../service/samaKeyyit.service';


@Component({
  selector: 'app-faire-demande',
  templateUrl: './faire-demande.component.html',
  styleUrls: ['./faire-demande.component.scss']
})
export class FaireDemandeComponent implements OnInit {

  guichets = false;
  extraits = false;
  validation!: FormGroup;
  personal_step = false;
  address_step = false;
  education_step = false;
  step = 1;
  guichet = '';
  extrait = "";
  date!: Date;
  newdemande: any;
  closeResult = '';
  compareDate = false


  constructor(private formBuilder: FormBuilder,private toastr: ToastrService,private router : Router, private SamaKeyyitServive: SamaKayyitService, private modalService: NgbModal,) { }

  ngOnInit() {

  }

  initForm() {
    // Renouvellement
    if (this.guichet === 'renouvellement') {
      this.validation = this.formBuilder.group({
        exemplaire: [1, Validators.required],
        numeroRegistre: ['', [Validators.required,Validators.minLength(4)]],
        annee: ['', Validators.required]
      });
    }
    // Declaration
    if (this.guichet === 'declaration') {
      this.validation = this.formBuilder.group({
        dateCeremonie: ['', Validators.required],
      });
    }
    // Tout genre Residence
    if (this.guichet === 'tout genre' && this.extrait === 'Residence') {
      this.validation = this.formBuilder.group({
        exemplaire: [1, Validators.required],
        numeroRegistre: ['', [Validators.required,Validators.minLength(4)]],
        annee: ['', Validators.required],
        adresse: ['', Validators.required]
      });
    }
  }
  onSubmitForm(content: any): void {
    //     var alertList = document.querySelectorAll('.alert')
    // var alerts =  [].slice.call(alertList).map(function (element) {
    //   return new coreui.Alert(element)
    // })
    //this.submitForm = true
    const formValue = this.validation.value;
    // Renouvellement
    if (this.guichet === 'renouvellement') {
      const annee = new Date(formValue.annee).getFullYear()
      const anneeActuel = new Date().getFullYear()
      // console.log(annee, anneeActuel )
      if (annee < anneeActuel && annee > 1800) {
        this.compareDate = true
      } else {
        this.compareDate = false

      }
      if (this.compareDate === true) {
        // creation de renouvellement
      this.SamaKeyyitServive.createRenouvellement(
        formValue.numeroRegistre,
        formValue.annee,
        formValue.exemplaire,
        this.extrait,
        ).subscribe({
          next: (res) => {
            console.log(res);
            this.toastr.success('Ajoutée avec succes ! Merci de vous munir de votre code de retrait pour la récupération', 'Demande');
            this.router.navigate(['/sk'])
          },
          error: (e) => console.error(e)
        });

      }else{
        this.open(content)
      }
    }
    // Declaration Naissance
    if (this.guichet === 'declaration' && this.extrait==='Naissance') {
      const dateCeremonie = new Date(formValue.dateCeremonie)
      const dateEnvoie = new Date()
      if (dateCeremonie < dateEnvoie) {
        this.newdemande = [
          this.extrait,
          dateCeremonie,
          dateEnvoie,
        ];
        console.log(this.newdemande)
        this.SamaKeyyitServive.createNaissance(
          formValue.dateCeremonie).subscribe({
            next: (res) => {
              console.log(res);
              this.toastr.success('ajouter avec succes !', 'Demande');
              this.router.navigate(['/sk'])
            },
            error: (e) => console.error(e)
          });
      } else {
        this.open(content);
      }

    }
    // declaration mariage
    if (this.guichet === 'declaration' && this.extrait==='Mariage') {
      const dateCeremonie = new Date(formValue.dateCeremonie)
      const dateEnvoie = new Date()
      if (dateCeremonie < dateEnvoie) {
        this.newdemande = [
          this.extrait,
          dateCeremonie,
          dateEnvoie,
        ];
        console.log(this.newdemande)
        this.SamaKeyyitServive.createMariage(
          formValue.dateCeremonie).subscribe({
            next: (res) => {
              console.log(res);
              this.toastr.success('ajouter avec succes !', 'Demande');
              this.router.navigate(['/sk'])
            },
            error: (e) => console.error(e)
          });
      } else {
        this.open(content);
      }

    }
    // declaration Deces
    if (this.guichet === 'declaration' && this.extrait==='Deces') {
      const dateCeremonie = new Date(formValue.dateCeremonie)
      const dateEnvoie = new Date()
      if (dateCeremonie < dateEnvoie) {
        this.newdemande = [
          this.extrait,
          dateCeremonie,
          dateEnvoie,
        ];
        console.log(this.newdemande)
        this.SamaKeyyitServive.createDeces(
          formValue.dateCeremonie).subscribe({
            next: (res) => {
              console.log(res);
              this.toastr.success('ajouter avec succes !', 'Demande');
              this.router.navigate(['/sk'])
            },
            error: (e) => console.error(e)
          });
      } else {
        this.open(content);
      }

    }
    //Tout genge Residence annee.getFullYear() &&
    if (this.guichet === 'tout genre' && this.extrait === 'Residence') {
      const annee = new Date(formValue.annee).getFullYear()
      const anneeActuel = new Date().getFullYear()
      // console.log(annee, anneeActuel )
      if (annee < anneeActuel && annee > 1800) {
        this.compareDate = true
      } else {
        this.compareDate = false
      }
      if (this.compareDate === true) {
        console.log(this.newdemande)
        this.SamaKeyyitServive.createToutGenre(
          formValue.numeroRegistre,
          formValue.annee,
          formValue.exemplaire,
          formValue.adresse,
          this.extrait,
          ).subscribe({
            next: (res) => {
              console.log(res);
              this.toastr.success('Ajoutée avec succes ! Merci de vous munir de votre code de retrait pour la récupération', 'Demande');
              this.router.navigate(['/sk'])
            },
            error: (e) => console.error(e)
          });

        }
      else {
        this.open(content)
      }
    }
  }

  chargerGuichet(chioxguichet: string) {
    this.guichet = chioxguichet;
    this.guichets = true
    console.log(this.guichet)
  }
  chargerPapier(choixPapier: string) {
    this.extrait = choixPapier;
    this.extraits = true
    console.log(this.extrait)

  }
  open(content: any) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      // this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  next() {
    if (this.step == 1) {
      if (this.guichets) { this.step++ }
    }

    if (this.step == 2) {
      if (this.extraits) {
        this.step++;
        this.initForm()
      }
    }

  }

  previous() {

    this.step--
    if (this.step == 1) {
      this.personal_step = false;
    }
    if (this.step == 2) {
      this.education_step = false;
    }
  }

  submit() {

    const formValue = this.validation.value;
    const exemplaire = formValue['exemplaire'];
    const numeroRegistre = formValue['numeroRegistre'];
    const annee = formValue['annee'];
    this.date = new Date();

  }
}


