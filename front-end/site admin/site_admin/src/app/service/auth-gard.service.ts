import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from "@angular/router";
import { Observable } from "rxjs";
import { Service } from "./service.module";

export class UserToken {}
class Permissions {
  canActivate(user: UserToken, id: string): boolean {
    return true;
  }
}

@Injectable({
  providedIn: 'root',
})
export class AuthGard implements CanActivate{
  issa= 'issa'
  isAuth= false
  constructor(private service: Service, private router: Router){
    this.isAuth=this.service.isAuth

  }

  canActivate(route: ActivatedRouteSnapshot,
     state: RouterStateSnapshot): boolean | Promise<boolean>  {
   if(!localStorage.getItem('login')){
    this.router.navigate(['/login'])
   }
      return true
  }

}
