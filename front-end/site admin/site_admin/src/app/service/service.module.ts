import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class Service {
  api = environment.api;
  isAuth=false;
  userConnect: any
  // private userConnectSubject: BehaviorSubject<any>;
  constructor(private http: HttpClient,private router : Router) {
    // const newLocal = localStorage.getItem('userConnect');
    // this.userConnectSubject = new BehaviorSubject<any>(JSON.parse('newLocal'));
    // this.userConnect = this.userConnectSubject.asObservable();
  }


  inscrireClient(prenom: string, nom: string, telephone: string, email: string, login: string, password: string) {
    let addclient = {
      "prenom": prenom,
      "nom": nom,
      "telephone": telephone,
      "email": email,
      "login": login,
      "password": password,
    }
   return this.http.post<any>(`${this.api}client/`,addclient);
  }

  logIn(login: string,password:string){
    this.isAuth=true
    let formatData : FormData=new FormData();
    formatData.append('login',JSON.stringify(login));
    formatData.append('password',JSON.stringify(password));

    return this.http.post<any>(`${this.api}login/`,formatData);

  }

  logOut(){
     localStorage.clear()
     this.router.navigate(['/login'])
  }

}
