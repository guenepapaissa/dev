import { NgModule } from '@angular/core';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { BrowserModule, Title } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { AuthGard,UserToken} from './service/auth-gard.service';

import {
  PERFECT_SCROLLBAR_CONFIG,
  PerfectScrollbarConfigInterface,
  PerfectScrollbarModule,
} from 'ngx-perfect-scrollbar';

// Import routing module
import { AppRoutingModule } from './app-routing.module';

// Import app component
import { AppComponent } from './app.component';


// Import containers
import {
  DefaultFooterComponent,
  DefaultHeaderComponent,
  DefaultLayoutComponent,
} from './containers';

import {
  AccordionModule,
  AvatarModule,
  BadgeModule,
  BreadcrumbModule,
  ButtonGroupModule,
  ButtonModule,
  CardModule,
  DropdownModule,
  FooterModule,
  FormModule,
  GridModule,
  HeaderModule,
  ListGroupModule,
  NavModule,
  ProgressModule,
  SharedModule,
  SidebarModule,
  TabsModule,
  UtilitiesModule,
} from '@coreui/angular';

import { IconModule, IconSetService } from '@coreui/icons-angular';
import { MainContaintComponent } from './user/components/main-containt/main-containt.component';
import { DetailUserComponent } from './user/components/detail-user/detail-user.component';
import { AddUserComponent } from './user/components/add-user/add-user.component';
import { EditUserComponent } from './user/components/edit-user/edit-user.component';
import { DelUserComponent } from './user/components/del-user/del-user.component';
import { AddAgentComponent } from './agent/components/add-agent/add-agent.component';
import { EditAgentComponent } from './agent/components/edit-agent/edit-agent.component';
import { DelAgentComponent } from './agent/components/del-agent/del-agent.component';
import { DetailAgentComponent } from './agent/components/detail-agent/detail-agent.component';
import { MainContentAgentComponent } from './agent/components/main-content-agent/main-content-agent.component';
import { FullCalendarModule } from '@fullcalendar/angular';
import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin from '@fullcalendar/interaction';
import { MainContaintCalendarComponent } from './calendar/main-containt-calendar/main-containt-calendar.component';
import { MainContentAuthentificationComponent } from './demande/authentification/components/main-content-authentification/main-content-authentification/main-content-authentification.component';
import { EditAuthentificationComponent } from './demande/authentification/components/edit-authentification/edit-authentification/edit-authentification.component';
import { AddAuthentificationComponent } from './demande/authentification/components/add-authentification/add-authentification/add-authentification.component';
import { SearchComponent } from './agent/search/search.component';
import { AddToutgenreComponent } from './demande/toutgenre/components/add-toutgenre/add-toutgenre/add-toutgenre.component';
import { DelToutgenreComponent } from './demande/toutgenre/components/del-toutgenre/del-toutgenre/del-toutgenre.component';
import { DetailToutgenreComponent } from './demande/toutgenre/components/detail-toutgenre/detail-toutgenre/detail-toutgenre.component';
import { EditToutgenreComponent } from './demande/toutgenre/components/edit-toutgenre/edit-toutgenre/edit-toutgenre.component';
import { MainContentToutgenreComponent } from './demande/toutgenre/components/main-content-toutGenre/main-content-toutgenre/main-content-toutgenre.component';
import { WidgetsModule } from './views/widgets/widgets.module';
import { AddRenouvellementComponent } from './demande/renouvellement/components/add-renouvellement/add-renouvellement/add-renouvellement.component';
import { DelRenouvellementComponent } from './demande/renouvellement/components/del-renouvellement/del-renouvellement/del-renouvellement.component';
import { DetailRenouvellementComponent } from './demande/renouvellement/components/detail-renouvellement/detail-renouvellement/detail-renouvellement.component';
import { EditRenouvellementComponent } from './demande/renouvellement/components/edit-renouvellement/edit-renouvellement/edit-renouvellement.component';
import { RejetRenouvellementComponent } from './demande/renouvellement/components/rejet-renouvellement/rejet-renouvellement/rejet-renouvellement.component';
import { MainContentRenouvellementComponent } from './demande/renouvellement/components/main-content-renouvellement/main-content-renouvellement/main-content-renouvellement.component';
import { AddDeclarationDecesComponent } from './demande/declaration/deces/components/add-declaration-deces/add-declaration-deces/add-declaration-deces.component';
import { DelDeclarationDecesComponent } from './demande/declaration/deces/components/del-declaration-deces/del-declaration-deces/del-declaration-deces.component';
import { MainContaintDecDecesComponent } from './demande/declaration/deces/components/main-containt-dec-deces/main-containt-dec-deces/main-containt-dec-deces.component';
import { AddContaintDeclarationMariageComponent } from './demande/declaration/mariage/components/add-declaration-mariage/add-containt-declaration-mariage/add-containt-declaration-mariage.component';
import { DelContaintDeclarationMariageComponent } from './demande/declaration/mariage/components/del-declaration-mariage/del-containt-declaration-mariage/del-containt-declaration-mariage.component';
import { DetailContaintDeclarationMariageComponent } from './demande/declaration/mariage/components/detail-declaration-mariage/detail-containt-declaration-mariage/detail-containt-declaration-mariage.component';
import { EditContaintDeclarationMariageComponent } from './demande/declaration/mariage/components/edit-declaration-mariage/edit-containt-declaration-mariage/edit-containt-declaration-mariage.component';
import { MainContaintDecMariageComponent } from './demande/declaration/mariage/components/main-containt-dec-mariage/main-containt-dec-mariage/main-containt-dec-mariage.component';
import { AddContaintDeclarationNaissanceComponent } from './demande/declaration/naissance/components/add-declaration-naissance/add-containt-declaration-naissance/add-containt-declaration-naissance.component';
import { DelContaintDeclarationNaissanceComponent } from './demande/declaration/naissance/components/del-declaration-naissance/del-containt-declaration-naissance/del-containt-declaration-naissance.component';
import { DetailContaintDeclarationNaissanceComponent } from './demande/declaration/naissance/components/detail-declaration-naissance/detail-containt-declaration-naissance/detail-containt-declaration-naissance.component';
import { EditDeclarationNaissanceComponent } from './demande/declaration/naissance/components/edit-declaration-naissance/edit-declaration-naissance/edit-declaration-naissance.component';
import { MainContaintDecNaissanceComponent } from './demande/declaration/naissance/components/main-containt-dec-naissance/main-containt-dec-naissance/main-containt-dec-naissance.component';
import { EditDeclarationDecesComponent } from './demande/declaration/deces/components/edit-declaration-deces/edit-declaration-deces/edit-declaration-deces.component';
import { RejetAuthentificationComponent } from './demande/authentification/components/rejet-authentification/rejet-authentification/rejet-authentification.component';
import { SamaKeyyitComponent } from './sama-keyyit/sama-keyyit/sama-keyyit.component';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { GuideComponent } from './sama-keyyit/guide/guide/guide.component';
import { MesDemandesComponent } from './sama-keyyit/mes-demandes/mes-demandes/mes-demandes.component';
import { NotificationCliComponent } from './sama-keyyit/notification/notification-cli/notification-cli.component';
import { FaireDemandeComponent } from './sama-keyyit/faire-demande/faire-demande/faire-demande.component';
import { HttpClientModule } from '@angular/common/http';
import { MainBarComponent } from './sama-keyyit/main-bar/main-bar/main-bar.component';
import { DetailDeclarationDecesComponent } from './demande/declaration/deces/components/detail-declaration-deces/detail-declaration-deces/detail-declaration-deces.component';
import { MainContentOfficierComponent } from './officier/component/main-content-officier/main-content-officier/main-content-officier.component';
import { DetailOfficierComponent } from './officier/component/detail-officier/detail-officier/detail-officier.component';
import { ChartsModule } from './views/charts/charts.module';
import { RejetOfficierComponent } from './officier/component/rejet-officier/rejet-officier/rejet-officier.component';
//import {AuthGard} from './service/auth-gard.service'

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true,
};

const APP_CONTAINERS = [
  DefaultFooterComponent,
  DefaultHeaderComponent,
  DefaultLayoutComponent,
];
FullCalendarModule.registerPlugins([ // register FullCalendar plugins
  dayGridPlugin,
  interactionPlugin
]);

@NgModule({
  declarations: [AppComponent, ...APP_CONTAINERS,RejetOfficierComponent,NotificationCliComponent,DetailOfficierComponent,MainContentOfficierComponent,DetailDeclarationDecesComponent,MainBarComponent,MainContaintComponent,FaireDemandeComponent,MainContaintComponent,RejetAuthentificationComponent,EditDeclarationDecesComponent,EditAuthentificationComponent,AddAuthentificationComponent, SearchComponent,AddAuthentificationComponent, MainContentAgentComponent,MainContaintComponent, DetailUserComponent, AddUserComponent, EditUserComponent, DelUserComponent, AddAgentComponent, EditAgentComponent, DelAgentComponent, DetailAgentComponent, MainContaintCalendarComponent,MainContentAuthentificationComponent, AddToutgenreComponent, DelToutgenreComponent, DetailToutgenreComponent, EditToutgenreComponent, MainContentToutgenreComponent, AddRenouvellementComponent, DelRenouvellementComponent, DetailRenouvellementComponent, EditRenouvellementComponent, RejetRenouvellementComponent, MainContentRenouvellementComponent, AddDeclarationDecesComponent, DelDeclarationDecesComponent, MainContaintDecDecesComponent,MainContaintComponent, AddContaintDeclarationMariageComponent, DelContaintDeclarationMariageComponent, DetailContaintDeclarationMariageComponent, EditContaintDeclarationMariageComponent, MainContaintDecMariageComponent, AddContaintDeclarationNaissanceComponent, DelContaintDeclarationNaissanceComponent, DetailContaintDeclarationNaissanceComponent, EditDeclarationNaissanceComponent, MainContaintDecNaissanceComponent, SamaKeyyitComponent, GuideComponent, MesDemandesComponent, NotificationCliComponent, FaireDemandeComponent ],
  imports: [
    BrowserModule,
    FormsModule,
    AccordionModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    AvatarModule,
    BreadcrumbModule,
    FooterModule,
    DropdownModule,
    GridModule,
    HeaderModule,
    SidebarModule,
    IconModule,
    PerfectScrollbarModule,
    NavModule,
    ButtonModule,
    FormModule,
    UtilitiesModule,
    ButtonGroupModule,
    ReactiveFormsModule,
    SidebarModule,
    SharedModule,
    TabsModule,
    ListGroupModule,
    ProgressModule,
    BadgeModule,
    ListGroupModule,
    CardModule,
    FullCalendarModule,
    NavModule,
    WidgetsModule,
    HttpClientModule,
    HttpClientModule,
    //AlertModule,
    CarouselModule,
    BrowserAnimationsModule,
    ChartsModule,
    ToastrModule.forRoot(),
    // MatTabsModule
  ],
  providers: [
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy,
    },
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG,
    },
   UserToken,
    Permissions,
    IconSetService,
    Title,
    AuthGard

  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
