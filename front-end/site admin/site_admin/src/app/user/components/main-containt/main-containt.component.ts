import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { Utilisateur } from '../../models/Utilisateur';
import { UserService } from '../services/user.service';



@Component({
  selector: 'app-main-containt',
  templateUrl: './main-containt.component.html',
  styleUrls: ['./main-containt.component.scss']
})

export class MainContaintComponent implements OnInit {
  utilisateur : any;
  currentUser:any;
  closeResult = '';
  id!:number
  constructor(private userService :UserService, private modalService: NgbModal,private toastr: ToastrService,private router: Router) {
   // this.utilisateur = userService.getUsers();
    console.log(this.utilisateur)
  };
  ngOnInit(): void {
    this.getUsers()
  }

  getUsers(){
    this.userService.getUsers().subscribe(response => {
      this.utilisateur = response;
      // alert(JSON.stringify(this.utilisateur))
    });
  }
  open(content: any, id: number) {
    this.id = id
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {

      //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      console.log(reason)
    });
  }
  supprimerUser(){
    this.userService.getUser(this.id).subscribe((response: any) => {
      this.currentUser = response;
      console.log( this.currentUser)
      this.userService.updateUser(
        this.currentUser?.id,
        this.currentUser?.prenom,
        this.currentUser?.nom,
        this.currentUser?.telephone,
        this.currentUser?.email,
        this.currentUser?.login,
        this.currentUser?.password,
        false
        ).subscribe({
        next: (res) => {
          console.log(res);
          //this.router.navigate(["/user"]);
          this.toastr.success(' avec succes','Suppression')
          this.getUsers()
         // window.location.reload()

        },
        error: (e) => console.error(e)
      });
    });
  }

}
