import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-detail-user',
  templateUrl: './detail-user.component.html',
  styleUrls: ['./detail-user.component.scss']
})
export class DetailUserComponent implements OnInit {
  id !:number;
  currentUser :any;

  constructor(private activatedRoute: ActivatedRoute,private userService : UserService) {

   }

  ngOnInit(): void {
    this.id =this.activatedRoute.snapshot.params['id'];
    this.getUser(this.id);
  }
  getUser(id:number){
    this.userService.getUser(id).subscribe(response => {
      this.currentUser = response;
    });
  }

}
