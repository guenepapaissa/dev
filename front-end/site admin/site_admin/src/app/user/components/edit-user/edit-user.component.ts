import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Utilisateur } from '../../models/Utilisateur';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {
  //currentUser = new Utilisateur();
  id !:number;
  currentUser :any;

  constructor(private activatedRoute: ActivatedRoute,private userService :UserService) {

   }

  ngOnInit(): void {
    this.id =this.activatedRoute.snapshot.params['id'];
    this.getUser(this.id);
};

getUser(id:number){
  this.userService.getUser(id).subscribe(response => {
    this.currentUser = response;
  });
}

}
