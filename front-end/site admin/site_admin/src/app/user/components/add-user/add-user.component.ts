import { Component, OnInit } from '@angular/core';
import { Utilisateur } from '../../models/Utilisateur';
import { UserService } from '../services/user.service';
import {NgForm} from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {
  newUser : any;
  currentUser: any
  constructor(private userService :UserService, private router: Router,private toastr: ToastrService,) {
   // this.newUser = userService.getUsers();
    console.log(this.newUser)
  };
  ngOnInit(): void {
    this.getUsers()
  }

  getUsers(){
    this.userService.getUsers().subscribe(response => {
      this.newUser = response;
      // alert(JSON.stringify(this.newUser))
    });
  }
  getUser(id:number){
    this.userService.getUser(id).subscribe((response)=>{
        this.currentUser=response
        console.log(this.currentUser)
      }
    )
  }
  updateUser(id:number){
    this.userService.getUser(id).subscribe((response: any) => {
      this.currentUser = response;
      console.log( this.currentUser)
      this.userService.updateUser(
        this.currentUser?.id,
        this.currentUser?.prenom,
        this.currentUser?.nom,
        this.currentUser?.telephone,
        this.currentUser?.email,
        this.currentUser?.login,
        this.currentUser?.password,
        true
        ).subscribe({
        next: (res) => {
          console.log(res);
         // window.location.reload()
         this.getUsers()
        },
        error: (e) => console.error(e)
      });
    });
  }

}
