import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Utilisateur } from '../../models/Utilisateur';
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: 'root'
})
export class UserService {
  api = environment.api;
  utilisateur: Utilisateur[];
  id !: any;
  usercurrent: any;

  constructor(private http: HttpClient) {
    this.utilisateur = [
      // {
      //   id: 1,
      //   nom: "guene",
      //   prenom: "issa",
      //   num: "785903857",
      //   motDePasse: "####",
      //   adresse: "thies",
      //   email: "@gmail",
      //   etat: "danger",
      //   img: "./assets/img/avatars/2.jpg",
      // },
      // {
      //   id: 2,
      //   nom: "ndiaye",
      //   prenom: "Mouhamed",
      //   num: "770001010",
      //   motDePasse: "####",
      //   adresse: "thies",
      //   email: "@gmail",
      //   etat: "success",
      //   img: "./assets/img/avatars/0.jpg",
      // },
      // {
      //   id: 3,
      //   nom: "Diouf",
      //   prenom: "Sigua",
      //   num: "787775566",
      //   motDePasse: "####",
      //   adresse: "bambey",
      //   email: "@gmail",
      //   etat: "danger",
      //   img: "./assets/img/avatars/0.jpg",
      // },
      // {
      //   id: 4,
      //   nom: "Toure",
      //   prenom: "Lala",
      //   num: "787775566",
      //   motDePasse: "####",
      //   adresse: "bambey",
      //   email: "@gmail",
      //   etat: "danger",
      //   img: "./assets/img/avatars/0.jpg",
      // },

    ];
  }
  getUsers(){
    return this.http.get<any>(this.api + 'client/');
  }
  // ajouterUser(utilisateur: Utilisateur) {
  //   this.utilisateur.push(utilisateur);

  // }

  getUser(id:any){
    return this.http.get<any>(this.api + 'client/'+id);
  }
  updateUser(id:any, prenom: any, nom : any, telephone: any, email: any, login: any, password: any,etat:boolean){
    let user={
      'prenom': prenom,
      'nom': nom,
      'telephone': telephone,
      'email': email,
      'login': login,
      'password': password,
      'etat': etat
    }

    return this.http.put(`${this.api}client/${id}`,user )
  }
  editerUser(idUser: number): Utilisateur[] {
    this.usercurrent = this.utilisateur.find(p => p.id == idUser)
    return this.usercurrent;
  }
  detailUser(idUser: number): Utilisateur[] {
    this.usercurrent = this.utilisateur.find(p => p.id == idUser)
    return this.usercurrent;
  }
}
