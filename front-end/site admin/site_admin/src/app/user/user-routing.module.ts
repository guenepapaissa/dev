import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from '../views/dashboard/dashboard.component';
import { AddUserComponent } from './components/add-user/add-user.component';
import { DetailUserComponent } from './components/detail-user/detail-user.component';
import { EditUserComponent } from './components/edit-user/edit-user.component';
import { MainContaintComponent } from './components/main-containt/main-containt.component';


const routes: Routes = [
  {
    path: '',
    data: {
      title: `Utilisateur`
    },
    children: [
      {
        path: '',
        redirectTo: '',
        pathMatch: 'full',
        component: MainContaintComponent,
        data: {
          title: `Utilisateur`
        }
      },
      {
        path: 'adduser',
        component: AddUserComponent,
        data: {
          title: $localize`Ajout utilisateur`
        }
      },
      {
        path: 'edituser/:id',
        component: EditUserComponent,
        data: {
          title: $localize`Modifier utilisteur`
        }
      },
      {
        path: 'detailuser/:id',
        component: DetailUserComponent,
        data: {
          title: $localize`Detail utilisateur`
        }
      }
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule {
}
