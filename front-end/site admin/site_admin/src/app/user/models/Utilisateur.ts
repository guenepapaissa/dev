export class Utilisateur {
  constructor(
    public id : number,
    public prenom : string,
    public nom : string,
    public email : string,
    public telephone : string,
    public password : string,
    public login : string,
    public adresse ?: string,
    public num ?: string,
    public motDePasse?: string,
    public etat?: string,
    public img ?: string,

    ) { };
}
