import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DefaultLayoutComponent } from './containers';
import { Page404Component } from './views/pages/page404/page404.component';
import { Page500Component } from './views/pages/page500/page500.component';
import { LoginComponent } from './views/pages/login/login.component';
import { RegisterComponent } from './views/pages/register/register.component';
import { AddUserComponent } from './user/components/add-user/add-user.component';
import { EditUserComponent } from './user/components/edit-user/edit-user.component';
import { DetailUserComponent } from './user/components/detail-user/detail-user.component';
import { AddAgentComponent } from './agent/components/add-agent/add-agent.component';
import { DetailAgentComponent } from './agent/components/detail-agent/detail-agent.component';
import { EditAgentComponent } from './agent/components/edit-agent/edit-agent.component';
import { DelAgentComponent } from './agent/components/del-agent/del-agent.component';
import { MainContaintCalendarComponent } from './calendar/main-containt-calendar/main-containt-calendar.component';
import { MainContentAgentComponent } from './agent/components/main-content-agent/main-content-agent.component';
import { SearchComponent } from './agent/search/search.component';
import { DelContaintDeclarationNaissanceComponent } from './demande/declaration/naissance/components/del-declaration-naissance/del-containt-declaration-naissance/del-containt-declaration-naissance.component';
import { DelContaintDeclarationMariageComponent } from './demande/declaration/mariage/components/del-declaration-mariage/del-containt-declaration-mariage/del-containt-declaration-mariage.component';
import { DelDeclarationDecesComponent } from './demande/declaration/deces/components/del-declaration-deces/del-declaration-deces/del-declaration-deces.component';
import { RejetAuthentificationComponent } from './demande/authentification/components/rejet-authentification/rejet-authentification/rejet-authentification.component';
import { DelRenouvellementComponent } from './demande/renouvellement/components/del-renouvellement/del-renouvellement/del-renouvellement.component';
import { DelToutgenreComponent } from './demande/toutgenre/components/del-toutgenre/del-toutgenre/del-toutgenre.component';
import { SamaKeyyitComponent } from './sama-keyyit/sama-keyyit/sama-keyyit.component';
import { MainContaintComponent } from './user/components/main-containt/main-containt.component';
import { AuthGard } from './service/auth-gard.service';
import { RejetOfficierComponent } from './officier/component/rejet-officier/rejet-officier/rejet-officier.component';
import { DetailOfficierComponent } from './officier/component/detail-officier/detail-officier/detail-officier.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',

  },
  {
    path: '',
    component: DefaultLayoutComponent,
    data: {
      title: 'Acceuil'
    },
    children: [
      // {
      //   path: 'user',
      //   loadChildren: () =>
      //     import('./user/user.module').then((m) => m.UserModule)
      // },
      // {
      //   path: 'adduser',
      //   component: AddUserComponent,
      //   data: {
      //     title: 'Ajout Utilisateur'
      //   }
      // },
      // {
      //   path: 'edituser/:id',
      //   component: EditUserComponent,
      //   data: {
      //     title: 'Modifier Utilisateur'
      //   }
      // },
      // {
      //   path: 'detailuser/:id',
      //   component: DetailUserComponent,
      //   data: {
      //     title: 'Detail Utilisateur'
      //   }
      // },

      // {
      //   path:'search/searchTerm',
      //   component: SearchComponent,
      //   data:{
      //     title:'search'
      //   }
      // },
      // {
      //   path: 'edituser/:id',
      //   component: EditUserComponent,
      //   data: {
      //     title: 'Modifier Utilisateur'
      //   }
      // },
      // {
      //   path: 'detailuser/:id',
      //   component: DetailUserComponent,
      //   data: {
      //     title: 'Detail Utilisateur'
      //   }
      // },

      // {
      //   path: 'addagent',
      //   component: AddAgentComponent,
      //   data: {
      //     title: 'Ajout Agent'
      //   }
      // },
      // {
      //   path: 'editagent/:id',
      //   component: EditAgentComponent,
      //   data: {
      //     title: 'Modifier Agent'
      //   }
      // },
      // {
      //   path: 'detailagent/:id',
      //   component: DetailAgentComponent,
      //   data: {
      //     title: 'Detail Agent'
      //   }
      // },
      // {
      //   path: 'delagent',
      //   component: DelAgentComponent,
      //   data: {
      //     title: 'Delete Agent'
      //   }
     // },
      // {
      //   path: 'agenda',
      //   component: MainContaintCalendarComponent,
      //   data: {
      //     title: 'Agenda'
      //   }
      // },

















      {
        path: '',
        canActivate : [AuthGard],
        loadChildren: () =>
          import('./views/dashboard/dashboard.module').then((m) => m.DashboardModule)
      },
      {
        path: 'agent',
        canActivate : [AuthGard],
        loadChildren: () =>
          import('./agent/agent.module').then((m) => m.AgentModule)
      },
      {
        path: 'agenda',
        canActivate : [AuthGard],
        component: MainContaintCalendarComponent,
        data: {
          title: 'Agenda'
        }
      },
      {
        path: 'authentification',
        canActivate : [AuthGard],
        loadChildren: () =>
          import('./demande/authentification/authentification.module').then((m) => m.AuthentificationModule)
      },
      {
        path: 'renouvellement',
        canActivate : [AuthGard],
        loadChildren: () =>
          import('./demande/renouvellement/renouvellement.Module').then((m) => m.RenouvellementModule)
      },
      {
        path: 'deces',
        canActivate : [AuthGard],
        loadChildren: () =>
          import('./demande/declaration/deces/deces.module').then((m) => m.DecesModule)
      },
      {
        path: 'mariage',
        canActivate : [AuthGard],
        loadChildren: () =>
          import('./demande/declaration/mariage/mariage.module').then((m) => m.MariageModule)
      },
      {
        path: 'naissance',
        canActivate : [AuthGard],
        loadChildren: () =>
          import('./demande/declaration/naissance/naissance.module').then((m) => m.NaissanceModule)
      },
      {
        path: 'officier',
        canActivate : [AuthGard],
        loadChildren: () =>
          import('./officier/officier.module').then((m) => m.OfficierModule)
      },
      {
        path: 'user',
        canActivate : [AuthGard],
        loadChildren: () =>
          import('./user/user.module').then((m) => m.UserModule)
      },
      {
        path: 'ttgenre',
        canActivate : [AuthGard],
        loadChildren: () =>
          import('./demande/toutgenre/toutGenre.Module').then((m) => m.ToutGenreModule)
      },
      {
        path: 'theme',
        canActivate : [AuthGard],
        loadChildren: () =>
          import('./views/theme/theme.module').then((m) => m.ThemeModule)
      },
      {
        path: 'base',
        canActivate : [AuthGard],
        loadChildren: () =>
          import('./views/base/base.module').then((m) => m.BaseModule)
      },
      {
        path: 'buttons',
        canActivate : [AuthGard],
        loadChildren: () =>
          import('./views/buttons/buttons.module').then((m) => m.ButtonsModule)
      },
      {
        path: 'forms',
        canActivate : [AuthGard],
        loadChildren: () =>
          import('./views/forms/forms.module').then((m) => m.CoreUIFormsModule)
      },
      {
        path: 'charts',
        canActivate : [AuthGard],
        loadChildren: () =>
          import('./views/charts/charts.module').then((m) => m.ChartsModule)
      },
      {
        path: 'icons',
        canActivate : [AuthGard],
        loadChildren: () =>
          import('./views/icons/icons.module').then((m) => m.IconsModule)
      },
      {
        path: 'notifications',
        canActivate : [AuthGard],
        loadChildren: () =>
          import('./views/notifications/notifications.module').then((m) => m.NotificationsModule)
      },
      {
        path: 'widgets',
        canActivate : [AuthGard],
        loadChildren: () =>
          import('./views/widgets/widgets.module').then((m) => m.WidgetsModule)
      },

      {
        path: 'pages',
        canActivate : [AuthGard],
        loadChildren: () =>
          import('./views/pages/pages.module').then((m) => m.PagesModule)
      },
      {
        path: 'adduser',
        canActivate : [AuthGard],
        component: AddUserComponent,
        data: {
          title: $localize`Nouveau Utilisateur`
        }
      },
      {
        path: 'delnaissance',
        canActivate : [AuthGard],
        component: DelContaintDeclarationNaissanceComponent,
        data: {
          title: $localize`Archive Declaration  Naissance`
        }
      },
      {
        path: 'delmariage',
        canActivate : [AuthGard],
        component: DelContaintDeclarationMariageComponent,
        data: {
          title: $localize`Archive Declaration Mariage`
        }
      },
      {
        path: 'agents',
        canActivate : [AuthGard],
        component: DetailOfficierComponent,
        data: {
          title: $localize`Liste Agent`
        }
      },
      {
        path: 'rejetDemande',
        canActivate : [AuthGard],
        component: RejetOfficierComponent,
        data: {
          title: $localize`Faire Demande`
        }
      },
      {
        path: 'deldeces',
        canActivate : [AuthGard],
        component: DelDeclarationDecesComponent,
        data: {
          title: $localize`Archive Declaration Deces`
        }
      },
      {
        path: 'rejetauth',
        canActivate : [AuthGard],
        component: RejetAuthentificationComponent,
        data: {
          title: $localize`Archive Authentification`
        }
      },
      {
        path: 'delrnuvlmnt',
        canActivate : [AuthGard],
        component: DelRenouvellementComponent,
        data: {
          title: $localize`Detail Renouvellement`
        }
      },
      {
        path: 'delttgenre',
        canActivate : [AuthGard],
        component:  DelToutgenreComponent,
        data: {
          title: $localize`Archive Tout Genre`
        }
      },
      {
        path: 'delagent',
        canActivate : [AuthGard],
        component:  DelAgentComponent,
        data: {
          title: $localize`Archive Agent`
        }
      },

    ]
  },

  {
    path: 'sk',
    loadChildren: () =>
    import('./sama-keyyit/samaKeyyit.module').then((m) => m.SamaKeyyitModule)
  },
  {
    path: '404',

    component: Page404Component,
    data: {
      title: 'Page 404'
    }
  },
  {
    path: '500',
    component: Page500Component,
    data: {
      title: 'Page 500'
    }
  },
  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: 'Login Page'
    }
  },
  {
    path: 'register',
    component: RegisterComponent,
    data: {
      title: 'Register Page'
    }
  },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      scrollPositionRestoration: 'top',
      anchorScrolling: 'enabled',
      initialNavigation: 'enabledBlocking'
      // relativeLinkResolution: 'legacy'
    }),
    RouterModule.forRoot([
      // {
      //   path: 'team/:id',
      //   component: auth-gard.service.ts,
      //   canActivate: [AuthGard]
      // }
    ])
  ],

  exports: [RouterModule]
})
export class AppRoutingModule {
}
